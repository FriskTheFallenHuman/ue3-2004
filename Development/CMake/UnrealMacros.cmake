set(UNREAL_MODULES "")

# message with bool option shorthand
macro(MESSAGE_BOOL_OPTION _NAME _VALUE)
    if(${_VALUE})
        message(STATUS "  ${_NAME}: ON")
    else(${_VALUE})
        message(STATUS "  ${_NAME}: OFF")
    endif(${_VALUE})
endmacro()

# message with string option shorthand
macro(MESSAGE_STR_OPTION _NAME _VALUE)
    message(STATUS "  ${_NAME}: '${${_VALUE}}'")
endmacro()

# _GAMENAME can be a name (WARGAME, UTGAME, DEMOGAME, etc), or a list:
#  "WARGAME;UTGAME" Or "ALL" to use everywhere.
# _MODULEFOLDERNAME is the projects folder name for IDEs
# _MODULE is the module project name
# _SRCS, _HEADERS, and _PCHHEADER should be self-explanatory
macro(ADD_UNREAL_MODULE _GAMENAME _MODULEFOLDERNAME _MODULE _SRCS _HEADERS _PCHHEADER)
	# match module by gamename
    if("${_GAMENAME}" STREQUAL "ALL")
        set(_MODULE_MATCHES ON)
    else()
        set(_MODULE_MATCHES OFF)
        foreach(LOOPER ${_GAMENAME})
            if(UNREAL_GAMENAME STREQUAL "${LOOPER}")
                set(_MODULE_MATCHES ON)
            endif()
        endforeach(LOOPER)
    endif()

	# if we have a matching module, lets get it compiled
    if(_MODULE_MATCHES)
        #message(STATUS "Module ${_MODULE} SRCS ${_SRCS} HEADERS ${_HEADERS} PCHHEADER ${_PCHHEADER}")

		# add library sources and headers and set folder property
        add_library(${_MODULE} STATIC ${_SRCS} ${_HEADERS})
		set_target_properties(${_MODULE} PROPERTIES FOLDER ${_MODULEFOLDERNAME})
		
		# set debug symbols
        option(UNREAL_DEBUG_SYMS_${_MODULE} "Specifically build '${_MODULE}' with debug symbols" ${UNREAL_BUILD_WITH_DEBUG_SYMBOLS})
        if(UNREAL_DEBUG_SYMS_${_MODULE})
            set_target_properties(${_MODULE} PROPERTIES COMPILE_FLAGS "${DEBUG_SYMBOLS_FLAG}")
        endif()

		# set precompiled headers
		if(UNREAL_PRECOMPILED_HEADERS)
            if(NOT "${_PCHHEADER}" STREQUAL "")
                target_precompile_headers(${_MODULE} PRIVATE "${_PCHHEADER}")
            endif()
        endif()

		# add to UNREAL_MODULES list for final linking
        set(UNREAL_MODULES "${UNREAL_MODULES};${_MODULE}" PARENT_SCOPE)
    endif()
endmacro()

