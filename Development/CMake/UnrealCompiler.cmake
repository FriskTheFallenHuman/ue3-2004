# Compiler/OS specific settings

# set C++11 standard
set(CMAKE_CXX_STANDARD 11)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

# I hate that they define "WIN32" ... what about the move to Win64...
if(WIN32 AND NOT WINDOWS)
    set(WINDOWS TRUE)
endif()

# Bleh, let's do it for "APPLE" too.
if(APPLE AND NOT MACOSX)
    set(MACOSX TRUE)
endif()

# make sure we only have Release and Debug types
if(CMAKE_CONFIGURATION_TYPES)      
	set(CMAKE_CONFIGURATION_TYPES Debug Release)
	set(CMAKE_CONFIGURATION_TYPES "${CMAKE_CONFIGURATION_TYPES}" CACHE STRING "Build configuration type" FORCE)
endif(CMAKE_CONFIGURATION_TYPES)

# set default build type to Debug if not specified
if(NOT CMAKE_BUILD_TYPE)
    set(UNREAL_DEBUG_BUILD ON)
endif()
if(CMAKE_BUILD_TYPE EQUAL "Debug")
    set(UNREAL_DEBUG_BUILD ON)
endif()

if (CMAKE_COMPILER_IS_GNUCC OR CMAKE_C_COMPILER_ID MATCHES "Clang")

	# GCC and Clang settings
	
	set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -std=gnu99 -Wall -fno-strict-aliasing -fwrapv")
    set(CMAKE_C_FLAGS_DEBUG "-g -ggdb-O0")
    set(CMAKE_C_FLAGS_DEBUGALL "-g -ggdb")

    set(CMAKE_CXX_FLAGS_DEBUGALL ${CMAKE_C_FLAGS_DEBUGALL})
    set(CMAKE_CXX_FLAGS_DEBUG ${CMAKE_C_FLAGS_DEBUG})

	# stick to a max of optimization level 2
	string(REPLACE "-O3" "-O2" CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE}")
	
	# Switch off some annoying warnings
	if ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
		set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wno-missing-braces -Wno-self-assign -Wno-unused-value")
	elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
		if ( ( CMAKE_CXX_COMPILER_VERSION GREATER 8.0 ) OR ( CMAKE_CXX_COMPILER_VERSION EQUAL 8.0 ) )
			set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -Wno-format-truncation -Wno-format-overflow")
		endif()
	endif()

	# If we're building with gcc for i386 let's define -ffloat-store.
	# This helps the old and crappy x87 FPU to produce correct values.
	# Would be nice if Clang had something comparable.
	if(UNREAL_IS_X86)
		if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
			set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -ffloat-store")
		endif()
	endif()

    # Set our C++ flags to match the C flags
    set(CMAKE_CXX_FLAGS ${CMAKE_C_FLAGS})

	# Force SSE math on x86_64 and x86
	if(UNREAL_IS_X86 OR UNREAL_IS_AMD64)
		add_definitions(-mmmx -msse2 -mfpmath=sse)
    endif()
	
	# Force Altivec math on macOS PPC
	if(MACOSX AND UNREAL_IS_POWERPC)
        add_definitions(-faltivec -force_cpusubtype_ALL)
    endif()

	# define Unix platform. macOS is set below
	if(NOT APPLE)
		add_definitions(-DPLATFORM_UNIX)
		
		# Karma MathEngine SDK expects this.
		add_definitions(-DLINUX=1)
	endif()

elseif (MSVC)

	# MSVC settings

	# define the standard libraries
	set(CMAKE_CXX_STANDARD_LIBRARIES "kernel32.lib user32.lib gdi32.lib winspool.lib shell32.lib ole32.lib oleaut32.lib uuid.lib comdlg32.lib comctl32.lib advapi32.lib dbghelp.lib wsock32.lib ws2_32.lib rpcrt4.lib wininet.lib winmm.lib")
	set(CMAKE_C_STANDARD_LIBRARIES "kernel32.lib user32.lib gdi32.lib winspool.lib shell32.lib ole32.lib oleaut32.lib uuid.lib comdlg32.lib comctl32.lib advapi32.lib dbghelp.lib wsock32.lib ws2_32.lib rpcrt4.lib wininet.lib winmm.lib")
	if (MSVC_VERSION GREATER 1900)
		# for MSVC 2015+, link the Universal CRT library as well as the legacy C stdio library
		set(CMAKE_CXX_STANDARD_LIBRARIES "${CMAKE_CXX_STANDARD_LIBRARIES} legacy_stdio_definitions.lib ucrtd.lib")
		set(CMAKE_C_STANDARD_LIBRARIES "${CMAKE_C_STANDARD_LIBRARIES} legacy_stdio_definitions.lib ucrtd.lib")
	endif()

	# define our own debug and release flags
	set(UNREAL_DEBUG_FLAGS "/Od /Oi /Ot /GF- /Gm- /Zp4 /Zm256 /Gy- /arch:SSE /fp:fast")
	set(UNREAL_RELEASE_FLAGS "/Ox /Ob2 /Oi /Ot /Oy- /GF- /Gm- /Zp4 /Zm256 /GS- /Gy- /arch:SSE /fp:fast")

	# set C and C++ flags
	set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} /MDd /D _DEBUG ${UNREAL_DEBUG_FLAGS}")
	set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /MD /D NDEBUG ${UNREAL_RELEASE_FLAGS}")
	set(CMAKE_C_FLAGS_DEBUG "${CMAKE_C_FLAGS_DEBUG} /MDd /D _DEBUG ${UNREAL_DEBUG_FLAGS}")
	set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} /MD /D NDEBUG ${UNREAL_RELEASE_FLAGS}")

	# set release mode debug symbol support
	if (UNREAL_BUILD_WITH_DEBUG_SYMBOLS)
		set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} /Zi")
		set(CMAKE_C_FLAGS_RELEASE "${CMAKE_C_FLAGS_RELEASE} /Zi")
	endif()

	# set linker flags
	set(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG} /DEBUG")
	set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} /DEBUG")

	# for MSVC 2013+, without /SAFESEH:NO, we can't link against older libraries
	if (MSVC_VERSION GREATER 1800)
		set(CMAKE_EXE_LINKER_FLAGS_DEBUG "${CMAKE_EXE_LINKER_FLAGS_DEBUG} /SAFESEH:NO")
		set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} /SAFESEH:NO")
	endif()

	# old CMake variants used to set the stack, newer ones do not, so we'll set it ourselves
	if( NOT (${CMAKE_VERSION} VERSION_LESS 2.8.11) )
		# set stack reserved and commit size to ~32MB
		set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} /STACK:3145728,3145728")
	endif()
	
	# set our default external library paths
	set(OALSOFT_PATH ${PROJECT_SOURCE_DIR}/External/OpenAL-soft)
	set(SDL2_PATH ${PROJECT_SOURCE_DIR}/External/SDL2)

	# add default Windows defines
	add_definitions(
		-DWIN32
		-D_WINDOWS
		-D_CRT_SECURE_NO_DEPRECATE
		-D_CRT_NONSTDC_NO_DEPRECATE
		-D_CRT_SECURE_NO_WARNINGS
		-D_WIN32_WINNT=0x0602
		-DWINVER=0x0602
	)

	# enable the CMake INSTALL target in MSVC
	set(CMAKE_VS_INCLUDE_INSTALL_TO_DEFAULT_BUILD 1)
endif()

if (APPLE)

	# macOS/iOS specific settings

	if(NOT ${CMAKE_SYSTEM_NAME} STREQUAL "iOS")
		add_definitions(-DPLATFORM_MACOSX)
	else()
		add_definitions(-DPLATFORM_IOS)
	endif()

	# Add the shared unix platform define as well
	add_definitions(-DPLATFORM_UNIX)
	
	# GameSpy's SDK expects this.
	add_definitions(-D_MACOSX=1)

	# Karma MathEngine SDK expects this.
	add_definitions(-DMACOSX=1)

	#set(CMAKE_XCODE_ATTRIBUTE_MACOSX_DEPLOYMENT_TARGET "10.10")
    set(CMAKE_XCODE_ATTRIBUTE_GCC_C_LANGUAGE_STANDARD "gnu11") 
    set(CMAKE_XCODE_ATTRIBUTE_CLANG_CXX_LANGUAGE_STANDARD "c++11")
    set(CMAKE_XCODE_ATTRIBUTE_CLANG_CXX_LIBRARY "libc++")
    set(CMAKE_XCODE_ATTRIBUTE_DEBUG_INFORMATION_FORMAT "dwarf-with-dsym")

	set(CMAKE_MACOSX_RPATH 1)

	if(NOT (${CMAKE_SYSTEM_NAME} STREQUAL "iOS"))
		set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -framework IOKit -framework Cocoa")
	else()
		set(CMAKE_EXE_LINKER_FLAGS "${CMAKE_EXE_LINKER_FLAGS} -framework AVFoundation -framework CoreAudio -framework QuartzCore -framework OpenGLES -framework CoreBluetooth -framework CoreVideo -framework Metal -framework CoreMotion -framework CoreGraphics -framework AudioToolbox -framework IOKit -framework UIKit -framework GameController -framework Foundation -framework CoreFoundation")
	endif()
 
	# For now "sign to run locally"
    set(CMAKE_XCODE_ATTRIBUTE_CODE_SIGNING_REQUIRED "YES")
    set(CMAKE_XCODE_ATTRIBUTE_CODE_SIGN_IDENTITY "-")

endif()

# Get some universal truths out of the way here...
add_definitions(-D_UNICODE=1 -DUNICODE=1)
if(UNREAL_DEBUG_BUILD)
    add_definitions(-DDEBUG=1 -D_DEBUG=1)
else()
    add_definitions(-DNDEBUG=1 -D_NDEBUG=1)
endif()

# Set the game name define
add_definitions("-DGAMENAME=${UNREAL_GAMENAME}")

# !!! FIXME: get this in there at some point...
#add_definitions(-DFINAL_RELEASE=1)

# Some things wxWidgets wants defined...
if(UNREAL_BUILD_WITH_EDITOR)
	add_definitions(-D_FILE_OFFSET_BITS=64 -D_LARGE_FILES)
	if(UNIX)
		if(APPLE)
			add_definitions(-D__WXMAC__)
		else()
			add_definitions(-D__WXGTK__)
		endif()
	endif()
	if(WINDOWS)
		add_definitions(-D__WXWIN__ -DWXUSINGDLL=1 -DWXXMLISDLL=1)
	endif()
endif()

# Concessions for PhysX...
#add_definitions(-DNX_USE_SDK_STATICLIBS=1)

# PhysX needs you to define this, but I'd rather they check
#  for things like _M_X64 or whatever in their headers...
# !!! FIXME: see about changing the PhysX headers for a future PhysX release.
if(CMAKE_SIZEOF_VOID_P EQUAL 4)
    add_definitions(-DNX32)
    set(UNREAL_CHOSE_PTRSIZE TRUE)
endif()
if(CMAKE_SIZEOF_VOID_P EQUAL 8)
    add_definitions(-DNX64)
    set(UNREAL_CHOSE_PTRSIZE TRUE)
endif()
if(NOT UNREAL_CHOSE_PTRSIZE)
    message(FATAL_ERROR "Uhoh, we're not targeting a 32 or 64 bit system!")
endif()

# PhysX expects this #define...
# (And GameSpy expects _LINUX ...)
# !!! FIXME: would like to change that in a future PhysX release...
# !!! FIXME:  (look for __linux__ instead...)
if(CMAKE_SYSTEM_NAME STREQUAL "Linux")
    add_definitions(-DLINUX=1)
    add_definitions(-D_LINUX=1)
endif()
