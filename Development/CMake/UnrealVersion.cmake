# Grab the Git revision
execute_process(COMMAND git log --pretty=format:'%h' -n 1
                OUTPUT_VARIABLE GIT_REV
                ERROR_QUIET)

# Check whether we got any revision
# (which isn't always the case, e.g. when someone downloaded a zip file instead of a checkout)
if ("${GIT_REV}" STREQUAL "")
    set(GIT_REV "n/a")
    set(GIT_DIFF "")
    set(GIT_TAG "n/a")
    set(GIT_BRANCH "n/a")
else()
    # Grab the Branch and Tag from Git
    execute_process(
        COMMAND bash -c "git diff --quiet --exit-code || echo +"
        OUTPUT_VARIABLE GIT_DIFF)
    execute_process(
        COMMAND git describe --exact-match --tags
        OUTPUT_VARIABLE GIT_TAG ERROR_QUIET)
    execute_process(
        COMMAND git rev-parse --abbrev-ref HEAD
        OUTPUT_VARIABLE GIT_BRANCH)

    string(STRIP "${GIT_REV}" GIT_REV)
    string(SUBSTRING "${GIT_REV}" 1 7 GIT_REV)
    string(STRIP "${GIT_DIFF}" GIT_DIFF)
    if(GIT_TAG STREQUAL "")
        set(GIT_TAG "n/a")
    else()
        string(STRIP "${GIT_TAG}" GIT_TAG)
    endif()
    string(STRIP "${GIT_BRANCH}" GIT_BRANCH)
endif()

# Set the version string
set(VERSION
    "const char* GGitRevision=\"${GIT_REV}${GIT_DIFF}\";
    const char* GGitTag=\"${GIT_TAG}\";
    const char* GGitBranch=\"${GIT_BRANCH}\";"
)

# Write out our version file
if(EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/CoreVersion.cpp)
    file(READ ${CMAKE_CURRENT_SOURCE_DIR}/CoreVersion.cpp VERSION_)
else()
    set(VERSION_ "")
endif()

if (NOT "${VERSION}" STREQUAL "${VERSION_}")
    file(WRITE ${CMAKE_CURRENT_SOURCE_DIR}/CoreVersion.cpp "${VERSION}")
endif()