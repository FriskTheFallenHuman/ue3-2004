@echo off
cls
del /s /q MSVC2019
mkdir MSVC2019
cd MSVC2019
cmake -G "Visual Studio 16 2019" -A Win32 -DUNREAL_GAMENAME=WARGAME -DUNREAL_EXENAME=WarfareGame ../..
pause
