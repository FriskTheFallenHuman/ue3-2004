@echo off
cls
del /s /q 32bit-warfaregame
mkdir 32bit-warfaregame
cd 32bit-warfaregame
cmake -G "Visual Studio 17 2022" -A Win32 -DUNREAL_GAMENAME=WARGAME -DUNREAL_EXENAME=WarfareGame ../..
pause
