@echo off
cls
del /s /q 32bit-utgame
mkdir 32bit-utgame
cd 32bit-utgame
cmake -G "Visual Studio 17 2022" -A Win32 -DUNREAL_GAMENAME=UTGAME -DUNREAL_EXENAME=UTGame ../..
pause
