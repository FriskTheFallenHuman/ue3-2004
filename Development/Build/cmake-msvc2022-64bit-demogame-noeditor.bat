@echo off
cls
del /s /q Build_With_Editor(x64)
mkdir Build_With_Editor(x64)
cd Build_With_Editor(x64)
cmake -G "Visual Studio 17 2022" -A x64 -DUNREAL_GAMENAME=DEMOGAME -DUNREAL_EXENAME=DemoGame -DUNREAL_BUILD_WITH_EDITOR=Off -DUNREAL_BUILD_WITH_NOVODEX=Off ../..
pause
