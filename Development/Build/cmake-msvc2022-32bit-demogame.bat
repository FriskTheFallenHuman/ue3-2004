@echo off
cls
del /s /q 32bit-demogame
mkdir 32bit-demogame
cd 32bit-demogame
cmake -G "Visual Studio 17 2022" -A Win32 -DUNREAL_GAMENAME=DEMOGAME -DUNREAL_EXENAME=DemoGame ../..
pause
