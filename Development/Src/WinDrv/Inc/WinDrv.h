/*=============================================================================
	WinDrv.h: Unreal Windows viewport and platform driver.
	Copyright 2003 Epic Games, Inc. All Rights Reserved.

Revision history:
	* Created by Tim Sweeney.
	* Rewritten by Andrew Scheidecker and Daniel Vogel
=============================================================================*/

#ifndef _INC_WINDRV
#define _INC_WINDRV

/**
 * The joystick support is hardcoded to either handle an Xbox 1 Type S controller or
 * a PlayStation 2 Dual Shock controller plugged into a PC.
 *
 * See http://www.redcl0ud.com/xbcd.html for Windows drivers for Xbox 1 Type S.
 */

/** This is how many joystick buttons are supported. Must be less than 256 */
#define MAX_JOYSTICK_BUTTONS		16

#define SAFE_RELEASE(p) { if(p) { (p)->Release(); (p)=NULL; } }

// Unreal includes.
#include "Engine.h"

// Windows includes.
#pragma pack (push,8)
#define DIRECTINPUT_VERSION 0x800
#include <dinput.h>
#include <xinput.h>
#pragma pack (pop)

/**
 * Joystick types supported by console controller emulation.	
 */
enum EJoystickType
{
	JOYSTICK_None,
	JOYSTICK_PS2_Old_Converter,
	JOYSTICK_PS2_New_Converter,
	JOYSTICK_X360,
	JOYSTICK_Xbox_Type_S
};

#define JOYSTICK_NUM_XINPUT_CONTROLLERS	4

/**
 * Information about a joystick.
 */
struct FJoystickInfo
{
	/** The DirectInput interface to this Joystick. */
	LPDIRECTINPUTDEVICE8 DirectInput8Joystick;

	/** The joystick's type. */
	EJoystickType JoystickType;

	/** The joystick's controller ID. */
	INT ControllerId;

	/** Last frame's joystick button states, so we only send events on edges */
	UBOOL JoyStates[MAX_JOYSTICK_BUTTONS];

	/** Next time a IE_Repeat event should be generated for each button */
	DOUBLE NextRepeatTime[MAX_JOYSTICK_BUTTONS];

	/** Store previous joy state. Sometimes DInput fails to query joystick, we don't want to have negative impacts such as zero acceleration */
	DIJOYSTATE2 PreviousState;

	/** Device GUID */
	GUID DeviceGUID;

	/** Whether an XInput controller is connected or not */
	UBOOL bIsConnected;
};


// Forward declarations.
struct FWindowsViewport;

//
//	UWindowsClient - Windows-specific client code.
//
class UWindowsClient : public UClient
{
	DECLARE_CLASS(UWindowsClient,UClient,CLASS_Transient|CLASS_Config,WinDrv)

	// Static variables.
	static TArray<FWindowsViewport*>	Viewports;
	static LPDIRECTINPUT8				DirectInput8;
	static LPDIRECTINPUTDEVICE8			DirectInput8Mouse;
	static TArray<FJoystickInfo>		Joysticks;
	static HANDLE						KeyboardHookThread;		// Thread that manages the low-level keyboard hook
	static DWORD						KeyboardHookThreadId;	// Thread id for the KeyboardHookThread.

	// Variables.
	UEngine*							Engine;
	FString								WindowClassName;

	TMap<BYTE,FName>					KeyMapVirtualToName;
	TMap<FName,BYTE>					KeyMapNameToVirtual;

	/** A quick lookup to map PS2 controller button indices to Xbox controller button indices */
	BYTE		PS2ToXboxControllerMapping[MAX_JOYSTICK_BUTTONS];
	/** A quick lookup to map X360 XInput controller button indices to Xbox controller button indices */
	BYTE		X360ToXboxControllerMapping[MAX_JOYSTICK_BUTTONS];
	/** An array of FNames that are sent to the input system. Maps Xbox button indices to FNames */
	FName		JoyNames[MAX_JOYSTICK_BUTTONS];

	/** Whether attached viewports process windows messages or not */
	UBOOL								ProcessWindowsMessages;

	/** Time until we check for newly connected XInput Controllers again */
	FLOAT								ControllerScanTime;

	/** Next controller to check for connection (Index into Joysticks array) */
	INT									NextControllerToCheck;

	// Render device.
	UClass*								RenderDeviceClass;
	URenderDevice*						RenderDevice;

	// Audio device.
	UClass*								AudioDeviceClass;
	UAudioDevice*						AudioDevice;

	/** Whether to allow joystick input. */
	UBOOL								bAllowJoystickInput;

	// Constructors.
	UWindowsClient();
	void StaticConstructor();

	// UObject interface.
	virtual void Serialize(FArchive& Ar);
	virtual void Destroy();
	virtual void ShutdownAfterError();

	// UClient interface.
	virtual void Init(UEngine* InEngine);
	virtual void Flush();
	virtual void Tick( FLOAT DeltaTime );
	virtual UBOOL Exec(const TCHAR* Cmd,FOutputDevice& Ar);

	/**
	 * PC only, debugging only function to prevent the engine from reacting to OS messages. Used by e.g. the script
	 * debugger to avoid script being called from within message loop (input).
	 *
	 * @param InValue	If FALSE disallows OS message processing, otherwise allows OS message processing (default)
	 */
	virtual void AllowMessageProcessing( UBOOL InValue ) { ProcessWindowsMessages = InValue; }

	virtual FViewport* CreateViewport(FViewportClient* ViewportClient,const TCHAR* Name,UINT SizeX,UINT SizeY,UBOOL Fullscreen = 0);
	virtual FChildViewport* CreateWindowChildViewport(FViewportClient* ViewportClient,void* ParentWindow,UINT SizeX=0,UINT SizeY=0);
	virtual void CloseViewport(FChildViewport* Viewport);

	virtual class URenderDevice* GetRenderDevice() { return RenderDevice; }
	virtual class UAudioDevice* GetAudioDevice() { return AudioDevice; }

	/** Function to immediately stop any force feedback vibration that might be going on this frame. */
	virtual void ForceClearForceFeedback();
	
	// Window message handling.
	static LRESULT APIENTRY StaticWndProc( HWND hWnd, UINT Message, WPARAM wParam, LPARAM lParam );
};


//
//	FWindowsViewport
//
struct FWindowsViewport: FViewport
{
	// Viewport state.
	UWindowsClient*			Client;
	HWND					Window,
							ParentWindow;
	UBOOL					InitializedRenderDevice;

	FString					Name;
	
	UINT					SizeX,
							SizeY;

	UBOOL					Fullscreen,
							Minimized,
							Maximized,
							Resizing,
							PreventCapture;

	UBOOL					KeyStates[256];
	UBOOL					bMouseHasMoved;

	// Info saved during captures and fullscreen sessions.
	POINT					PreCaptureMousePos;
	RECT					PreCaptureCursorClipRect;
	UBOOL					bCapturingMouseInput;
	UBOOL					bCapturingJoystickInput;
	UBOOL					bLockingMouseToWindow;

	enum EForceCapture		{EC_ForceCapture};

	// Constructor/destructor.
	FWindowsViewport(UWindowsClient* InClient,FViewportClient* InViewportClient,const TCHAR* InName,UINT InSizeX,UINT InSizeY,UBOOL InFullscreen,HWND InParentWindow);

	// FChildViewport interface.
	virtual void* GetWindow() { return Window; }

	virtual UBOOL CaptureMouseInput(UBOOL Capture);
	virtual void LockMouseToWindow(UBOOL bLock);
	virtual UBOOL KeyState(FName Key);

	virtual UBOOL CaptureJoystickInput(UBOOL Capture);

	virtual INT GetMouseX();
	virtual INT GetMouseY();

	virtual UBOOL MouseHasMoved() { return bMouseHasMoved; }

	virtual UINT GetSizeX() { return SizeX; }
	virtual UINT GetSizeY() { return SizeY; }

	virtual void Draw(UBOOL Synchronous);
	virtual UBOOL ReadPixels(TArray<FColor>& OutputBuffer);
	virtual void InvalidateDisplay();

	virtual void GetHitProxyMap(UINT MinX,UINT MinY,UINT MaxX,UINT MaxY,TArray<HHitProxy*>& OutMap);
	virtual void Invalidate();

	// FViewport interface.
	virtual const TCHAR* GetName() { return *Name; }
	virtual UBOOL IsFullscreen() { return Fullscreen; }
	virtual void SetName(const TCHAR* NewName);
	virtual void Resize(UINT NewSizeX,UINT NewSizeY,UBOOL NewFullscreen);
	
	// FWindowsViewport interface.
	void ShutdownAfterError();
	void ProcessInput( FLOAT DeltaTime );
	void HandlePossibleSizeChange();
	LONG ViewportWndProc(UINT Message, WPARAM wParam, LPARAM lParam);
	void Destroy();
};

#include "XnaForceFeedbackManager.h"

#define AUTO_INITIALIZE_REGISTRANTS_WINDRV \
	UWindowsClient::StaticClass(); \
	UXnaForceFeedbackManager::StaticClass();

/*-----------------------------------------------------------------------------
	Unicode support.
-----------------------------------------------------------------------------*/

#define SetClassLongX(a,b,c)						SetClassLongW(a,b,c)
#define GetClassLongX(a,b)							GetClassLongW(a,b)
#define RemovePropX(a,b)							RemovePropW(a,b)
#define DispatchMessageX(a)							DispatchMessageW(a)
#define PeekMessageX(a,b,c,d,e)						PeekMessageW(a,b,c,d,e)
#define PostMessageX(a,b,c,d)						PostMessageW(a,b,c,d)
#define SendMessageX(a,b,c,d)						SendMessageW(a,b,c,d)
#define SendMessageLX(a,b,c,d)						SendMessageW(a,b,c,(LPARAM)d)
#define SendMessageWX(a,b,c,d)						SendMessageW(a,b,(WPARAM)c,d)
#define DefWindowProcX(a,b,c,d)						DefWindowProcW(a,b,c,d)
#define CallWindowProcX(a,b,c,d,e)					CallWindowProcW(a,b,c,d,e)
#define GetWindowLongX(a,b)							GetWindowLongW(a,b)
#define SetWindowLongX(a,b,c)						SetWindowLongW(a,b,c)
#define LoadMenuIdX(i,n)							LoadMenuW(i,MAKEINTRESOURCEW(n))
#define LoadCursorIdX(i,n)							LoadCursorW(i,MAKEINTRESOURCEW(n))
#ifndef LoadIconIdX
#define LoadIconIdX(i,n)							LoadIconW(i,MAKEINTRESOURCEW(n))
#endif
#define CreateWindowExX(a,b,c,d,e,f,g,h,i,j,k,l)	CreateWindowEx(a,b,c,d,e,f,g,h,i,j,k,l)

#endif //_INC_WINDRV
