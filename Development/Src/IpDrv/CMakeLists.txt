project(IpDrv)

# set include directories
include_directories(
	Inc
	../Core/Inc
	../Engine/Inc
)

# IpDrv code
file(GLOB IPDRV_INCLUDES Inc/*.h)
file(GLOB IPDRV_SOURCES Src/*.cpp)
source_group("Inc" FILES ${IPDRV_INCLUDES})
source_group("Src" FILES ${IPDRV_SOURCES})

# build IpDrv static library
ADD_UNREAL_MODULE("ALL" "Unreal Engine" "IpDrv" "${IPDRV_SOURCES}" "${IPDRV_INCLUDES}" "Inc/UnIpDrv.h")