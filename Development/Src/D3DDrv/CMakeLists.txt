project(D3DDrv)

# set include directories
include_directories(
	Src
	../Core/Inc
	../Engine/Inc
	../WinDrv/Inc
	../../External/DirectX9/Include
)

# D3DDrv code
file(GLOB D3DDRV_INCLUDES Src/*.h)
file(GLOB D3DDRV_SOURCES Src/*.cpp)
source_group("Inc" FILES ${D3DDRV_INCLUDES})
source_group("Src" FILES ${D3DDRV_SOURCES})

# build D3DDrv static library
ADD_UNREAL_MODULE("ALL" "Unreal Engine" "D3DDrv" "${D3DDRV_SOURCES}" "${D3DDRV_INCLUDES}" "Src/D3DDrv.h")