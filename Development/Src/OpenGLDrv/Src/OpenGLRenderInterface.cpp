/*=============================================================================
	OpenGLRenderDevice.cpp: Unreal OpenGL render device implementation.
	Copyright 2004 Epic Games, Inc. All Rights Reserved.

	Revision history:
		* Reverse-engineered by Pat 'raynorpat' Raynor
=============================================================================*/

#include "OpenGLDrv.h"
#include "OpenGLDrvPrivate.h"

void FOpenGLRenderInterface::BeginScene(FOpenGLViewport* Viewport, UBOOL InHitTesting)
{
	debugf(TEXT("FOpenGLRenderInterface::BeginScene()"));

	glViewport(0, 0, Viewport->SizeX, Viewport->SizeY);
}

void FOpenGLRenderInterface::EndScene(FOpenGLViewport* Viewport, UBOOL Synchronous)
{
	debugf(TEXT("FOpenGLRenderInterface::EndScene()"));

	check(Viewport->Context && Viewport->Context->DeviceContext);

	UBOOL Result = SwapBuffers(Viewport->Context->DeviceContext);
	if (!Result)
		appErrorf(TEXT("SwapBuffers(GOpenGLRenderDevice->Context.DeviceContext) failed"));
}

void FOpenGLRenderInterface::Clear(const FColor& Color)
{
	check(!GOpenGLSceneRenderer);

	//FlushBatchedPrimitives();

//	if(!HitTesting)
//	{
//		GDirect3DDevice->Clear(0,NULL,D3DCLEAR_TARGET,D3DCOLOR_RGBA(Color.R,Color.G,Color.B,Color.A),1.0f,0);
//	}

	glClearColor(Color.R, Color.G, Color.B, Color.A);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void FOpenGLRenderInterface::DrawScene(const FSceneContext& Context)
{
	debugf(TEXT("FOpenGLRenderInterface::DrawScene()"));

	glClearColor(0.2f, 0.4f, 0.6f, 1.f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void FOpenGLRenderInterface::DrawLine2D(INT X1,INT Y1,INT X2,INT Y2,FColor Color)
{
	X1 = (X1 * Zoom2D) + Origin2D.X;
	Y1 = (Y1 * Zoom2D) + Origin2D.Y;
	X2 = (X2 * Zoom2D) + Origin2D.X;
	Y2 = (Y2 * Zoom2D) + Origin2D.Y;

	/*
	glColor4fv(&Color.R);

	glBegin(GL_LINES);
	glVertex3f(X1, Y1, 0.0f);
	glVertex3f(X2, Y2, 0.0f);
	glEnd();
	*/
}

void FOpenGLRenderInterface::DrawTile(INT X,INT Y,UINT SizeX,UINT SizeY,FLOAT U,FLOAT V,FLOAT W,FLOAT SizeU,FLOAT SizeV,const FLinearColor& Color,FTexture3D* Texture,UBOOL AlphaBlend)
{
}

void FOpenGLRenderInterface::DrawTile(INT X,INT Y,UINT SizeX,UINT SizeY,FLOAT U,FLOAT V,FLOAT SizeU,FLOAT SizeV,const FLinearColor& Color,FTexture2D* Texture,UBOOL AlphaBlend)
{
}

void FOpenGLRenderInterface::DrawTile(INT X,INT Y,UINT SizeX,UINT SizeY,FLOAT U,FLOAT V,FLOAT SizeU,FLOAT SizeV,FMaterial* Material,FMaterialInstance* MaterialInstance)
{
}

void FOpenGLRenderInterface::DrawTriangle2D(
		const FIntPoint& Vert0, FLOAT U0, FLOAT V0, 
		const FIntPoint& Vert1, FLOAT U1, FLOAT V1, 
		const FIntPoint& Vert2, FLOAT U2, FLOAT V2,
		const FLinearColor& Color, FTexture2D* Texture, UBOOL AlphaBlend)
{
}

void FOpenGLRenderInterface::DrawTriangle2D(
	const FIntPoint& Vert0, FLOAT U0, FLOAT V0, 
	const FIntPoint& Vert1, FLOAT U1, FLOAT V1, 
	const FIntPoint& Vert2, FLOAT U2, FLOAT V2,
	FMaterial* Material, FMaterialInstance* MaterialInstance)
{
}

void FOpenGLRenderInterface::SetHitProxy(HHitProxy* Hit)
{
}
