#pragma once

// OpenGL includes.
#pragma pack(push,8)

#ifdef _WIN32
#include <windows.h>
#endif

#ifdef MACOSX
    #include <OpenGL/gl.h>
    #include <OpenGL/glext.h>
#else
    #include <GL/gl.h>
    #include <GL/glext.h>
    #ifdef _WIN32
        #include <GL/wglext.h>
    #endif
#endif

#pragma pack(pop)
