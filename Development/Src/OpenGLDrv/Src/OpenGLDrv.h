/*=============================================================================
	OpenGLDrv.h: Unreal OpenGL driver precompiled header generator.
	Copyright 1997-2004 Epic Games, Inc. All Rights Reserved.

	Revision history:
		* Created by Pat 'raynorpat' Raynor
=============================================================================*/

// Unreal includes.
#include "Engine.h"

//
//	Definitions and enumerations.
//

#define MAX_TEXTURES			16

//
//	Forward declarations.
//

class UOpenGLRenderDevice;

//
//	Includes.
//

#include "OpenGLStats.h"
#include "OpenGLScene.h"
#include "OpenGLRenderInterface.h"
#include "OpenGLRenderDevice.h"

//
//	Globals.
//

extern UOpenGLRenderDevice*			GOpenGLRenderDevice;
extern FOpenGLRenderInterface		GOpenGLRenderInterface;
extern FOpenGLSceneRenderer*		GOpenGLSceneRenderer;
extern FOpenGLStatGroup				GOpenGLStats;

/*-----------------------------------------------------------------------------
	End.
-----------------------------------------------------------------------------*/
