/*=============================================================================
	OpenGLRenderDevice.cpp: Unreal OpenGL render device implementation.
	Copyright 2004 Epic Games, Inc. All Rights Reserved.

	Revision history:
		* Reverse-engineered by Pat 'raynorpat' Raynor
=============================================================================*/

#include "OpenGLDrv.h"
#include "OpenGLDrvPrivate.h"

/*-----------------------------------------------------------------------------
	Constants & static stuff.
-----------------------------------------------------------------------------*/

#if (defined WIN32)
	#define GL_DLL (TEXT("OpenGL32.dll"))
#elif ((__linux__) || (__FreeBSD__))
	#define GL_DLL (TEXT("libGL.so.1"))
#elif MACOSX
	#define GL_DLL (TEXT("libGL.dylib"))   // !!! FIXME: probably wrong.
#else
	#error Fill this in for your platform.
#endif

// OpenGL function pointers.
#define GL_EXT(name) UBOOL UOpenGLRenderDevice::SUPPORTS##name=0;
#define GL_PROC(ext,ret,func,parms) ret (STDCALL *UOpenGLRenderDevice::func)parms;
#include "OpenGLFuncs.h"
#undef GL_EXT
#undef GL_PROC

/*-----------------------------------------------------------------------------
	OpenGLRenderDevice.
-----------------------------------------------------------------------------*/

IMPLEMENT_CLASS(UOpenGLRenderDevice);

//
// FOpenGLContext
//

FOpenGLContext::FOpenGLContext() :
	Window(NULL),
	WindowClassName(NULL),
	DeviceContext(NULL),
	RenderingContext(NULL),
	bDestroyWindowOnRelease(0)
{
}

FOpenGLContext::~FOpenGLContext()
{
	if (RenderingContext)
	{
#ifdef _WIN32
		if (wglGetCurrentContext() == RenderingContext)
			wglMakeCurrent(NULL, NULL);

		wglDeleteContext(RenderingContext);
#else
	#error Please handle this for your platform.
#endif
	}

	if (DeviceContext)
	{
#ifdef _WIN32
		ReleaseDC(Window, DeviceContext);
#else
	#error Please handle this for your platform.
#endif
	}

	if (Window && bDestroyWindowOnRelease)
	{
#ifdef _WIN32
		DestroyWindow(Window);
		UnregisterClass(WindowClassName, NULL);
#else
	#error Please handle this for your platform.
#endif
	}
}

void FOpenGLContext::InitPixelFormat()
{
	check(DeviceContext);

#ifdef _WIN32
	// Pixel format descriptor for the context.
	PIXELFORMATDESCRIPTOR PixelFormatDesc;
	appMemzero(&PixelFormatDesc, sizeof(PixelFormatDesc));
	PixelFormatDesc.nSize		= sizeof(PIXELFORMATDESCRIPTOR);
	PixelFormatDesc.nVersion	= 1;
	PixelFormatDesc.dwFlags		= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	PixelFormatDesc.iPixelType	= PFD_TYPE_RGBA;
	PixelFormatDesc.cColorBits	= 32;
	PixelFormatDesc.cDepthBits	= 24;
	PixelFormatDesc.cStencilBits = 8;
	PixelFormatDesc.iLayerType	= PFD_MAIN_PLANE;

	// Stereo
//  PixelFormatDesc.dwFlags = PixelFormatDesc.dwFlags | PFD_STEREO;

	INT32 PixelFormat = ChoosePixelFormat(DeviceContext, &PixelFormatDesc);
	if (!PixelFormat || !SetPixelFormat(DeviceContext, PixelFormat, &PixelFormatDesc))
		appErrorf(TEXT("Failed to set pixel format for device context."));
#elif USE_SDL
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
	SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 24);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
#else
	#error Please handle this for your platform.
#endif
}

//
// FOpenGLViewport
//

#ifdef WIN32
LRESULT CALLBACK FOpenGLViewport::DummyWndproc(HWND hWnd, UINT32 Message, WPARAM wParam, LPARAM lParam)
{
	return DefWindowProc(hWnd, Message, wParam, lParam);
}
#endif

FOpenGLViewport::FOpenGLViewport(HWND Window, FChildViewport* InViewport):
	Viewport(InViewport)
{
#ifdef _WIN32
	// Create our OpenGL context
	debugf(TEXT("OpenGLRenderDevice: Creating OpenGL context"));

	Context = new FOpenGLContext();
	Context->Window = Window;

	Context->DeviceContext = GetDC(Context->Window);
	check(Context->DeviceContext);

	Context->InitPixelFormat();

	// TODO: Device gamma support. Should supply RenderDevice reference.
	//DeviceSupportsGamma = GetDeviceGammaRamp (Context->DeviceContext, DeviceGammaRamp) != 0;

	Context->RenderingContext = wglCreateContext(Context->DeviceContext);
	if (!Context->RenderingContext)
		appErrorf(TEXT("wglCreateContext() failed for OpenGL (not supported by driver)"));

	UBOOL Result = wglMakeCurrent(Context->DeviceContext, Context->RenderingContext);
	if (!Result)
		appErrorf(TEXT("wglMakeCurrent() failed for the OpenGL context"));
#else
	#error Please handle this for your platform.
#endif
}

FOpenGLViewport::~FOpenGLViewport()
{
	if (Context)
		delete Context;
}

//
// UOpenGLRenderDevice
//

// Constructor
UOpenGLRenderDevice::UOpenGLRenderDevice():
	bGLInitialized(0)
{
	GOpenGLRenderDevice = this;
}

//
// Helper functions
//

UBOOL UOpenGLRenderDevice::FindExt( const TCHAR* Name )
{
	if (strcmp(TCHAR_TO_ANSI(Name), "GL") == 0)
		return true;

	// Can't use appStrFind as extension string might be > 1024 characters.
    UBOOL Result = strstr( (char*)glGetString(GL_EXTENSIONS), TCHAR_TO_ANSI(Name) ) != NULL;
	if( Result )
		debugf( TEXT("OpenGL: Device supports: %s"), Name );
	return Result;
}

void UOpenGLRenderDevice::FindProc( void*& ProcAddress, char* Name, char* SupportName, UBOOL& Supports, UBOOL AllowExt )
{
#if (defined USE_SDL)
	ProcAddress = (void*)SDL_GL_GetProcAddress( Name );
#elif (defined WIN32)
	ProcAddress = wglGetProcAddress( Name );
#else
    #error Please handle this for your platform.
#endif

	if ( !ProcAddress )
	{
		if ( Supports )
			debugf( TEXT("   Missing function '%s' for '%s' support"), ANSI_TO_TCHAR(Name), ANSI_TO_TCHAR(SupportName) );
		else
			debugf( TEXT("   Missing function '%s'"), ANSI_TO_TCHAR(Name) );
		Supports = 0;
	}
}

void UOpenGLRenderDevice::FindProcs( UBOOL AllowExt )
{
	#define GL_EXT(name) if( AllowExt ) SUPPORTS##name = FindExt( TEXT(#name)+1 );
	#define GL_PROC(ext,ret,func,parms) FindProc( *(void**)&func, #func, #ext, SUPPORTS##ext, AllowExt );
	#include "OpenGLFuncs.h"
	#undef GL_EXT
	#undef GL_PROC
}

//
// FResourceClient interface.
//

void UOpenGLRenderDevice::FreeResource(FResource* Resource)
{
//	for(UINT SetIndex = 0;SetIndex < (UINT)ResourceSets.Num();SetIndex++)
//		ResourceSets(SetIndex)->FreeResource(Resource);
}

void UOpenGLRenderDevice::UpdateResource(FResource* Resource)
{
/*
	FOpenGLResource* OpenGLResource = Resources.ResourceMap.FindRef(Resource->ResourceIndex);
	
	if( Resource->Type()->IsA(FTexture2D::StaticType) )
	{	
		FTexture2D*	Texture2D = (FTexture2D*) Resource;
		FOpenGLTexture2D* OpenGLTexture2D = (FOpenGLTexture2D*) OpenGLResource;
		if(OpenGLTexture2D)
			OpenGLTexture2D->UpdateTexture(Texture2D);
	}
	else if( Resource->Type()->IsA(FTextureCube::StaticType) )
	{
		FTextureCube* TextureCube = (FTextureCube*) Resource;
		FOpenGLTextureCube* OpenGLTextureCube = (FOpenGLTextureCube*) OpenGLResource;
		if(OpenGLTextureCube)
			OpenGLTextureCube->UpdateTexture(TextureCube);
	}
	else if( Resource->Type()->IsA(FTexture3D::StaticType) )
	{
		FTexture3D*	Texture3D = (FTexture3D*) Resource;
		FOpenGLTexture3D* OpenGLTexture3D = (FOpenGLTexture3D*) OpenGLResource;
		if(OpenGLTexture3D)
			OpenGLTexture3D->UpdateTexture(Texture3D);
	}
	else if( Resource->Type()->IsA(FVertexBuffer::StaticType))
	{
		FVertexBuffer* VertexBuffer = (FVertexBuffer*)Resource;
		FOpenGLVertexBuffer* OpenGLVertexBuffer = (FOpenGLVertexBuffer*)OpenGLResource;
		if(OpenGLVertexBuffer)
		{
			if(OpenGLVertexBuffer->Size != VertexBuffer->Size)
				FreeResource(VertexBuffer);
			else
				OpenGLVertexBuffer->Invalidate();
		}
	}
	else
	{
		FreeResource(Resource);
	}
*/
}

void UOpenGLRenderDevice::RequestMips(FTextureBase* Texture, UINT RequestedMips, FTextureMipRequest* TextureMipRequest)
{
}

void UOpenGLRenderDevice::FinalizeMipRequest(FTextureMipRequest* TextureMipRequest)
{
}

void UOpenGLRenderDevice::GLError( TCHAR* Tag )
{
	GLenum Error = glGetError();
    while( Error != GL_NO_ERROR )
	{
		const TCHAR* Msg;
		switch( Error )
		{
			case GL_INVALID_ENUM:
				Msg = TEXT("GL_INVALID_ENUM");
				break;
			case GL_INVALID_VALUE:
				Msg = TEXT("GL_INVALID_VALUE");
				break;
			case GL_INVALID_OPERATION:
				Msg = TEXT("GL_INVALID_OPERATION");
				break;
			case GL_STACK_OVERFLOW:
				Msg = TEXT("GL_STACK_OVERFLOW");
				break;
			case GL_STACK_UNDERFLOW:
				Msg = TEXT("GL_STACK_UNDERFLOW");
				break;
			case GL_OUT_OF_MEMORY:
				Msg = TEXT("GL_OUT_OF_MEMORY");
				break;
			default :
				Msg = TEXT("UNKNOWN");
		};
		debugf( TEXT("OpenGL Error: %s (%s)"), Msg, Tag );
	}
}

//
// UObject interface.
//

void UOpenGLRenderDevice::StaticConstructor()
{
}

void UOpenGLRenderDevice::Destroy()
{
	// Deregister the resource client.
	GResourceManager->DeregisterClient( this );

	Super::Destroy();
}

void UOpenGLRenderDevice::PostEditChange( UProperty* PropertyThatChanged )
{
	Super::PostEditChange( PropertyThatChanged );
	UpdateRenderOptions();
}

void UOpenGLRenderDevice::Serialize( FArchive& Ar )
{
	Super::Serialize( Ar );

	if( !Ar.IsLoading() && !Ar.IsSaving() )
	{
		// Make sure objects referenced by hit proxies aren't garbage collected.
		for( INT ProxyIndex = 0; ProxyIndex < HitProxies.Num(); ProxyIndex++ )
			if( HitProxies(ProxyIndex) )
				HitProxies(ProxyIndex)->Serialize( Ar );
	}
}

//
// FExec interface.
//

UBOOL UOpenGLRenderDevice::Exec( const TCHAR* Cmd, FOutputDevice& Ar )
{
	return FALSE;
}

//
// URenderDevice interface.
//

void UOpenGLRenderDevice::Init()
{
	// Register the resource client.
	GResourceManager->RegisterClient(this);
}

void UOpenGLRenderDevice::Flush()
{
	// Ensure that all resources currently locked by the streaming code are unlocked.
	GStreamingManager->Flush(TRUE);

	InvalidateHitProxyCache();

	// Free the render targets.

	// Disassociate cached resources from their sources and free the unreferenced resources.

	// TODO: Free GL resources.
	//for(UINT SetIndex = 0;SetIndex < (UINT)ResourceSets.Num();SetIndex++)
		//ResourceSets(SetIndex)->Flush();

	// TODO: Delete textures, vertex buffers, programs, etc.
}

void UOpenGLRenderDevice::CreateViewport(FChildViewport* Viewport)
{
	HWND Window;

	// Choose window for this viewport's GL context.
	// TODO: This is gross. Viewport should already have its Window set properly by platform client. Investigate & fix.
	if (Viewport->IsFullscreen())
	{
		// There can be only one fullscreen viewport
		check(!FullscreenViewport);

		Window = (HWND)Viewport->GetWindow();

		//
		// Change display mode
		//
#ifdef _WIN32
		// Get the monitor info from the window handle.
		HMONITOR hMonitor = MonitorFromWindow(Window, MONITOR_DEFAULTTOPRIMARY);
		MONITORINFOEX MonitorInfo;
		memset(&MonitorInfo, 0, sizeof(MONITORINFOEX));
		MonitorInfo.cbSize = sizeof(MONITORINFOEX);
		GetMonitorInfo(hMonitor, &MonitorInfo);

		DEVMODE Mode;
		Mode.dmSize = sizeof(DEVMODE);
		Mode.dmBitsPerPel = 32;
		Mode.dmPelsWidth = Viewport->GetSizeX();
		Mode.dmPelsHeight = Viewport->GetSizeY();
		Mode.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Turn on fullscreen mode for the current monitor
		ChangeDisplaySettingsEx(MonitorInfo.szDevice, &Mode, NULL, CDS_FULLSCREEN, NULL);

		//WindowStyle = WS_POPUP;
		//WindowStyleEx = WS_EX_APPWINDOW | WS_EX_TOPMOST;
		//InsertAfter = HWND_TOPMOST;

		// Vertical synchronization
		//if (bHasSwapIntervalExtension)
			wglSwapIntervalEXT(0);
#else
	#error Please handle this for your platform.
#endif
	}
	else
	{
		if (Viewports.Num())
			Window = (HWND)Viewports(0)->Viewport->GetWindow();
		else
			Window = (HWND)Viewport->GetWindow();
	}

	// Create new GL viewport
	FOpenGLViewport* OpenGLViewport = new FOpenGLViewport(Window, Viewport);
	Viewports.AddItem(OpenGLViewport);

/*
	// Find the maximum viewport dimensions and any fullscreen viewports.
	FOpenGLViewport*	NewFullscreenViewport = NULL;
	UINT			MaxViewportSizeX = 0,
					MaxViewportSizeY = 0,
					MaxHitViewportSizeX = 0,
					MaxHitViewportSizeY = 0;

	for(INT ViewportIndex = 0;ViewportIndex < Viewports.Num();ViewportIndex++)
	{
		FOpenGLViewport* OpenGLViewport = Viewports(ViewportIndex);

		MaxViewportSizeX = Max(MaxViewportSizeX,OpenGLViewport->SizeX);
		MaxViewportSizeY = Max(MaxViewportSizeY,OpenGLViewport->SizeY);

		if(OpenGLViewport->Viewport->ViewportClient && OpenGLViewport->Viewport->ViewportClient->RequiresHitProxyStorage())
		{
			MaxHitViewportSizeX = Max(MaxHitViewportSizeX,OpenGLViewport->SizeX);
			MaxHitViewportSizeY = Max(MaxHitViewportSizeY,OpenGLViewport->SizeY);
		}

		if(OpenGLViewport->Viewport->IsFullscreen())
		{
			check(!NewFullscreenViewport);
			NewFullscreenViewport = OpenGLViewport;
		}
	}

	HWND NewWindow = NewFullscreenViewport ? (HWND)NewFullscreenViewport->Viewport->GetWindow() : (HWND)Viewports(0)->Viewport->GetWindow();
	UINT SizeX = NewFullscreenViewport ? NewFullscreenViewport->SizeX : MaxViewportSizeX,
		SizeY = NewFullscreenViewport ? NewFullscreenViewport->SizeY : MaxViewportSizeY;
*/
	// Update hit proxy buffer
//	HitProxyBufferSizeX = MaxHitViewportSizeX;
//	HitProxyBufferSizeY = MaxHitViewportSizeY;

	if (Viewport->IsFullscreen())
		FullscreenViewport = OpenGLViewport;

	// Get function pointers
	debugf(TEXT("OpenGLRenderDevice: Getting function pointers"));

	// Retrieve the OpenGL library
#ifdef _WIN32
	void* OpenGLDLL = appGetDllHandle(GL_DLL);
	if( !OpenGLDLL )
	{
		appErrorf(TEXT("Couldn't load OpenGL library"));
	}
#elif (defined USE_SDL)
	FString OpenGLLibName = GL_DLL;
	if( SDL_GL_LoadLibrary( TCHAR_TO_ANSI(*OpenGLLibName) ) == -1 )
	{
		appErrorf( ANSI_TO_TCHAR(SDL_GetError()) );
	}
#else
	#error Please handle this for your platform.
#endif

	// Get info and extensions.
	FindProcs( 1 );

	debugf(TEXT("GL_VENDOR     : %s"), ANSI_TO_TCHAR((ANSICHAR*)glGetString(GL_VENDOR)));
	debugf(TEXT("GL_RENDERER   : %s"), ANSI_TO_TCHAR((ANSICHAR*)glGetString(GL_RENDERER)));
	debugf(TEXT("GL_VERSION    : %s"), ANSI_TO_TCHAR((ANSICHAR*)glGetString(GL_VERSION)));

	INT DepthBits,
		StencilBits,
		RedBits,
		GreenBits,
		BlueBits,
		AlphaBits;

	glGetIntegerv(GL_DEPTH_BITS, &DepthBits);
	glGetIntegerv(GL_STENCIL_BITS, &StencilBits);
	glGetIntegerv(GL_RED_BITS, &RedBits);
	glGetIntegerv(GL_GREEN_BITS, &GreenBits);
	glGetIntegerv(GL_BLUE_BITS, &BlueBits);
	glGetIntegerv(GL_ALPHA_BITS, &AlphaBits);

	debugf(TEXT("OpenGL: RGB%i%i%i Z%i S%i"), RedBits, GreenBits, BlueBits, DepthBits, StencilBits);

	/*
	if ( SUPPORTS_GL_EXT_texture_filter_anisotropic )
	{
		GLfloat MaxAnisotropy;
		glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &MaxAnisotropy);
		LevelOfAnisotropy = Max<FLOAT>(1.0f, Min<FLOAT>(MaxAnisotropy, LevelOfAnisotropy));
		debugf(TEXT("OpenGL: Level of anisotropy is %f (max %f)."), (float)LevelOfAnisotropy, (float)MaxAnisotropy);
	}
	*/

	// Disable vsync.
	/*
	if ( SUPPORTS_WGL_EXT_swap_control )
		wglSwapIntervalEXT( !GIsBenchmarking && UseVSync ? 1 : 0 );
	*/

	// Set permanent state.
	glDisable(GL_NORMALIZE);
	glDisable(GL_BLEND);
	glDisable(GL_DITHER);
	glDisable(GL_CULL_FACE);
	glDisable(GL_LIGHTING);
	glDisable(GL_COLOR_MATERIAL);
	glDisable(GL_FOG);
	glDisable(GL_MINMAX);
	glDisable(GL_RESCALE_NORMAL);
	glDisable(GL_TEXTURE_1D);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_TEXTURE_3D);
	glDisable(GL_DEPTH_TEST);

	glEnable(GL_CULL_FACE);
	glFrontFace(GL_CCW);
	glCullFace(GL_BACK);
	glDepthFunc(GL_LEQUAL);

	glEnableClientState(GL_VERTEX_ARRAY);

	glViewport(0, 0, Viewport->GetSizeX(), Viewport->GetSizeY());

	glEnable(GL_SCISSOR_TEST);
	glScissor(0, 0, Viewport->GetSizeX(), Viewport->GetSizeY());

	// Update any render options set in the user's INI files
	UpdateRenderOptions();
}

void UOpenGLRenderDevice::ResizeViewport(FChildViewport* Viewport)
{
	DestroyViewport(Viewport);
	CreateViewport(Viewport);
}

void UOpenGLRenderDevice::DestroyViewport(FChildViewport* Viewport)
{
	INT	ViewportIndex = GetViewportIndex(Viewport);
	check(ViewportIndex != INDEX_NONE);

	if(FullscreenViewport == Viewports(ViewportIndex))
	{
		// Restore desktop resolution
		ChangeDisplaySettings(NULL, 0);
		FullscreenViewport = NULL;
	}

	// ~FOpenGLViewport() deletes context
	delete Viewports(ViewportIndex);
	Viewports.Remove(ViewportIndex);

	InvalidateHitProxies(Viewport);

	// If no viewports are open, clean up the existing stuff.
	if (!Viewports.Num() && bGLInitialized)
	{
		Flush();
		bGLInitialized = false;
	}
}

void UOpenGLRenderDevice::DrawViewport(FChildViewport* Viewport, UBOOL Synchronous)
{
	debugf(TEXT("DrawViewport()"));

	INT	ViewportIndex = GetViewportIndex(Viewport);
	check(ViewportIndex != INDEX_NONE);

	if (FullscreenViewport && Viewports(ViewportIndex) != FullscreenViewport)
		return;

	FOpenGLViewport *OpenGLViewport = Viewports(ViewportIndex);
	
	GOpenGLRenderInterface.BeginScene(OpenGLViewport, 0);

	if (Viewport->ViewportClient)
		Viewport->ViewportClient->Draw(Viewport, &GOpenGLRenderInterface);

	GOpenGLRenderInterface.EndScene(OpenGLViewport, Synchronous);
}

void UOpenGLRenderDevice::ReadPixels(FChildViewport* Viewport, FColor* OutputBuffer)
{
	// Find the corresponding OpenGL viewport.
	INT	ViewportIndex = GetViewportIndex(Viewport);
	check(ViewportIndex != INDEX_NONE);

	FOpenGLViewport* GLViewport = Viewports(ViewportIndex);
	if (FullscreenViewport && GLViewport != FullscreenViewport)
		return;

	// Copy the back buffer to the output buffer.
	glReadPixels( 0, 0, GLViewport->SizeX, GLViewport->SizeY, GL_RGBA, GL_UNSIGNED_BYTE, OutputBuffer );

	// Flip B and R
	for (UINT i = 0; i < GLViewport->SizeY / 2; i++)
	{
		for (UINT j = 0; j < GLViewport->SizeX; j++)
		{
			Exchange(OutputBuffer[j + i * GLViewport->SizeX].R, OutputBuffer[j + (GLViewport->SizeY - 1 - i) * GLViewport->SizeX].B);
			Exchange(OutputBuffer[j + i * GLViewport->SizeX].G, OutputBuffer[j + (GLViewport->SizeY - 1 - i) * GLViewport->SizeX].G);
			Exchange(OutputBuffer[j + i * GLViewport->SizeX].B, OutputBuffer[j + (GLViewport->SizeY - 1 - i) * GLViewport->SizeX].R);
		}
	}
}

void UOpenGLRenderDevice::UpdateRenderOptions()
{
}

void UOpenGLRenderDevice::GetHitProxyMap(FChildViewport* Viewport, UINT MinX, UINT MinY, UINT MaxX, UINT MaxY, TArray<HHitProxy*>& OutMap)
{
	check(Viewport->ViewportClient && Viewport->ViewportClient->RequiresHitProxyStorage());

	INT	ViewportIndex = GetViewportIndex(Viewport);
	check(ViewportIndex != INDEX_NONE);
	FOpenGLViewport*	OpenGLViewport = Viewports(ViewportIndex);

	MinX = Clamp<UINT>(MinX, 0, OpenGLViewport->SizeX-1);
	MinY = Clamp<UINT>(MinY, 0, OpenGLViewport->SizeY-1);
	MaxX = Clamp<UINT>(MaxX, 0, OpenGLViewport->SizeX-1);
	MaxY = Clamp<UINT>(MaxY, 0, OpenGLViewport->SizeY-1);

	UINT	SizeX = MaxX - MinX + 1,
			SizeY = MaxY - MinY + 1;

	OutMap.Empty(SizeX * SizeY);
	OutMap.AddZeroed(SizeX * SizeY);

	if(FullscreenViewport && Viewports(ViewportIndex) != FullscreenViewport)
		return;

	if(CachedHitProxyViewport != Viewport)
	{
		// Cache the hit proxy used by each pixel in the viewport.

		InvalidateHitProxyCache();

		GOpenGLRenderInterface.BeginScene(OpenGLViewport, 1);
		if(Viewport->ViewportClient)
			Viewport->ViewportClient->Draw(Viewport, &GOpenGLRenderInterface);
		GOpenGLRenderInterface.EndScene(OpenGLViewport, 0);

		CachedHitProxyViewport = Viewport;
	}

	/*
	if(DumpHitProxies)
	{
		// Dump the entire hit proxy buffer to a file.

		SaveRenderTarget(HitProxyBuffer,TEXT("HitProxies"));
		DumpHitProxies = 0;
	}
	*/

	// Read back the hit proxy buffer from (MinX,MinY) to (MaxX,MaxY)
	/*
	D3DLOCKED_RECT	LockedRect;
	RECT			HitRect;
	HitRect.left = MinX;
	HitRect.top = MinY;
	HitRect.right = MaxX + 1;
	HitRect.bottom = MaxY + 1;
	HRESULT	Result = HitProxyBuffer->LockRect(&LockedRect,&HitRect,D3DLOCK_READONLY);
	if(FAILED(Result))
		appErrorf(TEXT("Failed to lock hit proxy buffer: %s"),*D3DError(Result));

	for(UINT Y = MinY;Y <= MaxY;Y++)
	{
		DWORD*	SrcPtr = (DWORD*)((BYTE*)LockedRect.pBits + (Y - MinY) * LockedRect.Pitch);
		for(UINT X = MinX;X <= MaxX;X++)
		{
			DWORD	HitProxyIndex = *SrcPtr++ & 0x00ffffff;
			if(HitProxyIndex < (DWORD)HitProxies.Num())
				OutMap(((Y - MinY) * SizeX) + X - MinX) = HitProxies(HitProxyIndex);
		}
	}

	HitProxyBuffer->UnlockRect();
	*/
}

void UOpenGLRenderDevice::InvalidateHitProxyCache()
{
	for(INT ProxyIndex = 0;ProxyIndex < HitProxies.Num();ProxyIndex++)
		delete HitProxies(ProxyIndex);

	HitProxies.Empty(1);
	CachedHitProxyViewport = NULL;
	HitProxies.AddItem(NULL);
}

void UOpenGLRenderDevice::InvalidateHitProxies(FChildViewport* Viewport)
{
	if(Viewport == CachedHitProxyViewport)
		InvalidateHitProxyCache();
}

//
// FMaterial stuff
//

TArray<FMaterialError> UOpenGLRenderDevice::CompileMaterial(FMaterial* Material)
{
	TArray<FMaterialError>	Errors;
	return Errors;
}

//
// FPrecacheInterface interface
//

void UOpenGLRenderDevice::CacheResource(FResource* Resource)
{
/*
	if(Resource->Type()->IsA(FMaterial::StaticType))
		CompileMaterial((FMaterial*)Resource);
	else
		Resources.GetCachedResource(Resource);
*/
}
