#include "OpenGLDrv.h"
#include "OpenGLDrvPrivate.h"

FOpenGLSceneRenderer::FOpenGLSceneRenderer(const FSceneContext& InContext) :
	Scene(InContext.Scene),
	View(InContext.View),
	Context(InContext),
//	LastLightingBuffer(0),
//	NewLightingBuffer(0),
//	LastLightingLayerFinished(0),
//	LastBlurBuffer(0),
	TwoSidedSign(1.f)
{
}

FOpenGLSceneRenderer::~FOpenGLSceneRenderer()
{
}

void FOpenGLSceneRenderer::Render()
{
}

void FOpenGLSceneRenderer::RenderHitProxies()
{
}

