/*=============================================================================
	OpenGLRenderInterface.h: Unreal OpenGL render interface definition.
	Copyright 1997-2004 Epic Games, Inc. All Rights Reserved.

	Revision history:
		* Reverse-engineered by Pat 'raynorpat' Raynor
=============================================================================*/

//
//	FOpenGLRenderInterface
//

class FOpenGLRenderInterface : public FRenderInterface
{
public:

	// Variables.
	struct FOpenGLViewport*		CurrentViewport;
	UBOOL						HitTesting;
	DWORD						CurrentHitProxyIndex;

	// Constructor.
	FOpenGLRenderInterface():
		FRenderInterface(),
		CurrentViewport(NULL),
		HitTesting(0),
		CurrentHitProxyIndex(-1)
	{}

	void BeginScene(FOpenGLViewport* Viewport, UBOOL InHitTesting = 0);
	void EndScene(FOpenGLViewport* Viewport, UBOOL Synchronous);

	// FRenderInterface interface.
	virtual void Clear(const FColor& Color);

	virtual void DrawScene(const FSceneContext& Context);

	virtual void DrawLine2D(INT X1,INT Y1,INT X2,INT Y2,FColor Color);

	virtual void DrawTile(INT X,INT Y,UINT SizeX,UINT SizeY,FLOAT U,FLOAT V,FLOAT W,FLOAT SizeU,FLOAT SizeV,const FLinearColor& Color,FTexture3D* Texture = NULL,UBOOL AlphaBlend = 1);
	virtual void DrawTile(INT X,INT Y,UINT SizeX,UINT SizeY,FLOAT U,FLOAT V,FLOAT SizeU,FLOAT SizeV,const FLinearColor& Color,FTexture2D* Texture = NULL,UBOOL AlphaBlend = 1);
	virtual void DrawTile(INT X,INT Y,UINT SizeX,UINT SizeY,FLOAT U,FLOAT V,FLOAT SizeU,FLOAT SizeV,FMaterial* Material,FMaterialInstance* MaterialInstance);

	virtual void DrawTriangle2D(
		const FIntPoint& Vert0, FLOAT U0, FLOAT V0, 
		const FIntPoint& Vert1, FLOAT U1, FLOAT V1, 
		const FIntPoint& Vert2, FLOAT U2, FLOAT V2,
		const FLinearColor& Color, FTexture2D* Texture = NULL, UBOOL AlphaBlend = 1);
	virtual void DrawTriangle2D(
		const FIntPoint& Vert0, FLOAT U0, FLOAT V0, 
		const FIntPoint& Vert1, FLOAT U1, FLOAT V1, 
		const FIntPoint& Vert2, FLOAT U2, FLOAT V2,
		FMaterial* Material,FMaterialInstance* MaterialInstance);

	virtual void SetHitProxy(HHitProxy* Hit);
	virtual UBOOL IsHitTesting() { return HitTesting; }
};
