/*=============================================================================
	OpenGLRenderDevice.h: Unreal OpenGL render device definition.
	Copyright 2004 Epic Games, Inc. All Rights Reserved.

	Revision history:
		* Reverse-engineered by Pat 'raynorpat' Raynor
=============================================================================*/

#include "OpenGLDrvPrivate.h"

//
//	FOpenGLContext
//

struct FOpenGLContext
{
#ifdef WIN32
	HWND							Window;
	const TCHAR *					WindowClassName;
	HDC								DeviceContext;
	HGLRC							RenderingContext;
#else
	// TODO: Some SDL stuff.
#endif

	// FALSE for real context, TRUE for dummy one
	UBOOL							bDestroyWindowOnRelease;

	FOpenGLContext();
	~FOpenGLContext();

	// Default initializer
	void InitPixelFormat();
};

//
//	FOpenGLViewport
//

struct FOpenGLViewport
{
private:
#ifdef WIN32
	static LRESULT CALLBACK DummyWndproc(HWND hWnd, UINT32 Message, WPARAM wParam, LPARAM lParam);
#endif

public:
	FChildViewport*					Viewport;
	UINT							SizeX,
									SizeY;

	FOpenGLContext *				Context;

	FOpenGLViewport(HWND Window, FChildViewport* InViewport);
	~FOpenGLViewport();
};

//
//	UOpenGLRenderDevice
//

class UOpenGLRenderDevice : public URenderDevice, FResourceClient, FPrecacheInterface
{
	DECLARE_CLASS(UOpenGLRenderDevice,URenderDevice,CLASS_Config,OpenGLDrv);

public:
	// Configuration


	// Hit detection
//	TOpenGLRef<IDirect3DSurface9>	HitProxyBuffer;
//	UINT						HitProxyBufferSizeX,
//								HitProxyBufferSizeY;
	TArray<HHitProxy*>				HitProxies;
	FChildViewport*					CachedHitProxyViewport;

	// Variables
	TArray<FOpenGLViewport*>		Viewports;
	FOpenGLViewport*				FullscreenViewport;

	UBOOL							bGLInitialized;
	
	// GL functions
	#define GL_EXT(name) static UBOOL SUPPORTS##name;
	#define GL_PROC(ext,ret,func,parms) static ret (STDCALL *func)parms;
	#include "OpenGLFuncs.h"
	#undef GL_EXT
	#undef GL_PROC

	// Constructor.
	UOpenGLRenderDevice();

	// Helper functions.
	virtual UBOOL FindExt( const TCHAR* Name );
	virtual void FindProc( void*& ProcAddress, char* Name, char* SupportName, UBOOL& Supports, UBOOL AllowExt );
	virtual void FindProcs( UBOOL AllowExt );
	virtual void GLError( TCHAR* Tag );

	INT GetViewportIndex( FChildViewport* Viewport )
	{
		for(INT ViewportIndex = 0;ViewportIndex < Viewports.Num();ViewportIndex++)
			if(Viewports(ViewportIndex)->Viewport == Viewport)
				return ViewportIndex;

		return INDEX_NONE;
	}

	void UpdateRenderOptions();

	void InvalidateHitProxyCache();

	// FResourceClient interface.

	virtual void FreeResource( FResource* Resource );
	virtual void UpdateResource( FResource* Resource );
	
	/**
	 * Request an increase or decrease in the amount of miplevels a texture uses.
	 *
	 * @param	Texture				texture to adjust
	 * @param	RequestedMips		miplevels the texture should have
	 * @return	NULL if a decrease is requested, pointer to information about request otherwise (NOTE: pointer gets freed in FinalizeMipRequest)
	 */
	virtual void RequestMips( FTextureBase* Texture, UINT RequestedMips, FTextureMipRequest* TextureMipRequest );

	/**
	 * Finalizes a mip request. This gets called after all requested data has finished loading.	
	 *
	 * @param	TextureMipRequest	Mip request that is being finalized.
	 */
	virtual void FinalizeMipRequest( FTextureMipRequest* TextureMipRequest );

	// UObject interface.

	void StaticConstructor();
	virtual void Destroy();
	virtual void PostEditChange(UProperty* PropertyThatChanged);
	virtual void Serialize(FArchive& Ar);

	// FExec interface.

	virtual UBOOL Exec(const TCHAR* Cmd,FOutputDevice& Ar);

	// URenderDevice interface.

	virtual void Init();
	virtual void Flush();

	virtual void CreateViewport(FChildViewport* Viewport);
	virtual void ResizeViewport(FChildViewport* Viewport);
	virtual void DestroyViewport(FChildViewport* Viewport);

	virtual void DrawViewport(FChildViewport* Viewport,UBOOL Synchronous);
	virtual void ReadPixels(FChildViewport* Viewport,FColor* OutputBuffer);

	virtual void GetHitProxyMap(FChildViewport* Viewport,UINT MinX,UINT MinY,UINT MaxX,UINT MaxY,TArray<HHitProxy*>& OutMap);
	virtual void InvalidateHitProxies(FChildViewport* Viewport);

	virtual TArray<FMaterialError> CompileMaterial(FMaterial* Material);

	// FPrecacheInterface interface.

	virtual void CacheResource(FResource* Resource);
};

#define AUTO_INITIALIZE_REGISTRANTS_OPENGLDRV UOpenGLRenderDevice::StaticClass();
