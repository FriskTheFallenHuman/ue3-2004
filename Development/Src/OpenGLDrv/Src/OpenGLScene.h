struct FOpenGLSceneRenderer
{
/*
	UINT	LastLightingBuffer,
			NewLightingBuffer;
	UBOOL	LastLightingLayerFinished;

	UINT	LastBlurBuffer;
*/
	FSceneContext	Context;
	FSceneView*		View;
	FScene*			Scene;
	FMatrix			ViewProjectionMatrix,
					InvViewProjectionMatrix;
	FPlane			CameraPosition;
	FLOAT			TwoSidedSign;
	FLinearColor	SkyColor;

//	TArray<FD3DPrimitiveInfo>		PrimitiveInfoArray;
	TMap<UPrimitiveComponent*,INT>	PrimitiveIndexMap;		// A map from primitive to index in PrimitiveInfoArray.
//	TArray<FD3DLayerVisibilitySet>	LayerVisibilitySets;

//	TMap<ULightComponent*,FD3DLightInfo> VisibleLights;

//	TArray<TD3DRef<IDirect3DQuery9> >	TranslucencyLayerQueries;	// Each row is a translucency layer, each column is an occlusion query for a primitive in that layer.

	FConvexVolume	ViewFrustum;

	FLOAT FogMinHeight[4];
	FLOAT FogMaxHeight[4];
	FLOAT FogDistanceScale[4];
	FLinearColor FogInScattering[4];

	// Constructor/destructor.

	FOpenGLSceneRenderer(const FSceneContext& InContext);
	~FOpenGLSceneRenderer();

	// Render

	void Render();

	// RenderHitProxies - Renders the hit proxies of the scene primitives.

	void RenderHitProxies();
};
