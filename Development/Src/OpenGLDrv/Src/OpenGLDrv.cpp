/*=============================================================================
	OpenGLDrv.cpp: Unreal OpenGL driver precompiled header generator.

	Revision history:
		* Created by Konstantin Belichenko.
=============================================================================*/

#include "OpenGLDrv.h"
#include "OpenGLDrvPrivate.h"

UOpenGLRenderDevice*			GOpenGLRenderDevice = NULL;
FOpenGLRenderInterface			GOpenGLRenderInterface;
FOpenGLSceneRenderer*			GOpenGLSceneRenderer = NULL;
FOpenGLStatGroup				GOpenGLStats;

/*-----------------------------------------------------------------------------
	End.
-----------------------------------------------------------------------------*/
