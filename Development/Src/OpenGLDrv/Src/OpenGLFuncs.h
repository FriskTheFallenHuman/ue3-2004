/*=============================================================================
	OpenGLFuncs.h: OpenGL function-declaration macros.

	Copyright 2002 Epic Games, Inc. All Rights Reserved.

	Revision history:
		* Created by Daniel Vogel

=============================================================================*/

/*-----------------------------------------------------------------------------
	Standard OpenGL functions.
-----------------------------------------------------------------------------*/

GL_EXT(_GL)

/*-----------------------------------------------------------------------------
	OpenGL extensions.
-----------------------------------------------------------------------------*/

// BGRA textures.
GL_EXT(_GL_EXT_bgra)

//!!vogel: wglGetProcAddress requires an active context -> hack
// OpenGL 1.2
GL_PROC(_GL_EXT_bgra,void,glDrawRangeElements,(GLenum mode, GLuint start, GLuint end, GLsizei count, GLenum type, const GLvoid *indices))

// Generic texture compression.
GL_EXT(_GL_ARB_texture_compression)
GL_PROC(_GL_ARB_texture_compression,void,glCompressedTexSubImage2DARB,(GLenum, GLint, GLint, GLint, GLsizei, GLsizei, GLenum, GLsizei, const GLvoid *))
GL_PROC(_GL_ARB_texture_compression,void,glCompressedTexImage2DARB,(GLenum, GLint, GLenum, GLsizei, GLsizei, GLint, GLsizei, const GLvoid *))

// DXTC.
GL_EXT(_GL_EXT_texture_compression_s3tc)

// Cubemaps.
GL_EXT(_GL_ARB_texture_cube_map)

// Part of OpenGL 1.4 but we only require 1.3.
GL_EXT(_GL_EXT_texture_lod_bias)

// ARB multitexture.
GL_EXT(_GL_ARB_multitexture)
GL_PROC(_GL_ARB_multitexture,void,glMultiTexCoord1fARB,(GLenum target,GLfloat))
GL_PROC(_GL_ARB_multitexture,void,glMultiTexCoord2fARB,(GLenum target,GLfloat,GLfloat))
GL_PROC(_GL_ARB_multitexture,void,glMultiTexCoord3fARB,(GLenum target,GLfloat,GLfloat,GLfloat))
GL_PROC(_GL_ARB_multitexture,void,glMultiTexCoord4fARB,(GLenum target,GLfloat,GLfloat,GLfloat,GLfloat))
GL_PROC(_GL_ARB_multitexture,void,glMultiTexCoord1fvARB,(GLenum target,GLfloat))
GL_PROC(_GL_ARB_multitexture,void,glMultiTexCoord2fvARB,(GLenum target,GLfloat,GLfloat))
GL_PROC(_GL_ARB_multitexture,void,glMultiTexCoord3fvARB,(GLenum target,GLfloat,GLfloat,GLfloat))
GL_PROC(_GL_ARB_multitexture,void,glMultiTexCoord4fvARB,(GLenum target,GLfloat,GLfloat,GLfloat,GLfloat))
GL_PROC(_GL_ARB_multitexture,void,glActiveTextureARB,(GLenum target))
GL_PROC(_GL_ARB_multitexture,void,glClientActiveTextureARB,(GLenum target))

// Anisotropic filtering.
GL_EXT(_GL_EXT_texture_filter_anisotropic)

// Drivers report as part of GL extension string even though it's a WGL extension.
GL_EXT(_WGL_EXT_swap_control)
GL_PROC(_WGL_EXT_swap_control,GLboolean,wglSwapIntervalEXT,(GLint))
GL_PROC(_WGL_EXT_swap_control,GLint,wglGetSwapIntervalEXT,(void))

// Resource handling.
GL_EXT(_GL_ARB_vertex_buffer_object);
GL_PROC(_GL_ARB_vertex_buffer_object,void,glBindBufferARB,(GLenum,GLuint));
GL_PROC(_GL_ARB_vertex_buffer_object,void,glDeleteBuffersARB,(GLsizei, const GLuint*));
GL_PROC(_GL_ARB_vertex_buffer_object,void,glGenBuffersARB,(GLsizei, GLuint*));
GL_PROC(_GL_ARB_vertex_buffer_object,GLboolean,glIsBufferARB,(GLuint));
GL_PROC(_GL_ARB_vertex_buffer_object,void,glBufferDataARB,(GLenum,GLsizeiptrARB,const void*,GLenum));
GL_PROC(_GL_ARB_vertex_buffer_object,void,glBufferSubDataARB,(GLenum,GLintptrARB,GLsizeiptrARB,const void*));
GL_PROC(_GL_ARB_vertex_buffer_object,void,glGetBufferSubDataARB,(GLenum,GLintptrARB,GLsizeiptrARB,void*));
GL_PROC(_GL_ARB_vertex_buffer_object,void*,glMapBufferARB,(GLenum,GLenum));
GL_PROC(_GL_ARB_vertex_buffer_object,GLboolean,glUnmapBufferARB,(GLenum target));
GL_PROC(_GL_ARB_vertex_buffer_object,void,glGetBufferParameterivARB,(GLenum,GLenum,GLint*));
GL_PROC(_GL_ARB_vertex_buffer_object,void,glGetBufferPointervARB,(GLenum,GLenum,void**));

// Render-to-texture.
GL_EXT(_GL_EXT_framebuffer_object)
GL_PROC(_GL_EXT_framebuffer_object,GLboolean,glIsRenderbufferEXT,(GLuint))
GL_PROC(_GL_EXT_framebuffer_object,void,glBindRenderbufferEXT,(GLenum, GLuint))
GL_PROC(_GL_EXT_framebuffer_object,void,glDeleteRenderbuffersEXT,(GLsizei, const GLuint *))
GL_PROC(_GL_EXT_framebuffer_object,void,glGenRenderbuffersEXT,(GLsizei, GLuint *))
GL_PROC(_GL_EXT_framebuffer_object,void,glRenderbufferStorageEXT,(GLenum, GLenum, GLsizei, GLsizei))
GL_PROC(_GL_EXT_framebuffer_object,void,glGetRenderbufferParameterivEXT,(GLenum, GLenum, GLint*))
GL_PROC(_GL_EXT_framebuffer_object,GLboolean,glIsFramebufferEXT,(GLuint))
GL_PROC(_GL_EXT_framebuffer_object,void,glBindFramebufferEXT,(GLenum, GLuint))
GL_PROC(_GL_EXT_framebuffer_object,void,glDeleteFramebuffersEXT,(GLsizei, const GLuint *))
GL_PROC(_GL_EXT_framebuffer_object,void,glGenFramebuffersEXT,(GLsizei, GLuint *))
GL_PROC(_GL_EXT_framebuffer_object,GLenum,glCheckFramebufferStatusEXT,(GLenum))
GL_PROC(_GL_EXT_framebuffer_object,void,glFramebufferTexture1DEXT,(GLenum, GLenum, GLenum, GLuint, GLint))
GL_PROC(_GL_EXT_framebuffer_object,void,glFramebufferTexture2DEXT,(GLenum, GLenum, GLenum, GLuint, GLint))
GL_PROC(_GL_EXT_framebuffer_object,void,glFramebufferTexture3DEXT,(GLenum, GLenum, GLenum, GLuint, GLint, GLint))
GL_PROC(_GL_EXT_framebuffer_object,void,glFramebufferRenderbufferEXT,(GLenum, GLenum, GLenum, GLuint))
GL_PROC(_GL_EXT_framebuffer_object,void,glGetFramebufferAttachmentParameterivEXT,(GLenum, GLenum, GLenum, GLint *))
GL_PROC(_GL_EXT_framebuffer_object,void,glGenerateMipmapEXT,(GLenum))

// non-power-of-two texture support...
GL_EXT(_GL_ARB_texture_non_power_of_two)

/*-----------------------------------------------------------------------------
	The End.
-----------------------------------------------------------------------------*/

