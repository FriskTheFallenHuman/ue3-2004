/*=============================================================================
	LaunchPrivate.h: Unreal launcher.
	Copyright 1997-1999 Epic Games, Inc. All Rights Reserved.

Revision history:
	* Created by Tim Sweeney.
=============================================================================*/

#pragma once

#define DEMOGAME	1
#define WARGAME		2
#define UTGAME		3

//@warning: this needs to be the very first include
#if defined _WINDOWS && defined(WITH_EDITOR)
#include "UnrealEd.h"
#endif

#include "Engine.h"
#include "EngineMaterialClasses.h"
#include "EnginePhysicsClasses.h"
#include "EngineSequenceClasses.h"
#include "EngineSoundClasses.h"
#include "EngineInterpolationClasses.h"
#include "EngineParticleClasses.h"
#include "EngineAIClasses.h"
#include "EngineAnimClasses.h"
#include "UnTerrain.h"
#include "UnCodecs.h"
#include "UnIpDrv.h"
#if   GAMENAME == DEMOGAME
#include "DemoGameClasses.h"
#elif GAMENAME == WARGAME
#include "WarfareGameClasses.h"
#elif GAMENAME == UTGAME
#include "UTGameClasses.h"
#else
	#error Hook up your game name here
#endif
#ifdef WITH_EDITOR
#include "EditorPrivate.h"
#endif
#include "ALAudio.h"
#if _WINDOWS
#include "D3DDrv.h"
#include "OpenGLDrv.h"
#include "WinDrv.h"
#else
#include "SDLDrv.h"
#endif
#include "FMallocAnsi.h"
#include "FMallocDebug.h"
#include "FMallocThreadSafeProxy.h"
#include "FFeedbackContextAnsi.h"
#if _WINDOWS
#include "FFeedbackContextWindows.h"
#include "FFileManagerWindows.h"
#endif
#if __UNIX__
#include "FFileManagerUnix.h"
#endif
#include "FCallbackDevice.h"
#include "FConfigCacheIni.h"
#include "LaunchEngineLoop.h"
#if _WINDOWS
#include "UnThreadingWindows.h"
#endif
#if __UNIX__
#include "UnThreadingLinux.h"
#endif

/*-----------------------------------------------------------------------------
	The End.
-----------------------------------------------------------------------------*/
