/*=============================================================================
	Launch.cpp: Game launcher.
	Copyright 1997-2004 Epic Games, Inc. All Rights Reserved.

Revision history:
	* Created by Tim Sweeney.
	* Rewritten by Daniel Vogel.
=============================================================================*/

#include "LaunchPrivate.h"

FEngineLoop	GEngineLoop;

/**
 * Static guarded main function. Rolled into own function so we can have error handling for debug/ release builds depending
 * on whether a debugger is attached or not.
 */
INT GuardedMain(const TCHAR* CmdLine, HINSTANCE hInInstance, HINSTANCE hPrevInstance, INT nCmdShow)
{
	// make sure GEngineLoop::Exit() is always called.
	struct EngineLoopCleanupGuard { ~EngineLoopCleanupGuard() { GEngineLoop.Exit(); } } CleanupGuard;

	INT ErrorLevel = GEngineLoop.Init(CmdLine);
	
	// exit if Init failed.
	if (ErrorLevel != 0 || GIsRequestingExit)
	{
		return ErrorLevel;
	}

#if (!defined XBOX) && (!defined __UNIX__) && (defined WITH_EDITOR)
	UBOOL UsewxWindows = !(GIsUCC || ParseParam(appCmdLine(), TEXT("nowxwindows")));
	if (UsewxWindows)
	{
		// UnrealEd of game with wxWindows.
		ErrorLevel = wxEntry((WXHINSTANCE)hInInstance, (WXHINSTANCE)hPrevInstance, "", nCmdShow);
	}
	else if (GIsUCC)
	{
		// UCC.

		// Either close log window manually or press CTRL-C to exit if not in "silent" or "nopause" mode.
		if (!ParseParam(appCmdLine(), TEXT("SILENT")) && !ParseParam(appCmdLine(), TEXT("NOPAUSE")))
		{
			GLog->TearDown();
			Sleep(INFINITE);
		}
	}
	else
#endif
	{
		// Game without wxWindows.
		while (!GIsRequestingExit)
			GEngineLoop.Tick();
	}

	return ErrorLevel;
}

#if _WINDOWS

/*-----------------------------------------------------------------------------
	WinMain.
 -----------------------------------------------------------------------------*/

#ifndef _DEBUG
extern TCHAR MiniDumpFilenameW[64];
extern char  MiniDumpFilenameA[64];
extern INT CreateMiniDump( LPEXCEPTION_POINTERS ExceptionInfo );
#endif

INT WINAPI WinMain( HINSTANCE hInInstance, HINSTANCE hPrevInstance, char*, INT nCmdShow )
{
	// Initialize all timing info
    appInitTiming();

#if   GAMENAME == DEMOGAME
	appStrcpy( GGameName, TEXT("Demo") );
#elif GAMENAME == WARGAME
	appStrcpy( GGameName, TEXT("War") );
#elif GAMENAME == UTGAME
	appStrcpy( GGameName, TEXT("UT") );
#else
	#error Hook up your game name here
#endif

	INT ErrorLevel			= 0;
	GIsStarted				= 1;
	hInstance				= hInInstance;
	const TCHAR* CmdLine	= GetCommandLine();
	
#ifndef _DEBUG
	// Set up minidump filename.
	appStrcpy( MiniDumpFilenameW, TEXT("unreal-v") );
	appStrcat( MiniDumpFilenameW, appItoa( GEngineVersion ) );
	appStrcat( MiniDumpFilenameW, TEXT(".dmp") );
	strcpy( MiniDumpFilenameA, TCHAR_TO_ANSI( MiniDumpFilenameW ) );

	__try
#endif
	{
		GIsGuarded			= 1;

		// Run the guarded code.
		ErrorLevel = GuardedMain(CmdLine, hInInstance, hPrevInstance, nCmdShow);

		GIsGuarded = 0;
	}
#ifndef _DEBUG
	__except( CreateMiniDump( GetExceptionInformation() ) )
	{
		// Crashed.
		ErrorLevel = 1;
		GError->HandleError();
	}
#endif

	// Final shut down.
	appExit();
	GIsStarted = 0;
	return ErrorLevel;
}

#elif __UNIX__

/*-----------------------------------------------------------------------------
	Main.
 -----------------------------------------------------------------------------*/

static void BuildUnixCommandLine(FString &CmdLineStr, int argc, char **argv)
{
	CmdLineStr.Empty();
	if (argc >= 2)
	{
		CmdLineStr = ANSI_TO_TCHAR(argv[1]);
	}

	for (int i = 2; i < argc; i++)
	{
		CmdLineStr += TEXT(" ");
		CmdLineStr += ANSI_TO_TCHAR(argv[i]);
	}
}

int main(int argc, char **argv)
{
	// Initialize all timing info
	appInitTiming();

#if   GAMENAME == DEMOGAME
	appStrcpy(GGameName, TEXT("Demo"));
#elif GAMENAME == WARGAME
	appStrcpy(GGameName, TEXT("War"));
#elif GAMENAME == UTGAME
	appStrcpy(GGameName, TEXT("UT"));
#else
	#error Hook up your game name here
#endif

	int ErrorLevel = 0;
	GIsStarted = 1;

	FString CmdLine;
	BuildUnixCommandLine(CmdLine, argc, argv);
	ErrorLevel = GuardedMain(*CmdLine, NULL, NULL, 0);

	// Final shut down.
	appExit();

	GIsStarted = 0;
	return ErrorLevel;
}

#endif

/*-----------------------------------------------------------------------------
	The End.
-----------------------------------------------------------------------------*/