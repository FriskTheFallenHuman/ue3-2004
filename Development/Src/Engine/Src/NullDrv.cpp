/*=============================================================================
	NullDrv.cpp: Unreal Null render device.
	Copyright 1997-2004 Epic Games, Inc. All Rights Reserved.

	Revision history:
		* Created by Daniel Vogel.
=============================================================================*/

// Includes.
#include "EnginePrivate.h"
#include "UnNullRenderDevice.h"

IMPLEMENT_CLASS(UNullRenderDevice);

/*-----------------------------------------------------------------------------
	End.
-----------------------------------------------------------------------------*/

