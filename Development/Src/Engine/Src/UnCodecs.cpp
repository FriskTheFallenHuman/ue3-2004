/*=============================================================================
UnCodecs.cpp: Movie codec implementations
Copyright 2004-2005 Epic Games, Inc. All Rights Reserved.
=============================================================================*/

#include "EnginePrivate.h"
#include "UnCodecs.h"

IMPLEMENT_CLASS(UCodecMovie);
IMPLEMENT_CLASS(UCodecMovieFallback);


/*-----------------------------------------------------------------------------
UCodecMovieFallback
-----------------------------------------------------------------------------*/


/**
 * Returns the movie width.
 *
 * @return width of movie.
 */
UINT UCodecMovieFallback::GetSizeX()
{
	return 1;
}

/**
 * Returns the movie height.
 *
 * @return height of movie.
 */
UINT UCodecMovieFallback::GetSizeY()
{
	return 1;
}

/** 
 * Returns the movie format, in this case PF_A8R8G8B8
 *
 * @return format of movie (always PF_A8R8G8B8)
 */
EPixelFormat UCodecMovieFallback::GetFormat()
{
	return PF_A8R8G8B8;
}

/**
 * Initializes the decoder to stream from disk.
 *
 * @param	Filename	unused
 * @param	Offset		unused
 * @param	Size		unused.
 *
 * @return	TRUE if initialization was successful, FALSE otherwise.
 */
UBOOL UCodecMovieFallback::Open( const FString& /*Filename*/, DWORD /*Offset*/, DWORD /*Size*/ )
{
	PlaybackDuration	= 1.f;
	CurrentTime			= 0;
	return TRUE;
}

/**
 * Initializes the decoder to stream from memory.
 *
 * @param	Source		unused
 * @param	Size		unused
 *
 * @return	TRUE if initialization was successful, FALSE otherwise.
 */
UBOOL UCodecMovieFallback::Open( void* /*Source*/, DWORD /*Size*/ )
{
	PlaybackDuration	= 1.f;
	CurrentTime			= 0;
	return TRUE;
}

/**
 * Tears down stream, closing file handle if there was an open one.	
 */
void UCodecMovieFallback::Close()
{
}

/**
 * Resets the stream to its initial state so it can be played again from the beginning.
 */
void UCodecMovieFallback::ResetStream()
{
	CurrentTime = 0.f;
}

/**
 * Blocks until the decoder has finished the pending decompression request.
 *
 * @return	Time into movie playback as seen from the decoder side.
 */
FLOAT UCodecMovieFallback::BlockUntilIdle() 
{ 
	return CurrentTime;
}		

/**
 * Returns the framerate the movie was encoded at.
 *
 * @return framerate the movie was encoded at.
 */
FLOAT UCodecMovieFallback::GetFrameRate()
{
	return 30.f;
}

/**
 * Queues the request to retrieve the next frame.
 *
 * @param	Destination		Memory block to uncompress data into.
 * @return	FALSE if the end of the frame has been reached and the frame couldn't be completed, TRUE otherwise
 */
UBOOL UCodecMovieFallback::GetFrame( void* Destination )
{
	CurrentTime += 1.f / GetFrameRate();

	if( CurrentTime > PlaybackDuration )
	{
		return FALSE;
	}
	else
	{
		*((FColor*) Destination) = FColor( 255, 255 * appFractional(PlaybackDuration / CurrentTime), 0 );
	}
	return TRUE;
}

/*-----------------------------------------------------------------------------
	The End.
-----------------------------------------------------------------------------*/
