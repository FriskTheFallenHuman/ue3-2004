#ifndef UNullRenderDevice_H
#define UNullRenderDevice_H

// Package implementation.
class UNullRenderDevice;

//
//	FNullViewport
//

struct FNullViewport
{
public:
	FChildViewport*				Viewport;
	UINT						SizeX,
								SizeY;

	FNullViewport(void* Window, FChildViewport* InViewport) {}
	~FNullViewport() {}
};

//
//	FNullRenderInterface
//
class FNullRenderInterface : public FRenderInterface
{
public:

	// Variables.
	struct FNullViewport*		CurrentViewport;
	UBOOL						HitTesting;
	DWORD						CurrentHitProxyIndex;

	// Constructor.
	FNullRenderInterface() :
		FRenderInterface(),
		CurrentViewport(NULL),
		HitTesting(0),
		CurrentHitProxyIndex(-1)
	{}

	void BeginScene(FNullViewport* Viewport, UBOOL InHitTesting = 0) {}
	void EndScene(FNullViewport* Viewport, UBOOL Synchronous) {}

	// FRenderInterface interface.
	virtual void Clear(const FColor& Color) {}

	virtual void DrawScene(const FSceneContext& Context) {}

	virtual void DrawLine2D(INT X1, INT Y1, INT X2, INT Y2, FColor Color) {}

	virtual void DrawTile(INT X, INT Y, UINT SizeX, UINT SizeY, FLOAT U, FLOAT V, FLOAT W, FLOAT SizeU, FLOAT SizeV, const FLinearColor& Color, FTexture3D* Texture = NULL, UBOOL AlphaBlend = 1) {}
	virtual void DrawTile(INT X, INT Y, UINT SizeX, UINT SizeY, FLOAT U, FLOAT V, FLOAT SizeU, FLOAT SizeV, const FLinearColor& Color, FTexture2D* Texture = NULL, UBOOL AlphaBlend = 1) {}
	virtual void DrawTile(INT X, INT Y, UINT SizeX, UINT SizeY, FLOAT U, FLOAT V, FLOAT SizeU, FLOAT SizeV, FMaterial* Material, FMaterialInstance* MaterialInstance) {}

	virtual void DrawTriangle2D(
		const FIntPoint& Vert0, FLOAT U0, FLOAT V0,
		const FIntPoint& Vert1, FLOAT U1, FLOAT V1,
		const FIntPoint& Vert2, FLOAT U2, FLOAT V2,
		const FLinearColor& Color, FTexture2D* Texture = NULL, UBOOL AlphaBlend = 1) {}
	virtual void DrawTriangle2D(
		const FIntPoint& Vert0, FLOAT U0, FLOAT V0,
		const FIntPoint& Vert1, FLOAT U1, FLOAT V1,
		const FIntPoint& Vert2, FLOAT U2, FLOAT V2,
		FMaterial* Material, FMaterialInstance* MaterialInstance) {}

	virtual void SetHitProxy(HHitProxy* Hit) {}
	virtual UBOOL IsHitTesting() { return HitTesting; }

private:
};

class UNullRenderDevice : public URenderDevice, FResourceClient, FPrecacheInterface
{
	DECLARE_CLASS(UNullRenderDevice,URenderDevice,CLASS_Config,Engine);

public:
	// Configuration


	// Hit detection
	TArray<HHitProxy*>		HitProxies;
	FChildViewport*			CachedHitProxyViewport;

	// Variables
	TArray<FNullViewport*> Viewports;
	FNullViewport*			FullscreenViewport;

	UBOOL					bGLInitialized;

	// Constructor.
	UNullRenderDevice() {}

	// Helper functions.

	INT GetViewportIndex(FChildViewport* Viewport)
	{
		for (INT ViewportIndex = 0; ViewportIndex < Viewports.Num(); ViewportIndex++)
			if (Viewports(ViewportIndex)->Viewport == Viewport)
				return ViewportIndex;

		return INDEX_NONE;
	}

	void UpdateRenderOptions() {}

	void InvalidateHitProxyCache() {}

	// FResourceClient interface.

	virtual void FreeResource(FResource* Resource) {}
	virtual void UpdateResource(FResource* Resource) {}

	/**
	 * Request an increase or decrease in the amount of miplevels a texture uses.
	 *
	 * @param	Texture				texture to adjust
	 * @param	RequestedMips		miplevels the texture should have
	 * @return	NULL if a decrease is requested, pointer to information about request otherwise (NOTE: pointer gets freed in FinalizeMipRequest)
	 */
	virtual void RequestMips(FTextureBase* Texture, UINT RequestedMips, FTextureMipRequest* TextureMipRequest) {}

	/**
	 * Finalizes a mip request. This gets called after all requested data has finished loading.
	 *
	 * @param	TextureMipRequest	Mip request that is being finalized.
	 */
	virtual void FinalizeMipRequest(FTextureMipRequest* TextureMipRequest) {}

	// UObject interface.

	void StaticConstructor() {}
	virtual void Destroy() {}
	virtual void PostEditChange(UProperty* PropertyThatChanged) {}
	virtual void Serialize(FArchive& Ar) {}

	// FExec interface.

	virtual UBOOL Exec(const TCHAR* Cmd, FOutputDevice& Ar) { return TRUE; }

	// URenderDevice interface.

	virtual void Init() {}
	virtual void Flush() {}

	virtual void CreateViewport(FChildViewport* Viewport) {}
	virtual void ResizeViewport(FChildViewport* Viewport) {}
	virtual void DestroyViewport(FChildViewport* Viewport) {}

	virtual void DrawViewport(FChildViewport* Viewport, UBOOL Synchronous) {}
	virtual void ReadPixels(FChildViewport* Viewport, FColor* OutputBuffer) {}

	virtual void GetHitProxyMap(FChildViewport* Viewport, UINT MinX, UINT MinY, UINT MaxX, UINT MaxY, TArray<HHitProxy*>& OutMap) {}
	virtual void InvalidateHitProxies(FChildViewport* Viewport) {}

	virtual TArray<FMaterialError> CompileMaterial(FMaterial* Material)
	{
		TArray<FMaterialError>	Errors;
		return Errors;
	}

	// FPrecacheInterface interface.

	virtual void CacheResource(FResource* Resource) {}
};

#endif
