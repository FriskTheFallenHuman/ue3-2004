/*=============================================================================
	SDLClient.cpp: USDLClient code.
	Copyright 1997-2004 Epic Games, Inc. All Rights Reserved.

	Revision history:
=============================================================================*/

#include "SDLDrv.h"

/*-----------------------------------------------------------------------------
	Class implementation.
-----------------------------------------------------------------------------*/

IMPLEMENT_CLASS(USDLClient);

/*-----------------------------------------------------------------------------
	USDLClient implementation.
-----------------------------------------------------------------------------*/

//
// USDLClient constructor.
//
USDLClient::USDLClient()
{
	AudioDevice				= NULL;
	RenderDevice			= NULL;
}

//
// Static init.
//
void USDLClient::StaticConstructor()
{
	new(GetClass(),TEXT("RenderDeviceClass"),RF_Public)UClassProperty(CPP_PROPERTY(RenderDeviceClass)	,TEXT("Display"),CPF_Config,URenderDevice::StaticClass());
	new(GetClass(),TEXT("AudioDeviceClass")	,RF_Public)UClassProperty(CPP_PROPERTY(AudioDeviceClass)	,TEXT("Audio")	,CPF_Config,UAudioDevice::StaticClass());
}

//
// Initialize the platform-specific viewport manager subsystem.
// Must be called after the Unreal object manager has been initialized.
// Must be called before any viewports are created.
//
void USDLClient::Init( UEngine* InEngine )
{
	Engine = InEngine;

	debugf( NAME_Init, TEXT("Initializing SDL client...") );

	// Startup SDL library
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) == -1)
    {
        const TCHAR *err = ANSI_TO_TCHAR(SDL_GetError());
		appErrorf(TEXT("Couldn't initialize SDL: %s\n"), err);
		appExit();
	}

	// Initialize the render device.
	RenderDevice = CastChecked<URenderDevice>(StaticConstructObject(RenderDeviceClass));
	RenderDevice->Init();

	// Initialize the audio device.
	if( GEngine->UseSound )
	{
		AudioDevice = CastChecked<UAudioDevice>(StaticConstructObject(AudioDeviceClass));
		if( !AudioDevice->Init() )
		{
			delete AudioDevice;
			AudioDevice = NULL;
		}
	}

	// Success.
	debugf( NAME_Init, TEXT("SDL client initialized") );
}

//
//	USDLClient::Flush
//
void USDLClient::Flush()
{
	RenderDevice->Flush();
	if( AudioDevice )
		AudioDevice->Flush();
}

//
//	USDLClient::Serialize - Make sure objects the client reference aren't garbage collected.
//
void USDLClient::Serialize(FArchive& Ar)
{
	Super::Serialize(Ar);

	Ar << Engine << RenderDeviceClass << RenderDevice << AudioDeviceClass << AudioDevice;
}

//
//	USDLClient::Destroy - Shut down the platform-specific viewport manager subsystem.
//
void USDLClient::Destroy()
{
	// Close all viewports.
	for(INT ViewportIndex = 0;ViewportIndex < Viewports.Num();ViewportIndex++)
		delete Viewports(ViewportIndex);
	Viewports.Empty();

	// Delete the render device.
	delete RenderDevice;
	RenderDevice = NULL;

	// Delete the audio device.
	delete AudioDevice;
	AudioDevice = NULL;

	// Shutdown SDL
	SDL_Quit();

	debugf( NAME_Exit, TEXT("SDL client shut down") );
	Super::Destroy();
}

//
// Failsafe routine to shut down viewport manager subsystem
// after an error has occured. Not guarded.
//
void USDLClient::ShutdownAfterError()
{
	debugf( NAME_Exit, TEXT("Executing USDLClient::ShutdownAfterError") );

	SDL_ShowCursor(1);

	for(UINT ViewportIndex = 0;ViewportIndex < (UINT)Viewports.Num();ViewportIndex++)
		Viewports(ViewportIndex)->ShutdownAfterError();

	SDL_Quit();

	Super::ShutdownAfterError();
}

//
//	USDLClient::Tick - Perform timer-tick processing on all visible viewports.
//
void USDLClient::Tick( FLOAT DeltaTime )
{
	// Process input.
	BEGINCYCLECOUNTER(GEngineStats.InputTime);
	for(UINT ViewportIndex = 0;ViewportIndex < (UINT)Viewports.Num();ViewportIndex++)
	{
		Viewports(ViewportIndex)->ProcessInput( DeltaTime );
	}
	ENDCYCLECOUNTER;

	// Cleanup viewports that have been destroyed.
	for(INT ViewportIndex = 0;ViewportIndex < Viewports.Num();ViewportIndex++)
	{
		if(!Viewports(ViewportIndex)->Window)
		{
			delete Viewports(ViewportIndex);
			Viewports.Remove(ViewportIndex--);
		}
	}
}

//
//	USDLClient::Exec
//
UBOOL USDLClient::Exec(const TCHAR* Cmd,FOutputDevice& Ar)
{
	if(RenderDevice && RenderDevice->Exec(Cmd,Ar))
		return 1;
	else
		return 0;
}

//
//	USDLClient::CreateViewport
//
FViewport* USDLClient::CreateViewport(FViewportClient* ViewportClient,const TCHAR* Name,UINT SizeX,UINT SizeY,UBOOL Fullscreen)
{
	return new FSDLViewport(this,ViewportClient,Name,SizeX,SizeY,Fullscreen,NULL);
}

//
//	USDLClient::CreateWindowChildViewport
//
FChildViewport* USDLClient::CreateWindowChildViewport(FViewportClient* ViewportClient,void* ParentWindow,UINT SizeX,UINT SizeY)
{
	return new FSDLViewport(this,ViewportClient,TEXT(""),SizeX,SizeY,0,ParentWindow);
}

//
//	USDLClient::CloseViewport
//
void USDLClient::CloseViewport(FChildViewport* Viewport)
{
	FSDLViewport* WindowsViewport = (FSDLViewport*)Viewport;
	WindowsViewport->Destroy();
}

//
//	Static variables.
//
TArray<FSDLViewport*>		USDLClient::Viewports;
