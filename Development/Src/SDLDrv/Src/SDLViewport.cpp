/*=============================================================================
	SDLViewport.cpp: FSDLViewport code.
	Copyright 1997-2004 Epic Games, Inc. All Rights Reserved.

	Revision history:
=============================================================================*/

#include "SDLDrv.h"

//
//	FSDLViewport::FSDLViewport
//
FSDLViewport::FSDLViewport(USDLClient* InClient,FViewportClient* InViewportClient,const TCHAR* InName,UINT InSizeX,UINT InSizeY,UBOOL InFullscreen,HWND InParentWindow)
{
	Client					= InClient;
	ViewportClient			= InViewportClient;

	Name					= InName;
	Window					= NULL;
	ParentWindow			= InParentWindow;
	
	InitializedRenderDevice = 0;
	bCapturingMouseInput	= 0;
	bCapturingJoystickInput = 1;
	bLockingMouseToWindow	= 0;

	Minimized				= 0;
	Maximized				= 0;
	Resizing				= 0;
	
	Client->Viewports.AddItem(this);

	// Creates the viewport window.
	Resize(InSizeX,InSizeY,InFullscreen);

#if 0
	// Set as active window.
	::SetActiveWindow(Window);

	// Set initial key state.
	for(UINT KeyIndex = 0;KeyIndex < 256;KeyIndex++)
	{
		FName*	KeyName = Client->KeyMapVirtualToName.Find(KeyIndex);

		if(KeyName && *KeyName != KEY_LeftMouseButton && *KeyName != KEY_RightMouseButton && *KeyName != KEY_MiddleMouseButton)
			KeyStates[KeyIndex] = ::GetKeyState(KeyIndex) & 0x8000;
	}
#endif
}

//
//	FSDLViewport::Destroy
//

void FSDLViewport::Destroy()
{
	ViewportClient = NULL;

	if( InitializedRenderDevice )
	{
		Client->RenderDevice->DestroyViewport(this);
		InitializedRenderDevice = 0;
	}

	CaptureMouseInput(0);

	//DestroyWindow(Window);
}

//
//	FSDLViewport::Resize
//
void FSDLViewport::Resize(UINT NewSizeX,UINT NewSizeY,UBOOL NewFullscreen)
{
#if 0	
	static UBOOL ReEntrant = 0;

	if( ReEntrant )
	{
		debugf(NAME_Error,TEXT("ReEntrant == 1, FSDLViewport::Resize"));
		appDebugBreak();
		return;
	}

	ReEntrant				= 1;

	SizeX					= NewSizeX;
	SizeY					= NewSizeY;
	Fullscreen				= NewFullscreen;

	// Figure out physical window size we must specify to get appropriate client area.
	DWORD	WindowStyle;
	INT		PhysWidth		= NewSizeX,
			PhysHeight		= NewSizeY;

	if( ParentWindow )
	{
		WindowStyle			= WS_CHILD | WS_CLIPSIBLINGS;
		Fullscreen			= 0;
	}
	else
	{
		WindowStyle	= WS_OVERLAPPED | WS_SYSMENU | WS_MINIMIZEBOX | WS_MAXIMIZEBOX;

		if(!Fullscreen) // Hide the caption bar for fullscreen windows.
			WindowStyle |= WS_THICKFRAME | WS_CAPTION;

		RECT  WindowRect;
		WindowRect.left		= 100;
		WindowRect.top		= 100;
		WindowRect.right	= NewSizeX + 100;
		WindowRect.bottom	= NewSizeY + 100;

		::AdjustWindowRect(&WindowRect,WindowStyle,0);

		PhysWidth			= WindowRect.right  - WindowRect.left,
		PhysHeight			= WindowRect.bottom - WindowRect.top;
	}

	if( Window == NULL )
	{
		// Obtain width and height of primary monitor.
		INT ScreenWidth  = ::GetSystemMetrics( SM_CXSCREEN );
		INT ScreenHeight = ::GetSystemMetrics( SM_CYSCREEN );
		
		// Create the window in the upper left quadrant.
		Window = CreateWindowExX( WS_EX_APPWINDOW, *Client->WindowClassName, *Name, WindowStyle, (ScreenWidth - PhysWidth) / 6, (ScreenHeight - PhysHeight) / 6, PhysWidth, PhysHeight, ParentWindow, NULL, hInstance, this );
		verify( Window );
	}

	if(ParentWindow)
		SetWindowLongX(Window,GWL_STYLE,WS_POPUP | WS_VISIBLE); // AJS says: ??? Editor viewports don't work without this.
	else
		SetWindowLongX(Window,GWL_STYLE,WindowStyle);

	// Initialize the viewport's render device.
	if( NewSizeX && NewSizeY )
	{
		if( InitializedRenderDevice )
		{
			InitializedRenderDevice = 0;
			Client->RenderDevice->ResizeViewport(this);
			InitializedRenderDevice = 1;
		}
		else
		{
			Client->RenderDevice->CreateViewport(this);
			InitializedRenderDevice = 1;
		}
	}
	else
	{
		if( InitializedRenderDevice )
		{
			InitializedRenderDevice = 0;
			Client->RenderDevice->DestroyViewport(this);
		}
	}

	if( !ParentWindow && !NewFullscreen )
	{
		// Resize viewport window.
		RECT WindowRect;
		::GetWindowRect( Window, &WindowRect );

		RECT ScreenRect;
		ScreenRect.left		= ::GetSystemMetrics( SM_XVIRTUALSCREEN );
		ScreenRect.top		= ::GetSystemMetrics( SM_YVIRTUALSCREEN );
		ScreenRect.right	= ::GetSystemMetrics( SM_CXVIRTUALSCREEN );
		ScreenRect.bottom	= ::GetSystemMetrics( SM_CYVIRTUALSCREEN );

		if( WindowRect.left >= ScreenRect.right-4 || WindowRect.left < ScreenRect.left - 4 )
			WindowRect.left = ScreenRect.left;
		if( WindowRect.top >= ScreenRect.bottom-4 || WindowRect.top < ScreenRect.top - 4 )
			WindowRect.top = ScreenRect.top;

		::SetWindowPos( Window, HWND_TOP, WindowRect.left, WindowRect.top, PhysWidth, PhysHeight, SWP_NOSENDCHANGING | SWP_NOZORDER );
	}

	// Show the viewport.
	::ShowWindow( Window, SW_SHOW );
	::UpdateWindow( Window );

	ReEntrant = 0;
#endif	
}

//
//	FSDLViewport::ShutdownAfterError - Minimalist shutdown.
//
void FSDLViewport::ShutdownAfterError()
{
	if(Window)
	{
		//::DestroyWindow(Window);
		Window = NULL;
	}
	
	SDL_Quit();
}

//
//	FSDLViewport::CaptureInput
//
UBOOL FSDLViewport::CaptureMouseInput(UBOOL Capture)
{
#if 0
	Capture = Capture && ::GetForegroundWindow() == Window && ::GetFocus() == Window;

	if(Capture && !bCapturingMouseInput)
	{
		// Store current mouse position and capture mouse.
		::GetCursorPos(&PreCaptureMousePos);
		::SetCapture(Window);

		// Hider mouse cursor.
		while( ::ShowCursor(FALSE)>=0 );

		// Clip mouse to window.
		LockMouseToWindow(true);

		// Clear mouse input buffer.
		if( SUCCEEDED( UWindowsClient::DirectInput8Mouse->Acquire() ) )
		{
			DIDEVICEOBJECTDATA	Event;
			DWORD				Elements = 1;
			while( SUCCEEDED( UWindowsClient::DirectInput8Mouse->GetDeviceData(sizeof(DIDEVICEOBJECTDATA),&Event,&Elements,0) ) && (Elements > 0) );
		}

		bCapturingMouseInput = 1;
	}
	else if(!Capture && bCapturingMouseInput)
	{
		bCapturingMouseInput = 0;

		// No longer confine mouse to window.
		LockMouseToWindow(false);

		// Show mouse cursor.
		while( ::ShowCursor(TRUE)<0 );

		// Restore old mouse position and release capture.
		::SetCursorPos(PreCaptureMousePos.x,PreCaptureMousePos.y);
		::ReleaseCapture();
	}

	return bCapturingMouseInput;
#else
	return TRUE;
#endif
}

void FSDLViewport::LockMouseToWindow(UBOOL bLock)
{
#if 0	
	if(bLock && !bLockingMouseToWindow)
	{
		// Store the old cursor clip rectangle.
		::GetClipCursor(&PreCaptureCursorClipRect);

		RECT R;
		::GetClientRect( Window, &R );
		::MapWindowPoints( Window, NULL, (POINT*)&R, 2 );
		::ClipCursor(&R);

		bLockingMouseToWindow = true;

		//debugf( TEXT("LOCK MOUSE: (%d %d) (%d %d)  SAVED: (%d %d) (%d %d)"), R.left, R.top, R.right, R.bottom, PreCaptureCursorClipRect.left, PreCaptureCursorClipRect.top, PreCaptureCursorClipRect.right, PreCaptureCursorClipRect.bottom );
	}
	else if(!bLock && bLockingMouseToWindow)
	{
		//debugf( TEXT("UNLOCK MOUSE: (%d %d) (%d %d)"), PreCaptureCursorClipRect.left, PreCaptureCursorClipRect.top, PreCaptureCursorClipRect.right, PreCaptureCursorClipRect.bottom );

		::ClipCursor(&PreCaptureCursorClipRect);

		bLockingMouseToWindow = false;
	}
#endif	
}

//
//	FSDLViewport::CaptureJoystickInput
//
UBOOL FSDLViewport::CaptureJoystickInput(UBOOL Capture)
{
#if 0	
	bCapturingJoystickInput	= Capture;

	return bCapturingJoystickInput;
#else
	return FALSE;
#endif
}

//
//	FSDLViewport::KeyState
//
UBOOL FSDLViewport::KeyState(FName Key)
{
#if 0	
	BYTE* VirtualKey = Client->KeyMapNameToVirtual.Find(Key);

	if( VirtualKey )
		return ::GetKeyState(*VirtualKey) & 0x8000;
	else
		return 0;
#else
	return 0;
#endif
}

//
//	FSDLViewport::GetMouseX
//
INT FSDLViewport::GetMouseX()
{
	int X, Y;
	SDL_GetMouseState(&X, &Y);
	return (INT)X;
}

//
//	FSDLViewport::GetMouseY
//
INT FSDLViewport::GetMouseY()
{
	int X, Y;
	SDL_GetMouseState(&X, &Y);
	return (INT)Y;
}

//
//	FSDLViewport::Draw
//

void FSDLViewport::Draw(UBOOL Synchronous)
{
	if(InitializedRenderDevice)
		Client->RenderDevice->DrawViewport(this,Synchronous);
}

//
//	FSDLViewport::InvalidateDisplay
//

void FSDLViewport::InvalidateDisplay()
{
}

//
//	FSDLViewport::GetHitProxyMap
//

void FSDLViewport::GetHitProxyMap(UINT MinX,UINT MinY,UINT MaxX,UINT MaxY,TArray<HHitProxy*>& OutMap)
{
	if(InitializedRenderDevice)
		Client->RenderDevice->GetHitProxyMap(this,MinX,MinY,MaxX,MaxY,OutMap);
	else	
	{
		check(MaxX >= MinX && MaxY >= MinY);
		OutMap.Empty((MaxX - MinX + 1) * (MaxY - MinY + 1));
		OutMap.AddZeroed((MaxX - MinX + 1) * (MaxY - MinY + 1));
	}
}

//
//	FSDLViewport::Invalidate
//

void FSDLViewport::Invalidate()
{
	if(InitializedRenderDevice)
		Client->RenderDevice->InvalidateHitProxies(this);
	InvalidateDisplay();
}

//
//	FSDLViewport::ReadPixels
//

UBOOL FSDLViewport::ReadPixels(TArray<FColor>& OutputBuffer)
{
	if(InitializedRenderDevice)
	{
		OutputBuffer.Empty(SizeX * SizeY);
		OutputBuffer.Add(SizeX * SizeY);
		Client->RenderDevice->ReadPixels(this,&OutputBuffer(0));
		return 1;
	}
	else
		return 0;
}

//
//	FSDLViewport::SetName
//
void FSDLViewport::SetName(const TCHAR* NewName)
{
#if 0
	if( Window )
	{
		Name = NewName;
		SendMessageLX( Window, WM_SETTEXT, 0, NewName );
	}
#endif
}

//
//	FSDLViewport::ProcessInput
//
void FSDLViewport::ProcessInput( FLOAT DeltaTime )
{
	if(!ViewportClient)
		return;

#if 0
	// Process mouse input.
	if( bCapturingMouseInput )
	{
		if( SUCCEEDED( UWindowsClient::DirectInput8Mouse->Acquire() ) )
		{
			UWindowsClient::DirectInput8Mouse->Poll();
			
			DIDEVICEOBJECTDATA	Event;
			DWORD				Elements = 1;
			while( SUCCEEDED( UWindowsClient::DirectInput8Mouse->GetDeviceData(sizeof(DIDEVICEOBJECTDATA),&Event,&Elements,0) ) && (Elements > 0) )
			{
				switch( Event.dwOfs ) 
				{
				case DIMOFS_X: 
					ViewportClient->InputAxis(this,KEY_MouseX,(INT)Event.dwData,DeltaTime);
					bMouseHasMoved = 1;
					break;
				case DIMOFS_Y: 
					ViewportClient->InputAxis(this,KEY_MouseY,-(INT)Event.dwData,DeltaTime);
					bMouseHasMoved = 1;
					break; 
				case DIMOFS_Z:
					if( ((INT)Event.dwData) < 0 )
					{
						ViewportClient->InputKey(this,KEY_MouseScrollDown,IE_Pressed);
						ViewportClient->InputKey(this,KEY_MouseScrollDown,IE_Released);
					}
					else if( ((INT)Event.dwData) > 0)
					{
						ViewportClient->InputKey(this,KEY_MouseScrollUp,IE_Pressed);
						ViewportClient->InputKey(this,KEY_MouseScrollUp,IE_Released);
					}
					break;
				default:
					break;
				}
			}
		}
	}

	// Process keyboard input.
	for( UINT KeyIndex = 0;KeyIndex < 256;KeyIndex++ )
	{
		FName* KeyName = Client->KeyMapVirtualToName.Find(KeyIndex);

		if(  KeyName 
		&&  *KeyName != KEY_LeftMouseButton 
		&&  *KeyName != KEY_RightMouseButton 
		&&  *KeyName != KEY_MiddleMouseButton 
		)
		{
			UBOOL NewKeyState = ::GetKeyState(KeyIndex) & 0x8000;

			if( !NewKeyState && KeyStates[KeyIndex] )
			{
				ViewportClient->InputKey(this,*KeyName,IE_Released);
				KeyStates[KeyIndex] = 0;
			}
		}
	}
#endif	
}

//
//	FSDLViewport::HandlePossibleSizeChange
//
void FSDLViewport::HandlePossibleSizeChange()
{
#if 0	
	RECT	WindowClientRect;
	::GetClientRect( Window, &WindowClientRect );

	UINT	NewSizeX = WindowClientRect.right - WindowClientRect.left,
			NewSizeY = WindowClientRect.bottom - WindowClientRect.top;

	if(!Fullscreen && (NewSizeX != SizeX || NewSizeY != SizeY))
	{
		Resize( NewSizeX, NewSizeY, 0 );

		if(ViewportClient)
			ViewportClient->ReceivedFocus(this);
	}
#endif
}
