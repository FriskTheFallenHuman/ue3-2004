/*=============================================================================
	SDLDrv.h: Unreal Windows viewport and platform driver.
	Copyright 2003 Epic Games, Inc. All Rights Reserved.

Revision history:

=============================================================================*/

#ifndef _INC_SDLDRV
#define _INC_SDLDRV

/*----------------------------------------------------------------------------
	Dependencies.
 ----------------------------------------------------------------------------*/

// System includes.
#include <stdlib.h>
#ifdef WIN32
#include <windows.h>
#else
typedef void *HWND;
#endif

// Unreal includes.
#include "Engine.h"

// SDL includes
#include "SDL.h"
#include "SDL_syswm.h"

/*-----------------------------------------------------------------------------
	Declarations.
 -----------------------------------------------------------------------------*/

// Forward declarations.
struct FSDLViewport;

//
//	USDLClient - SDL-specific client code.
//
class USDLClient : public UClient
{
	DECLARE_CLASS(USDLClient,UClient,CLASS_Transient|CLASS_Config,SDLDrv)

	// Static variables.
	static TArray<FSDLViewport*>        Viewports;

	// Variables.
	UEngine*							Engine;
	FString								WindowClassName;

	TMap<BYTE,FName>					KeyMapVirtualToName;
	TMap<FName,BYTE>					KeyMapNameToVirtual;

	// Render device.
	UClass*								RenderDeviceClass;
	URenderDevice*						RenderDevice;

	// Audio device.
	UClass*								AudioDeviceClass;
	UAudioDevice*						AudioDevice;

	// Constructors.
	USDLClient();
	void StaticConstructor();

	// UObject interface.
	virtual void Serialize(FArchive& Ar);
	virtual void Destroy();
	virtual void ShutdownAfterError();

	// UClient interface.
	virtual void Init(UEngine* InEngine);
	virtual void Flush();
	virtual void Tick( FLOAT DeltaTime );
	virtual UBOOL Exec(const TCHAR* Cmd,FOutputDevice& Ar);
	/**
	 * PC only, debugging only function to prevent the engine from reacting to OS messages. Used by e.g. the script
	 * debugger to avoid script being called from within message loop (input).
	 *
	 * @param InValue	If FALSE disallows OS message processing, otherwise allows OS message processing (default)
	 */
	virtual void AllowMessageProcessing( UBOOL InValue ) { return; }

	virtual FViewport* CreateViewport(FViewportClient* ViewportClient,const TCHAR* Name,UINT SizeX,UINT SizeY,UBOOL Fullscreen = 0);
	virtual FChildViewport* CreateWindowChildViewport(FViewportClient* ViewportClient,void* ParentWindow,UINT SizeX=0,UINT SizeY=0);
	virtual void CloseViewport(FChildViewport* Viewport);

	virtual class URenderDevice* GetRenderDevice() { return RenderDevice; }
	virtual class UAudioDevice* GetAudioDevice() { return AudioDevice; }
};


//
//	FSDLViewport
//
struct FSDLViewport: FViewport
{
	// Viewport state.
	USDLClient*				Client;
    void*					Window;
	void*					ParentWindow;
	UBOOL					InitializedRenderDevice;

	FString					Name;
	
	UINT					SizeX,
							SizeY;

	UBOOL					Fullscreen,
							Minimized,
							Maximized,
							Resizing,
							PreventCapture;

	UBOOL					KeyStates[256];
	UBOOL					bMouseHasMoved;

	// Info saved during captures and fullscreen sessions.
	//POINT					PreCaptureMousePos;
	//RECT					PreCaptureCursorClipRect;
	UBOOL					bCapturingMouseInput;
	UBOOL					bCapturingJoystickInput;
	UBOOL					bLockingMouseToWindow;

	enum EForceCapture		{EC_ForceCapture};

	// Constructor/destructor.
	FSDLViewport(USDLClient* InClient,FViewportClient* InViewportClient,const TCHAR* InName,UINT InSizeX,UINT InSizeY,UBOOL InFullscreen,HWND InParentWindow);

	// FChildViewport interface.
	virtual void* GetWindow() { return Window; }

	virtual UBOOL CaptureMouseInput(UBOOL Capture);
	virtual void LockMouseToWindow(UBOOL bLock);
	virtual UBOOL KeyState(FName Key);

	virtual UBOOL CaptureJoystickInput(UBOOL Capture);

	virtual INT GetMouseX();
	virtual INT GetMouseY();

	virtual UBOOL MouseHasMoved() { return bMouseHasMoved; }

	virtual UINT GetSizeX() { return SizeX; }
	virtual UINT GetSizeY() { return SizeY; }

	virtual void Draw(UBOOL Synchronous);
	virtual UBOOL ReadPixels(TArray<FColor>& OutputBuffer);
	virtual void InvalidateDisplay();

	virtual void GetHitProxyMap(UINT MinX,UINT MinY,UINT MaxX,UINT MaxY,TArray<HHitProxy*>& OutMap);
	virtual void Invalidate();

	// FViewport interface.
	virtual const TCHAR* GetName() { return *Name; }
	virtual UBOOL IsFullscreen() { return Fullscreen; }
	virtual void SetName(const TCHAR* NewName);
	virtual void Resize(UINT NewSizeX,UINT NewSizeY,UBOOL NewFullscreen);
	
	// FSDLViewport interface.
	void ShutdownAfterError();
	void ProcessInput( FLOAT DeltaTime );
	void HandlePossibleSizeChange();
	void Destroy();
};

#define AUTO_INITIALIZE_REGISTRANTS_SDLDRV \
	USDLClient::StaticClass();

#endif //_INC_SDLDRV
