/*=============================================================================
	ALAudioDevice.h: Unreal OpenAL Audio interface object.
	Copyright 1999-2004 Epic Games, Inc. All Rights Reserved.

Revision history:
	* Created by Daniel Vogel.
	* Ported to Linux by Ryan C. Gordon.
=============================================================================*/

#ifndef _INC_ALAUDIODEVICE
#define _INC_ALAUDIODEVICE

/*------------------------------------------------------------------------------------
	Dependencies, helpers & forward declarations.
------------------------------------------------------------------------------------*/

class UALAudioDevice;

#if SUPPORTS_PRAGMA_PACK
#pragma pack (push,8)
#endif
#define AL_NO_PROTOTYPES 1
#define ALC_NO_PROTOTYPES 1
#include "al.h"
#include "alc.h"
#if SUPPORTS_PRAGMA_PACK
#pragma pack (pop)
#endif

/*------------------------------------------------------------------------------------
	Helpers
------------------------------------------------------------------------------------*/

// Constants.

#define MAX_AUDIOCHANNELS 64

#if __WIN32__
#define AL_DLL TEXT("OpenAL32.dll")
#else
#define AL_DLL TEXT("libopenal.so")
#endif

#define SAFE_RELEASE(p) { if(p) { (p)->Release(); (p)=NULL; } }

/*------------------------------------------------------------------------------------
	FALSoundBuffer
------------------------------------------------------------------------------------*/

class FALSoundBuffer
{
public:
	// Constructor/ Destructor.
	FALSoundBuffer( UALAudioDevice* AudioDevice );
	~FALSoundBuffer();

	// Buffer creation (static).
	static FALSoundBuffer* Init( USoundNodeWave* InWave, UALAudioDevice* AudioDevice );

	// Variables.
	UALAudioDevice*		AudioDevice;
	TArray<ALuint>		BufferIds;
	/** Resource ID of associated USoundNodeWave */
	INT					ResourceID;
};


/*------------------------------------------------------------------------------------
	FALSoundSource
------------------------------------------------------------------------------------*/

class FALSoundSource : public FSoundSource
{
public:
	// Constructor/ Destructor.
	FALSoundSource( UAudioDevice* InAudioDevice )
	:	FSoundSource( InAudioDevice ),
		Playing( 0 ),
		Paused( 0 ),
		Buffer( NULL )
	{}

	// Initialization & update.
	UBOOL Init( FWaveInstance* WaveInstance );
	void Update();

	// Playback.
	void Play();
	void Stop();
	void Pause();

	// Query.
	UBOOL IsFinished();

protected:
	// Variables.
	UBOOL				Playing,
						Paused;
	ALuint				SourceId;
	FALSoundBuffer*		Buffer;

	friend class UALAudioDevice;
};


/*------------------------------------------------------------------------------------
	UALAudioDevice
------------------------------------------------------------------------------------*/

class UALAudioDevice : public UAudioDevice
{
	DECLARE_CLASS(UALAudioDevice,UAudioDevice,CLASS_Config,ALAudio)

	// Constructor.
	void StaticConstructor();

	// UAudioDevice interface.
	UBOOL Init();
	void Flush();
	void Update( UBOOL Realtime );

	// UObject interface.
	void Destroy();
	void ShutdownAfterError();

protected:
	// Dynamic binding.
	void FindProc( void*& ProcAddress, char* Name, char* SupportName, UBOOL& Supports, UBOOL AllowExt );
	void FindProcs( UBOOL AllowExt );
	UBOOL FindExt( const TCHAR* Name );

	// Error checking.
	UBOOL alError( const TCHAR* Text, UBOOL Log = 1 );

	// Cleanup.
	void Teardown();


	// Variables.

	/** The name of the OpenAL Device to open - defaults to "Generic Software" */
	FStringNoInit								DeviceName;
	/** All buffers that are currently loaded */
	TArray<FALSoundBuffer*>						Buffers;
	/** Map from resource ID to sound buffer */
	TDynamicMap<INT,FALSoundBuffer*>			WaveBufferMap;
	DOUBLE										LastHWUpdate;
	/** Next resource ID value used for registering USoundNodeWave objects */
	INT											NextResourceID;

	// AL specific.
	ALCdevice*									HardwareDevice;
	ALCcontext*									SoundContext;
	void*										DLLHandle;

	// Configuration.
	FLOAT										TimeBetweenHWUpdates;
	UBOOL										ReverseStereo;
	UBOOL										UseDefaultDriver;

	friend class FALSoundBuffer;
};

#define AUTO_INITIALIZE_REGISTRANTS_ALAUDIO	\
	UALAudioDevice::StaticClass();

#endif

