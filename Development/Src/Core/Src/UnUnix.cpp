/*=============================================================================
	UnIX: Unix port of UnVcWin32.cpp.
	Copyright 1997-1999 Epic Games, Inc. All Rights Reserved.

	Revision history:
		* Cloned by Mike Danylchuk
		* Severely amputated and mutilated by Brandon Reinhart
		* Surgically altered by Jack Porter
		* Mangled and rehabilitated by Brandon Reinhart
		* Obfuscated by Daniel Vogel
		* Peed on by Ryan C. Gordon
=============================================================================*/

#if __GNUG__

// Standard includes.
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <float.h>
#if defined(__MACH__)
    #include <mach/mach_time.h>
#else
    #include <time.h>
#endif
#include <limits.h>
#include <errno.h>
#include <pwd.h>

// System includes.
#include <sys/param.h>
#include <unistd.h>
#include <dirent.h>
#include <utime.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/resource.h>
#include <dlfcn.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <sched.h>

// Stack traces via C runtime...
#if ((MACOSX) || (__FreeBSD__))
    #define HAVE_EXECINFO_H 0
#else
    #define HAVE_EXECINFO_H 1
    #include <execinfo.h>
#endif

// Core includes.
#include "CorePrivate.h"

#include <locale.h>
#include <langinfo.h>

/*-----------------------------------------------------------------------------
	Globals
-----------------------------------------------------------------------------*/

// Environment
extern char **environ;

TCHAR *GUnixSpawnOnExit = NULL;

void unicode_str_to_stdout(const UNICHAR *str)
{
    if (str == NULL)
        str = TEXT("(null)");

    printf("UNICODE str: ");
    for (int i = 0; str[i]; i++)
        printf("%c", (ANSICHAR) str[i]);
    printf("\n");
}

ANSICHAR* unixToANSI( ANSICHAR* ACh, const UNICHAR* InUCh )
{
    if(InUCh)
    {
        INT Index;
        for(Index = 0;InUCh[Index] != 0;Index++)
        {
#if 0
            if (InUCh[Index] > 255)
            debugf("unixToANSI: can't convert character %d!", (int) InUCh[Index]);
#endif
            
            ACh[Index] = (ANSICHAR) InUCh[Index];
        }
        ACh[Index] = 0;
        return ACh;
    }
    else
    {
        ACh[0] = 0;
        return ACh;
    }
}

INT unixGetSizeANSI( const UNICHAR* InUCh )
{
    if(InUCh)
        return (wcslen(InUCh) + 1) * sizeof(ANSICHAR);
    else
        return sizeof(ANSICHAR);
}

UNICHAR* unixToUNICODE( UNICHAR* UCh, const ANSICHAR* InACh )
{
    if(InACh)
    {
        INT Index;
        for(Index = 0;InACh[Index] != 0;Index++)
        {
            UCh[Index] = (UNICHAR) InACh[Index];
        }
        UCh[Index] = 0;
        return UCh;
    }
    else
    {
        UCh[0] = 0;
        return UCh;
    }
}

INT unixGetSizeUNICODE( const ANSICHAR* InACh )
{
    if(InACh)
        return (strlen(InACh) + 1) * sizeof(UNICHAR);
    else
        return sizeof(UNICHAR);
}

UNICHAR* unixANSIToUNICODE(char* str)
{
    INT iLength = unixGetSizeUNICODE(str);
    UNICHAR* pBuffer = new UNICHAR[iLength];
    appStrcpy(pBuffer,TEXT(""));  // !!! FIXME: Is this really necessary?
    return unixToUNICODE(pBuffer,str);
}

UNICHAR* wcscpy( UNICHAR* _Dest, const UNICHAR* Src)
{
    UNICHAR *Dest = _Dest;

    while (*Src)
    {
        *Dest = *Src;
        Dest++;
        Src++;
    }

    *Dest = 0;

    return(_Dest);
}

UNICHAR* wcsncpy( UNICHAR* _Dest, const UNICHAR* Src, INT MaxLen )
{
    UNICHAR *Dest = _Dest;

    MaxLen--;  // always leave room for null char.
    for (INT i = 0; ((i < MaxLen) && (*Src)); i++)
    {
        *Dest = *Src;
        Dest++;
        Src++;
    }

    *Dest = 0;

    return(Dest);
}

INT wcslen( const UNICHAR* String )
{
    INT retval = 0;
    for (retval = 0; *String; String++)
        retval++;

    return(retval);
}

INT wcscmp( const UNICHAR* Str1, const UNICHAR *Str2 )
{
    while (true)
    {
        UNICHAR ch1 = *Str1;
        UNICHAR ch2 = *Str2;

        if ((ch1 == 0) && (ch1 == ch2))
            return(0);

        if (ch1 < ch2)
            return(-1);
        else if (ch1 > ch2)
            return(1);

        Str1++;
        Str2++;
    }

    return(1);  // shouldn't ever hit this.
}

INT wcsncmp( const UNICHAR* Str1, const UNICHAR *Str2, INT max )
{
	INT i = 0;
    while (true)
    {
        UNICHAR ch1 = *Str1;
        UNICHAR ch2 = *Str2;

        if (i == max)
            return(0);

        if ((ch1 == 0) && (ch1 == ch2))
            return(0);

        if (ch1 < ch2)
            return(-1);
        else if (ch1 > ch2)
            return(1);

        i++;
        Str1++;
        Str2++;
    }

    return(1);  // shouldn't ever hit this.
}

INT _wcsicmp( const UNICHAR* String1, const UNICHAR* String2 )
{
    /*
    printf("_wcsicmp:\n");
    printf("  "); unicode_str_to_stdout(String1);
    printf("  "); unicode_str_to_stdout(String2);
    */

    while (true)
    {
        INT ch1 = toupper((INT) *String1);
        INT ch2 = toupper((INT) *String2);

        if ((ch1 == 0) && (ch1 == ch2))
            return(0);

        if (ch1 < ch2)
            return(-1);
        else if (ch1 > ch2)
            return(1);

        String1++;
        String2++;
    }

    return(1);  // shouldn't ever hit this.
}

INT _wcsnicmp( const UNICHAR* Str1, const UNICHAR *Str2, INT max )
{
    /*
    printf("_wcsnicmp:\n");
    printf("  "); unicode_str_to_stdout(Str1);
    printf("  "); unicode_str_to_stdout(Str2);
    */

    INT i = 0;
    while (true)
    {
        UNICHAR ch1 = toupper((int) *Str1);
        UNICHAR ch2 = toupper((int) *Str2);

        if (i == max)
            return(0);

        if ((ch1 == 0) && (ch1 == ch2))
            return(0);

        if (ch1 < ch2)
            return(-1);
        else if (ch1 > ch2)
            return(1);

        i++;
        Str1++;
        Str2++;
    }

    return(1);  // shouldn't ever hit this.
}

UNICHAR* wcschr( const UNICHAR* String, const UNICHAR Find )
{
    while (*String)
    {
        if (*String == Find)
            return((UNICHAR *) String);

        String++;
    }

    return(NULL);
}

UNICHAR* wcsstr( const UNICHAR* String, const UNICHAR* Find )
{
    INT findlen = wcslen(Find);

    while (true)
    {
        String = wcschr(String, *Find);
        if ( String == NULL)
            return(NULL);

        if (wcsncmp(String, Find, findlen) == 0)
            return((UNICHAR *) String);

        String++;
    }

    return(NULL);  // shouldn't ever hit this point.
}

UNICHAR* wcscat( UNICHAR* String, const UNICHAR *Add )
{
    wcscpy(String + wcslen(String), Add);
    return(String);
}

UNICHAR* _wcsupr( UNICHAR* String )
{
    for (UNICHAR *ptr = String; *ptr; ptr++)
        *ptr = (UNICHAR) toupper((int) *ptr);

    return(String);
}

INT _wtoi( const UNICHAR* Str )
{
    ANSICHAR *ansistr = UNICHAR_TO_ANSI(Str);
    return(atoi(ansistr));
}

DOUBLE wcstod( const UNICHAR* Start, UNICHAR** End )
{
	ANSICHAR* StartAnsi = UNICHAR_TO_ANSI(Start);
	ANSICHAR* EndAnsi;

	DOUBLE ret = strtod(StartAnsi, End ? &EndAnsi : NULL);

	if (End) {
		SIZE_T EndOffset = (EndAnsi - StartAnsi) / sizeof(UNICHAR);
		*End = (UNICHAR*)Start + EndOffset;
	}

	return ret;
}

_LONG wcstol( const UNICHAR* Start, UNICHAR** End, INT Base )
{
	ANSICHAR* StartAnsi = UNICHAR_TO_ANSI(Start);
	ANSICHAR* EndAnsi;

	_LONG ret = strtol(StartAnsi, End ? &EndAnsi : NULL, Base);

	if (End) {
		SIZE_T EndOffset = (EndAnsi - StartAnsi) / sizeof(UNICHAR);
		*End = (UNICHAR*)Start + EndOffset;
	}

	return ret;
}

_ULONG wcstoul( const UNICHAR* Start, UNICHAR** End, INT Base )
{
	ANSICHAR* StartAnsi = UNICHAR_TO_ANSI(Start);
	ANSICHAR* EndAnsi;

	_ULONG ret = strtoul(StartAnsi, End ? &EndAnsi : NULL, Base);

	if (End) {
		SIZE_T EndOffset = (EndAnsi - StartAnsi) / sizeof(UNICHAR);
		*End = (UNICHAR*)Start + EndOffset;
	}

	return ret;
}

INT vswscanf(const UNICHAR *str, const UNICHAR *fmt, va_list args)
{
	const UNICHAR *f, *s;
	const UNICHAR point = localeconv()->decimal_point[0];
	INT cnv = 0;

	for (s = str, f = fmt; *f; ++f)
	{
		if (*f == u'%')
		{
			int size = 0;
			int width = 0;
			int do_cnv = 1;

			if (*++f == '*')
				++f, do_cnv = 0;

			for (; isdigit((INT)(UINT)*f); ++f)
				width *= 10, width += *f - u'0';

			if (*f == u'h' || *f == u'l' || *f == u'L')
				size = *f++;

			if (*f != u'[' && *f != u'c' && *f != u'n')
				while (isspace((INT)(UINT)*s))
					++s;

#define COPY                         *b++ = *s++, --width
#define MATCH(cond)                  if (width && (cond)) COPY;
#define MATCH_ACTION(cond, action)   if (width && (cond)) { COPY; action; }
#define MATCHES_ACTION(cond, action) while (width && (cond)) { COPY; action; }
#define FAIL                         (cnv) ? cnv : EOF
			switch (*f)
			{
				case u'd':
				case u'i':
				case u'o':
				case u'u':
				case u'x':
				case u'X':
				case u'p':
				{
					static const UNICHAR types[] = TEXT("diouxXp");
					static const INT bases[] = { 10, 0, 8, 10, 16, 16, 16 };
					static const UNICHAR digitset[] = TEXT("0123456789abcdefABCDEF");
					static const INT setsizes[] = { 10, 0, 0, 0, 0, 0, 0, 0, 8, 0, 10, 0, 0, 0, 0, 0, 22 };
					INT base = bases[wcschr(types, *f) - types];
					INT setsize;
					UNICHAR buf[513];
					UNICHAR *b = buf;
					INT digit = 0;
					if (width <= 0 || width > 512) width = 512;
					MATCH(*s == u'+' || *s == u'-')
					MATCH_ACTION(*s == u'0',
											 digit = 1;
															 MATCH_ACTION((*s == u'x' || *s == u'X') && (base == 0 || base == 16), base = 16) else base = 8;
					)
					setsize = setsizes[base];
					MATCHES_ACTION(memchr(digitset, *s, setsize), digit = 1)
					if (!digit) return FAIL;
					*b = '\0';
					if (do_cnv)
					{
						if (*f == u'd' || *f == u'i')
						{
							_LONG data = wcstol(buf, NULL, base);
							if (size == u'h')
								*va_arg(args, SHORT *) = (SHORT)data;
							else if (size == u'l')
								*va_arg(args, _LONG *) = data;
							else
								*va_arg(args, INT *) = (INT)data;
						}
						else
						{
							_ULONG data = wcstoul(buf, NULL, base);
							if (size == u'p')
								*va_arg(args, void **) = (void *)data;
							else if (size == u'h')
								*va_arg(args, _WORD *) = (_WORD)data;
							else if (size == u'l')
								*va_arg(args, _ULONG *) = data;
							else
								*va_arg(args, DWORD *) = (DWORD)data;
						}
						++cnv;
					}
					break;
				}

				case u'e':
				case u'E':
				case u'f':
				case u'g':
				case u'G':
				{
					UNICHAR buf[513];
					UNICHAR *b = buf;
					INT digit = 0;
					if (width <= 0 || width > 512) width = 512;
					MATCH(*s == u'+' || *s == u'-')
					MATCHES_ACTION(isdigit((INT)(UINT)*s), digit = 1)
					MATCH(*s == point)
					MATCHES_ACTION(isdigit((INT)(UINT)*s), digit = 1)
					MATCHES_ACTION(digit && (*s == u'e' || *s == u'E'),
												 MATCH(*s == u'+' || *s == u'-')
																 digit = 0;
																 MATCHES_ACTION(isdigit((INT)(UINT)*s), digit = 1)
					)
					if (!digit) return FAIL;
					*b = '\0';
					if (do_cnv)
					{
						DOUBLE data = wcstod(buf, NULL);
						if (size == u'l')
							*va_arg(args, DOUBLE *) = data;
						else if (size == u'L')
							*va_arg(args, LDOUBLE *) = (LDOUBLE)data;
						else
							*va_arg(args, FLOAT *) = (FLOAT)data;
						++cnv;
					}
					break;
				}

				case u's':
				{
					UNICHAR *arg = va_arg(args, UNICHAR *);
					if (width <= 0) width = INT_MAX;
					while (width-- && *s && !isspace((INT)(UINT)*s))
						if (do_cnv) *arg++ = *s++;
					if (do_cnv) *arg = u'\0', ++cnv;
					break;
				}

				case u'[':
				{
					UNICHAR *arg = va_arg(args, UNICHAR *);
					INT setcomp = 0;
					SIZE_T setsize;
					const UNICHAR *end;
					if (width <= 0) width = INT_MAX;
					if (*++f == u'^') setcomp = 1, ++f;
					end = wcschr((*f == u']') ? f + 1 : f, u']');
					if (!end) return FAIL; /* Could be cnv to match glibc-2.2 */
					setsize = end - f;     /* But FAIL matches the C standard */
					while (width-- && *s)
					{
						if (!setcomp && !memchr(f, *s, setsize)) break;
						if (setcomp && memchr(f, *s, setsize)) break;
						if (do_cnv) *arg++ = *s++;
					}
					if (do_cnv) *arg = u'\0', ++cnv;
					f = end;
					break;
				}

				case u'c':
				{
					UNICHAR *arg = va_arg(args, UNICHAR *);
					if (width <= 0) width = 1;
					while (width--)
					{
						if (!*s) return FAIL;
						if (do_cnv) *arg++ = *s++;
					}
					if (do_cnv) ++cnv;
					break;
				}

				case u'n':
				{
					if (size == u'h')
						*va_arg(args, SHORT *) = (SHORT)(s - str);
					else if (size == u'l')
						*va_arg(args, _LONG *) = (_LONG)(s - str);
					else
						*va_arg(args, INT *) = (INT)(s - str);
					break;
				}

				case u'%':
				{
					if (*s++ != u'%') return cnv;
					break;
				}

				default:
					return FAIL;
			}
		}
		else if (isspace((INT)(UINT)*f))
		{
			while (isspace((INT)(UINT)f[1]))
				++f;
			while (isspace((INT)(UINT)*s))
				++s;
		}
		else
		{
			if (*s++ != *f)
				return cnv;
		}
	}

	return cnv;
}

INT swscanf( const UNICHAR* str, const UNICHAR* fmt, ... )
{
	va_list args;
	UNICHAR buf[1024];  // (*shrug*)

	va_start(args, fmt);
	INT rc = vswscanf(str, fmt, args);
	va_end(args);

	return rc;
}

/* Format read states */

#define PARSE_DEFAULT    0
#define PARSE_FLAGS      1
#define PARSE_WIDTH      2
#define PARSE_DOT        3
#define PARSE_PRECISION  4
#define PARSE_SIZE       5
#define PARSE_CONVERSION 6
#define PARSE_DONE       7

/* Format flags */

#define FLAG_MINUS      (1 << 0)
#define FLAG_PLUS       (1 << 1)
#define FLAG_SPACE      (1 << 2)
#define FLAG_ALT        (1 << 3)
#define FLAG_ZERO       (1 << 4)
#define FLAG_UP         (1 << 5)
#define FLAG_UNSIGNED   (1 << 6)
#define FLAG_F          (1 << 7)
#define FLAG_E          (1 << 8)
#define FLAG_G          (1 << 9)
#define FLAG_PTR_SIGNED (1 << 10)
#define FLAG_PTR_NIL    (1 << 11)
#define FLAG_PTR_NOALT  (1 << 12)

/* Conversion flags */

#define SIZE_SHORT   1
#define SIZE_LONG    2
#define SIZE_LDOUBLE 3

static void outch(UNICHAR *buffer, SIZE_T *currlen, SIZE_T *reqlen, SIZE_T size, INT c)
{
	if (*currlen + 1 < size)
		buffer[(*currlen)++] = (UNICHAR)c;

	++*reqlen;
}

static void outstr_rev(UNICHAR *buffer, SIZE_T *currlen, SIZE_T *reqlen, SIZE_T size, const UNICHAR* str)
{
	INT len = appStrlen(str);
	for (INT i = len - 1; i >= 0; i--)
		outch(buffer, currlen, reqlen, size, str[i]);
}

static void fmtch(UNICHAR *buffer, SIZE_T *currlen, SIZE_T *reqlen, SIZE_T size, INT value, INT flags, INT width)
{
	INT padlen;

	/*
	** A negative field width argument is taken as a - flag followed by a
	** positive field width argument
	*/

	if (width < 0)
	{
		flags |= FLAG_MINUS;
		width = -width;
	}

	padlen = width - 1;
	if (padlen < 0)
		padlen = 0;

	if (flags & FLAG_MINUS)
		padlen = -padlen;

	while (padlen > 0)
	{
		outch(buffer, currlen, reqlen, size, u' ');
		--padlen;
	}

	outch(buffer, currlen, reqlen, size, (UINT)value);

	while (padlen < 0)
	{
		outch(buffer, currlen, reqlen, size, u' ');
		++padlen;
	}
}

static void fmtstr(UNICHAR *buffer, SIZE_T *currlen, SIZE_T *reqlen, SIZE_T size, UNICHAR *value, INT flags, INT width, INT precision)
{
	INT padlen, bytes;
	INT cnt = 0;

	/*
	** A negative field width argument is taken as a - flag followed by a
	** positive field width argument
	*/

	if (width < 0)
	{
		flags |= FLAG_MINUS;
		width = -width;
	}

	/* A negative precision argument is taken as if the precision were omitted */

	if (precision < 0)
		precision = -1;

	/* Print NULL as "(null)" if possible, otherwise as "" (like glibc) */

	if (value == NULL)
		value = (UNICHAR*)((precision == -1 || precision >= 6) ? u"(null)" : u"");

	for (bytes = 0; (precision == -1 || bytes < precision) && value[bytes]; ++bytes)
	{}

	padlen = width - bytes;
	if (padlen < 0)
		padlen = 0;
	if (flags & FLAG_MINUS)
		padlen = -padlen;

	while (padlen > 0)
	{
		outch(buffer, currlen, reqlen, size, u' ');
		--padlen;
	}

	while (*value && (precision == -1 || (cnt < precision)))
	{
		outch(buffer, currlen, reqlen, size, *value++);
		++cnt;
	}

	while (padlen < 0)
	{
		outch(buffer, currlen, reqlen, size, u' ');
		++padlen;
	}
}

static void fmtint(UNICHAR *buffer, SIZE_T *currlen, SIZE_T *reqlen, SIZE_T size, _LONG value, INT base, INT width, INT precision, INT flags)
{
	INT signvalue = 0;
	DWORD uvalue;
	BYTE convert[22];
	INT place = 0;
	INT spadlen = 0;
	INT zpadlen = 0;
	UNICHAR *digits;
	INT zextra = 0;
	INT sextra = 0;

	/*
	** A negative field width argument is taken as a - flag followed by a
	** positive field width argument
	*/

	if (width < 0)
	{
		flags |= FLAG_MINUS;
		width = -width;
	}

	/* A negative precision argument is taken as if the precision were omitted */

	if (precision < 0)
		precision = -1;

	/* If the space and + flags both appear, the space flag will be ignored */

	if (flags & FLAG_PLUS)
		flags &= ~FLAG_SPACE;

	/* If the 0 and - flags both appear, the 0 flag will be ignored */

	if (flags & FLAG_MINUS)
		flags &= ~FLAG_ZERO;

	/* If a precision is specified, the 0 flag will be ignored */
	/* The d, i, o, u, x and X conversions have a default precision of 1 */

	if (precision == -1)
		precision = 1;
	else
		flags &= ~FLAG_ZERO;

	/*
	** The + flag: The result of the conversion will always begin with a plus
	** or minus sign. (It will begin with a sign only when a negative value is
	** converted if this flag is not specified.)
	**
	** The space flag: If the first character of a signed conversion is not a
	** sign, or if a signed conversion results in no characters, a space will
	** be prefixed to the result.
	*/

	uvalue = (DWORD)value;

	if (!(flags & FLAG_UNSIGNED))
	{
		if (value < 0)
		{
			signvalue = u'-';
			uvalue = (DWORD)-value;
		}
		else if (flags & FLAG_PLUS)
			signvalue = u'+';
		else if (flags & FLAG_SPACE)
			signvalue = u' ';
	}

	/* Behave like glibc (treat %p as signed (almost)) */
	if (flags & FLAG_PTR_SIGNED && value)
	{
		if (flags & FLAG_PLUS)
			signvalue = u'+';
		else if (flags & FLAG_SPACE)
			signvalue = u' ';
	}

	if (flags & FLAG_PTR_NIL && !value)
	{
		spadlen = width - 5;
		if (spadlen < 0)
			spadlen = 0;
		if (flags & FLAG_MINUS)
			spadlen = -spadlen;

		while (spadlen > 0)
		{
			outch(buffer, currlen, reqlen, size, u' ');
			--spadlen;
		}

		outch(buffer, currlen, reqlen, size, u'(');
		outch(buffer, currlen, reqlen, size, u'n');
		outch(buffer, currlen, reqlen, size, u'i');
		outch(buffer, currlen, reqlen, size, u'l');
		outch(buffer, currlen, reqlen, size, u')');

		while (spadlen < 0)
		{
			outch(buffer, currlen, reqlen, size, u' ');
			++spadlen;
		}

		return;
	}

	/* %[XEG] produce uppercase alpha characters */

	digits = (UNICHAR*)((flags & FLAG_UP) ? u"0123456789ABCDEF" : u"0123456789abcdef");

	/*
	** The result of converting a zero value with a precision of zero is no
	** characters
	*/

	if (precision || uvalue)
	{
		do
		{
			convert[place++] = digits[uvalue % base];
			uvalue /= base;
		}
		while (uvalue && (place < 21));
	}

	convert[place] = 0;

	/*
	** The # flag: For o conversion, it increases the precision to force the
	** first digit of the result to be a zero. For x (or X) conversion, a
	** non-zero result will have 0x (or 0X) prefixed to it.
	*/

	if (flags & FLAG_ALT)
	{
		if (base == 16 && value)
			sextra = 2;
		else if (base == 8 && precision <= place && (place == 0 || convert[place - 1] != u'0'))
			zextra = 1;
	}

	/*
	** The 0 flag: Leading zeroes (following any indication of sign or base)
	** are used to pad to the field width; no space padding is performed.
	*/

	zpadlen = precision - place;
	if (zpadlen < 0)
		zpadlen = 0;
	zpadlen += zextra;

	spadlen = width - (place + zpadlen) - (signvalue ? 1 : 0) - sextra;
	if (spadlen < 0)
		spadlen = 0;

	if (flags & FLAG_ZERO)
	{
		zpadlen = Max(zpadlen, spadlen);
		spadlen = 0;
	}

	if (flags & FLAG_MINUS)
		spadlen = -spadlen;

	while (spadlen > 0)
	{
		outch(buffer, currlen, reqlen, size, u' ');
		--spadlen;
	}

	if (signvalue)
		outch(buffer, currlen, reqlen, size, signvalue);

	if (flags & FLAG_ALT && base == 16 && sextra == 2)
	{
		outch(buffer, currlen, reqlen, size, u'0');
		outch(buffer, currlen, reqlen, size, (flags & FLAG_UP) ? u'X' : u'x');
	}

	while (zpadlen > 0)
	{
		outch(buffer, currlen, reqlen, size, u'0');
		--zpadlen;
	}

	while (place > 0)
		outch(buffer, currlen, reqlen, size, convert[--place]);

	while (spadlen < 0)
	{
		outch(buffer, currlen, reqlen, size, u' ');
		++spadlen;
	}
}

#define FMTFP_MAX_VALUE 1e15
#define FMTFP_BUFFER_SIZE 64
#define FMTFP_DEFAULT_PRECISION 6

static void fmtfp(UNICHAR *buffer, SIZE_T *currlen, SIZE_T *reqlen, SIZE_T size, LDOUBLE fvalue, INT width, INT prec, INT flags)
{
	UNICHAR buf[FMTFP_BUFFER_SIZE];
	size_t len  = 0U;

	// powers of 10
	static const DOUBLE pow10[] = {
					1,
					10,
					100,
					1000,
					10000,
					100000,
					1000000,
					10000000,
					100000000,
					1000000000,
					10000000000,
					100000000000,
					1000000000000,
					10000000000000,
					100000000000000,
					1000000000000000,
	};

	// test for special values
	if (fvalue != fvalue)
	{
		outstr_rev(buffer, currlen, reqlen, size, u"nan");
		return;
	}
	if (fvalue < -DBL_MAX)
	{
		outstr_rev(buffer, currlen, reqlen, size, u"fni-");
		return;
	}
	if (fvalue > DBL_MAX)
	{
		outstr_rev(buffer, currlen, reqlen, size, (flags & FLAG_PLUS) ? u"fni+" : u"fni");
		return;
	}
	// test for very large values
	// standard printf behavior is to print EVERY whole number digit -- which could be 100s of characters overflowing your buffers == bad
	if ((fvalue > FMTFP_MAX_VALUE) || (fvalue < -FMTFP_MAX_VALUE)) {
		return;
	}

	// test for negative
	UBOOL negative = 0;
	if (fvalue < 0) {
		negative = 1;
		fvalue = 0 - fvalue;
	}

	// if precision not specified, use default
	if (prec < 0) {
		prec = FMTFP_DEFAULT_PRECISION;
	}

	// limit precision to 15, cause a prec >= 15 can lead to overflow errors
	while ((len < FMTFP_BUFFER_SIZE) && (prec > 15)) {
		buf[len++] = '0';
		prec--;
	}

	QWORD whole = (QWORD)fvalue;
	LDOUBLE tmp = (fvalue - whole) * pow10[prec];
	QWORD frac = (QWORD)tmp;
	LDOUBLE diff = tmp - frac;

	if (diff > 0.5)
	{
		++frac;
		// handle rollover, e.g. case 0.99 with prec 1 is 1.0
		if (frac >= pow10[prec])
		{
			frac = 0;
			++whole;
		}
	}
	else if (diff < 0.5)
	{
	}
	else if ((frac == 0U) || (frac & 1U))
	{
		// if halfway, round up if odd OR if last digit is 0
		++frac;
	}

	if (prec == 0)
	{
		diff = fvalue - (LDOUBLE)whole;
		if ((!(diff < 0.5) || (diff > 0.5)) && (whole & 1))
		{
			// exactly 0.5 and ODD, then round up
			// 1.5 -> 2, but 2.5 -> 2
			++whole;
		}
	}
	else
	{
		INT count = prec;
		// now do fractional part, as an unsigned number
		while (len < FMTFP_BUFFER_SIZE)
		{
			--count;
			buf[len++] = (UNICHAR)(48U + (frac % 10U));
			if (!(frac /= 10U)) {
				break;
			}
		}
		// add extra 0s
		while ((len < FMTFP_BUFFER_SIZE) && (count-- > 0))
			buf[len++] = u'0';

		if (len < FMTFP_BUFFER_SIZE)
		{
			// add decimal
			buf[len++] = u'.';
		}
	}

	// do whole part, number is reversed
	while (len < FMTFP_BUFFER_SIZE)
	{
		buf[len++] = (UNICHAR)(48 + (whole % 10));
		if (!(whole /= 10)) {
			break;
		}
	}

	// pad leading zeros
	if (!(flags & FLAG_MINUS) && (flags & FLAG_ZERO))
	{
		if (width && (negative || (flags & (FLAG_PLUS | FLAG_SPACE))))
			width--;

		while ((len < width) && (len < FMTFP_BUFFER_SIZE))
			buf[len++] = '0';
	}

	if (len < FMTFP_BUFFER_SIZE)
	{
		if (negative)
			buf[len++] = u'-';
		else if (flags & FLAG_PLUS)
			buf[len++] = u'+';  // ignore the space if the '+' exists
		else if (flags & FLAG_SPACE)
			buf[len++] = u' ';
	}

	if (len < FMTFP_BUFFER_SIZE)
		buf[len] = u'\0';
	else
		buf[FMTFP_BUFFER_SIZE - 1] = u'\0';

	outstr_rev(buffer, currlen, reqlen, size, buf);
}

INT wvsnprintf( UNICHAR *str, INT size, const UNICHAR *fmt, va_list args )
{
	UNICHAR ch;
	_LONG value;
	LDOUBLE fvalue;
	UNICHAR *strvalue;
	INT width;
	INT precision;
	INT state;
	INT flags;
	INT cflags;
	SIZE_T currlen;
	SIZE_T reqlen;

	state = PARSE_DEFAULT;
	currlen = reqlen = flags = cflags = width = 0;
	precision = -1;
	ch = *fmt++;

	while (state != PARSE_DONE)
	{
		if (ch == u'\0')
		{
			if (state == PARSE_DEFAULT)
				state = PARSE_DONE;
			else
				return -1;
		}

		switch (state)
		{
			case PARSE_DEFAULT:
				if (ch == u'%')
					state = PARSE_FLAGS;
				else
					outch(str, &currlen, &reqlen, size, ch);
				ch = *fmt++;
				break;

			case PARSE_FLAGS:
				switch (ch)
				{
					case u'-':
						flags |= FLAG_MINUS;
						ch = *fmt++;
						break;

					case u'+':
						flags |= FLAG_PLUS;
						ch = *fmt++;
						break;

					case u' ':
						flags |= FLAG_SPACE;
						ch = *fmt++;
						break;

					case u'#':
						flags |= FLAG_ALT;
						ch = *fmt++;
						break;

					case u'0':
						flags |= FLAG_ZERO;
						ch = *fmt++;
						break;

					default:
						state = PARSE_WIDTH;
						break;
				}
				break;

			case PARSE_WIDTH:
				if (isdigit((INT)(BYTE)ch))
				{
					width = 10 * width + ch - u'0';
					ch = *fmt++;
				}
				else if (ch == u'*')
				{
					width = va_arg(args, INT);
					ch = *fmt++;
					state = PARSE_DOT;
				}
				else
					state = PARSE_DOT;
				break;

			case PARSE_DOT:
				if (ch == u'.')
				{
					precision = 0;
					state = PARSE_PRECISION;
					ch = *fmt++;
				}
				else
					state = PARSE_SIZE;
				break;

			case PARSE_PRECISION:
				if (isdigit((INT)(BYTE)ch))
				{
					precision = 10 * precision + ch - u'0';
					ch = *fmt++;
				}
				else if (ch == u'*')
				{
					precision = va_arg(args, INT);
					ch = *fmt++;
					state = PARSE_SIZE;
				}
				else
					state = PARSE_SIZE;
				break;

			case PARSE_SIZE:
				switch (ch)
				{
					case u'h':
						cflags = SIZE_SHORT;
						ch = *fmt++;
						break;

					case u'l':
						cflags = SIZE_LONG;
						ch = *fmt++;
						break;

					case u'L':
						cflags = SIZE_LDOUBLE;
						ch = *fmt++;
						break;
				}

				state = PARSE_CONVERSION;
				break;

			case PARSE_CONVERSION:
				switch (ch)
				{
					case u'd':
					case u'i':
						if (cflags == SIZE_SHORT)
							value = (SHORT)va_arg(args, INT);
						else if (cflags == SIZE_LONG)
							value = (_LONG)va_arg(args, _LONG);
						else
							value = (INT)va_arg(args, INT);

						fmtint(str, &currlen, &reqlen, size, value, 10, width, precision, flags);
						break;

					case u'o':
						flags |= FLAG_UNSIGNED;
						if (cflags == SIZE_SHORT)
							value = (_WORD)va_arg(args, INT);
						else if (cflags == SIZE_LONG)
							value = (DWORD)va_arg(args, _LONG);
						else
							value = (UINT)va_arg(args, INT);

						fmtint(str, &currlen, &reqlen, size, value, 8, width, precision, flags);
						break;

					case u'u':
						flags |= FLAG_UNSIGNED;
						if (cflags == SIZE_SHORT)
							value = (_WORD)va_arg(args, INT);
						else if (cflags == SIZE_LONG)
							value = (DWORD)va_arg(args, _LONG);
						else
							value = (UINT)va_arg(args, INT);

						fmtint(str, &currlen, &reqlen, size, value, 10, width, precision, flags);
						break;

					case u'X':
						flags |= FLAG_UP;
					case u'x':
						flags |= FLAG_UNSIGNED;
						if (cflags == SIZE_SHORT)
							value = (_WORD)va_arg(args, INT);
						else if (cflags == SIZE_LONG)
							value = (DWORD)va_arg(args, _LONG);
						else
							value = (UINT)va_arg(args, INT);

						fmtint(str, &currlen, &reqlen, size, value, 16, width, precision, flags);
						break;

					case u'f':
						flags |= FLAG_F;
						if (cflags == SIZE_LDOUBLE)
							fvalue = va_arg(args, LDOUBLE);
						else
							fvalue = (LDOUBLE)va_arg(args, DOUBLE);

						fmtfp(str, &currlen, &reqlen, size, fvalue, width, precision, flags);
						break;

					case u'E':
						flags |= FLAG_UP;
					case u'e':
						flags |= FLAG_E;
						if (cflags == SIZE_LDOUBLE)
							fvalue = va_arg(args, LDOUBLE);
						else
							fvalue = (LDOUBLE)va_arg(args, DOUBLE);

						// FIXME: fallback to float instead of exponent
						fmtfp(str, &currlen, &reqlen, size, fvalue, width, precision, flags);
						break;

					case u'G':
						flags |= FLAG_UP;
					case u'g':
						flags |= FLAG_G;
						if (cflags == SIZE_LDOUBLE)
							fvalue = va_arg(args, LDOUBLE);
						else
							fvalue = (LDOUBLE)va_arg(args, DOUBLE);

						fmtfp(str, &currlen, &reqlen, size, fvalue, width, precision, flags);
						break;

					case u'c':
						fmtch(str, &currlen, &reqlen, size, va_arg(args, INT), flags, width);
						break;

					case u's':
						strvalue = va_arg(args, UNICHAR *);
						fmtstr(str, &currlen, &reqlen, size, strvalue, flags, width, precision);
						break;

					case u'p':
						flags |= FLAG_UNSIGNED;
						flags |= FLAG_ALT;
						flags |= FLAG_PTR_SIGNED;
						flags |= FLAG_PTR_NIL;
						flags |= FLAG_PTR_NOALT;
						cflags = SIZE_LONG;
						strvalue = (UNICHAR*)va_arg(args, void *);
						fmtint(str, &currlen, &reqlen, size, (PTRINT)strvalue, 16, width, precision, flags);
						break;

					case u'n':
						if (cflags == SIZE_SHORT)
						{
							SHORT *num = va_arg(args, SHORT *);
							*num = (SHORT)currlen;
						}
						else if (cflags == SIZE_LONG)
						{
							_LONG *num = va_arg(args, _LONG *);
							*num = (_LONG)currlen;
						}
						else
						{
							INT *num = va_arg(args, INT *);
							*num = currlen;
						}

						break;

					case u'%':
						if (flags != 0 || cflags != 0 || width != 0 || precision != 0)
							return -1;

						outch(str, &currlen, &reqlen, size, ch);
						break;

					default:
						return -1;
				}

				ch = *fmt++;
				state = PARSE_DEFAULT;
				flags = cflags = width = 0;
				precision = -1;
				break;

			case PARSE_DONE:
				break;
		}
	}

	if (size)
		str[currlen] = u'\0';

	return reqlen;
}

INT wprintf( const UNICHAR* fmt, ... )
{
    va_list args;
    UNICHAR buf[1024];  // (*shrug*)

    va_start(args, fmt);
    INT rc = wvsnprintf(buf, sizeof (buf) / sizeof (UNICHAR), fmt, args);
    va_end(args);

		fwrite(buf, sizeof (UNICHAR), rc, stdout);

    return(rc);
}

INT iswspace( UNICHAR ch  )
{
    return((INT) isspace((int) ch));
}

UNICHAR *_itow( const INT Num, UNICHAR *Buffer, const INT Radix )
{
    return(_i64tow((SQWORD) Num, Buffer, Radix));
}

QWORD _wcstoui64( const UNICHAR* Start, UNICHAR** End, INT Base )
{
    check((Base >= 2) && (Base <= 36));

    while (*Start == u' ')
        Start++;

    const UNICHAR *ptr = Start;
    while (1)
    {
        char ch = toupper((char) *ptr);

        if ((ch >= u'A') && (ch <= u'Z'))
        {
            if ((Base <= 10) || (ch >= (u'A' + Base)))
                break;
        }

        else if ((ch >= u'0') && (ch <= u'9'))
        {
            if (ch >= (u'0' + Base))
                break;
        }

        else
            break;

        ptr++;
    }

    if (End != NULL)
        *End = const_cast<TCHAR *>(ptr);

    QWORD retval = 0;
    QWORD accumulator = 1;
    while (--ptr >= Start)
    {
        char ch = toupper((char) *ptr);
        QWORD val = (((ch >= u'A') && (ch <= u'Z')) ? (ch - u'A') + 10 : ch - u'0');
        retval += val * accumulator;
        accumulator *= (QWORD) Base;
    }

    return(retval);
}


UNICHAR* _ui64tow( QWORD Num, UNICHAR *Buffer, INT Base )
{
    check((Base >= 2) && (Base <= 36));

    int bufidx = 1;
    for (QWORD x = Num; x >= Base; x /= Base)
        bufidx++;

    Buffer[bufidx] = 0;  /* null terminate where the end will be. */

    while (--bufidx >= 0)
    {
        int val = (int) (Num % Base);
        Buffer[bufidx] = ((val > 9) ? ('a' + (val - 10)) : ('0' + val));
        Num /= Base;
    }

    return(Buffer);
}

UNICHAR* _i64tow( SQWORD Num, UNICHAR *Buffer, INT Base )
{
    check((Base >= 2) && (Base <= 36));

    int bufidx = 1;
    int negative = 0;
    if (Num < 0)
    {
        negative = 1;
        Num = -Num;
        bufidx++;
        Buffer[0] = u'-';
    }

    for (QWORD x = Num; x >= Base; x /= Base)
        bufidx++;

    Buffer[bufidx] = 0;  /* null terminate where the end will be. */

    while ((--bufidx - negative) >= 0)
    {
        int val = (int) (Num % Base);
        Buffer[bufidx] = ((val > 9) ? (u'a' + (val - 10)) : (u'0' + val));
        Num /= Base;
    }

    return(Buffer);
}


FLOAT _wtof( const UNICHAR* Str )
{
    return((FLOAT) atof(TCHAR_TO_ANSI(Str)));
}


/* visual C++ compatibility. --ryan. */
ANSICHAR *strlwr(ANSICHAR *str)
{
    for (char *p = str; *p; p++)
        *p = (char) tolower((int) *p);

    return(str);
}


const UBOOL appMsgf( INT Type, const TCHAR* Fmt, ... )
{
	TCHAR TempStr[4096]=TEXT("");
	GET_VARARGS( TempStr, ARRAY_COUNT(TempStr), Fmt, Fmt );
    printf("appMsgf(): %s\n", TCHAR_TO_ANSI(TempStr));
    appDebugBreak();
    return 1;
}

/*-----------------------------------------------------------------------------
	Exit.
-----------------------------------------------------------------------------*/

//
// Immediate exit.
//
void appRequestExit( UBOOL Force )
{
	debugf( TEXT("appRequestExit(%i)"), Force );
	if( Force )
	{
		// Force immediate exit. Dangerous because config code isn't flushed, etc.
		exit( 1 );
	}
	else
	{
		// Tell the platform specific code we want to exit cleanly from the main loop.
		//!UNIX No quit message in UNIX.
		GIsRequestingExit = 1;
	}
}

/*-----------------------------------------------------------------------------
	Clipboard.
-----------------------------------------------------------------------------*/

void ClipboardCopy( const TCHAR* Str )
{
	//!UNIX Not supported in UNIX.
}

void ClipboardPaste( FString& Result )
{
	//!UNIX Not supported in UNIX.
}

/*-----------------------------------------------------------------------------
	Shared libraries.
-----------------------------------------------------------------------------*/

//
// Load a library.
//
void* appGetDllHandle( const TCHAR* Filename )
{
	check(Filename);
	void* Result;
    
    dlerror();	// Clear any error condition.

	// Load the library.
	Result = (void*)dlopen( TCHAR_TO_ANSI(Filename), RTLD_NOW | RTLD_GLOBAL );
	if( Result == NULL )
	{
		TCHAR Temp[256];
		appStrcpy( Temp, Filename );
		appStrcat( Temp, DLLEXT );
		Result = (void*)dlopen( TCHAR_TO_ANSI(Temp), RTLD_NOW | RTLD_GLOBAL );
#ifdef LOOKING_FOR_UNDEFINED_SYMBOLS
		const char *Error = dlerror();
		if( Error != NULL )
            debugf( "dlerror(): %s", Error );
#endif
	}

	return Result;
}

//
// Free a library.
//
void appFreeDllHandle( void* DllHandle )
{
	check(DllHandle);

	if (DllHandle == (void *) -1)
		return;

	// remember that you can unload bundles,
	//  but not .dylib files on Darwin! --ryan.
	dlclose( DllHandle );
}

//
// Lookup the address of a shared library function.
//
void* appGetDllExport( void* DllHandle, const TCHAR* ProcName )
{
	check(DllHandle);
	check(ProcName);

	void* Result = NULL;

	dlerror();	// Clear any error condition.

	Result = (void*)dlsym( DllHandle, TCHAR_TO_ANSI(ProcName) );
#ifdef LOOKING_FOR_UNDEFINED_SYMBOLS
    const char* Error = dlerror();
	if( Error != NULL )
		debugf( TEXT("dlerror: %s"), ANSI_TO_TCHAR(Error) );
#endif
	return Result;
}

//
// Break the debugger.
//
void appDebugBreak()
{
#if defined(__INTEL_COMPILER)
    __debugbreak()
#elif defined(__ARMCC_VERSION)
    __breakpoint(42)
#elif defined(__i386__) || defined(__x86_64__)
    __asm__ __volatile__("int3");
#elif defined(__thumb__)
    __asm__ __volatile__(".inst 0xde01");
#elif defined(__aarch64__)
    __asm__ __volatile__(".inst 0xd4200000");
#elif defined(__arm__)
    __asm__ __volatile__(".inst 0xe7f001f0");
#else
    raise(SIGTRAP);
#endif
}

//
// IsDebuggerPresent()
//
UBOOL appIsDebuggerPresent()
{
	// Does not need to be implemented.
	return 0;
}

// Interface for recording loading errors in the editor
const void EdClearLoadErrors()
{
    GEdLoadErrors.Empty();
}

VARARG_BODY( const void VARARGS, EdLoadErrorf, const TCHAR*, VARARG_EXTRA(INT Type) )
{
    TCHAR TempStr[4096]=TEXT("");
    GET_VARARGS( TempStr, ARRAY_COUNT(TempStr), Fmt, Fmt );
    
    // Check to see if this error already exists ... if so, don't add it.
    // NOTE : for some reason, I can't use AddUniqueItem here or it crashes
    for( INT x = 0 ; x < GEdLoadErrors.Num() ; ++x )
    if( GEdLoadErrors(x) == FEdLoadError( Type, TempStr ) )
        return;
    
    new( GEdLoadErrors )FEdLoadError( Type, TempStr );
}

/*-----------------------------------------------------------------------------
	External processes.
-----------------------------------------------------------------------------*/

static TMap<pid_t,int>* ExitCodeMap = NULL;

void* appCreateProc( const TCHAR* URL, const TCHAR* Parms )
{
	debugf( TEXT("Create Proc: %s %s"), URL, Parms );

	TCHAR LocalParms[PATH_MAX] = TEXT("");
	appStrcpy(LocalParms, URL);
	appStrcat(LocalParms, TEXT(" "));
	appStrcat(LocalParms, Parms);

	pid_t pid = fork();
	if (pid == 0)
		_exit( system( TCHAR_TO_ANSI(LocalParms) ) );

	return (void*) ((PTRINT) pid);
}

UBOOL appGetProcReturnCode( void* ProcHandle, INT* ReturnCode )
{
#if 0
	int* p = ExitCodeMap->Find( (pid_t) ((PTRINT) ProcHandle) );
	if(p)
	{
		*ReturnCode = *p;
		ExitCodeMap->Remove( (pid_t) ((PTRINT) ProcHandle) );
		return 1;
	}
#endif
	return 0;
}

/*-----------------------------------------------------------------------------
	Signal Handling.
-----------------------------------------------------------------------------*/

static void HandleChild(int Signal)
{
	int Status;
	pid_t pid;
	while( (pid=waitpid(-1, &Status, WNOHANG)) > 0 )
		ExitCodeMap->Set( pid, WEXITSTATUS(Status) );
}

static pid_t mainPID;
static INT AlreadyAborting	= 0;
static INT SignalExit		= 0;
static INT SignalCritical	= 0;

static void HandleSignal( int Signal )
{
	const char* sig_text;
	switch (Signal)
	{
		case SIGHUP:
			sig_text = "Signal: SIGHUP [hangup]";
			SignalExit++;
			break;
		case SIGQUIT:
			sig_text = "Signal: SIGQUIT [quit]";
			SignalExit++;
			break;
		case SIGILL:
			sig_text = "Signal: SIGILL [illegal instruction]";
			SignalCritical++;
			break;
		case SIGTRAP:
			sig_text = "Signal: SIGTRAP [trap]";
			SignalCritical++;
			break;
		case SIGIOT:
			sig_text = "Signal: SIGIOT [iot trap]";
			SignalCritical++;
			break;
		case SIGBUS:
			sig_text = "Signal: SIGBUS [bus error]";
			SignalCritical++;
			break;
		case SIGFPE:
			sig_text = "Signal: SIGFPE [floating point exception]";
			SignalCritical++;
			break;
		case SIGSEGV:
			sig_text = "Signal: SIGSEGV [segmentation fault]";
			SignalCritical++;
			break;
		case SIGTERM:
			sig_text = "Signal: SIGTERM [terminate]";
			SignalExit++;
			break;
		case SIGINT:  // must have gone through HandleInterrupt previously.
			sig_text = "Signal: SIGINT [interrupt]";
			SignalCritical++;
			break;
		default:
			sig_text = "Signal: UNKNOWN SIGNAL [%i]";
			SignalCritical++;
			break;
	}

	if ( (SignalCritical > 0) || (SignalExit > 1) )
	{
		if (!AlreadyAborting)
		{
			// Avoid calling appExit again.
			AlreadyAborting = true;

			printf( "%s\n", sig_text );
			printf( "Aborting.\n\n" );
			printf( "\nCrash information will be saved to your logfile.\n" );

            #if HAVE_EXECINFO_H
                void *aTrace[64];
                char **tString;
                int size = backtrace(aTrace, ARRAY_COUNT(aTrace));
                tString = backtrace_symbols(aTrace, size);
                debugf(TEXT("\nDeveloper Backtrace:"));
                for (int i = 0; i < size; i++)
                    debugf(TEXT("[%2.i]  %s"), i+1, appFromAnsi(tString[i]));
            #endif

			appExit();
		}
		exit(1);
	}
	if ( SignalExit == 1 )
	{
		printf("%s\n", sig_text);
		printf("Requesting Exit.\n");
		appRequestExit( 0 );
	}
}


// special case for CTRL-C.  --ryan.
static void HandleInterrupt( int Signal )
{
    check(Signal==SIGINT);

    if (getpid() == mainPID)
    {
        static bool alreadyTriggered = false;

        if (alreadyTriggered)
        {
            // User is mashing CTRL-C, or appRequestExit() work is deadlocked.
            // Pass SIGINT to our standard interrupt handler for panic crash.
            HandleSignal(Signal);
        }

        alreadyTriggered = true;

        if (GIsRunning)   // already in main loop.
        {
            // let the engine try to shut down cleanly.
            debugf(TEXT("CTRL-C has been pressed.\n"));
            appRequestExit(0);
        }

        else
        {
            // Need to force an immediate termination with _exit(),
            //  since things with static destructors may not be initialized
            //  yet! Otherwise, this could cause a confusing segfault in
            //  the normal exit() processing. Please note that calling
            //  appRequestExit(1) will result in a normal exit() call.
            printf("CTRL-C before main loop ... forcing exit.\n");
            _exit(42);  // all threads should die.
        }
    }
}


/*-----------------------------------------------------------------------------
	Timing.
-----------------------------------------------------------------------------*/

//
// Get file time.
//
DWORD appGetTime( const TCHAR* Filename )
{
	struct utimbuf FileTime;
	if( utime(TCHAR_TO_ANSI(Filename),&FileTime)!=0 )
		return 0;
	//FIXME Is this the right "time" to return?
	return (DWORD)FileTime.modtime;
}

//
// Get time in seconds.
//
DOUBLE appSecondsSlow()
{
	static DOUBLE InitialTime = 0.0;
	static DOUBLE TimeCounter = 0.0;
	DOUBLE NewTime;
	struct timeval TimeOfDay;

	gettimeofday( &TimeOfDay, NULL );

	// Initialize.
	if( InitialTime == 0.0 )
		 InitialTime = TimeOfDay.tv_sec + TimeOfDay.tv_usec / 1000000.0;

	// Accumulate difference to prevent wraparound.
	NewTime = TimeOfDay.tv_sec + TimeOfDay.tv_usec / 1000000.0;
	TimeCounter += NewTime - InitialTime;
	InitialTime = NewTime;

	return TimeCounter;
}

#if !DEFINED_appCycles
DWORD appCycles()
{
	return (DWORD)appSeconds();
}
#endif


//
// Get time in seconds. Origin is arbitrary.
//
#if !DEFINED_appSeconds
DOUBLE appSeconds()
{
	return appSecondsSlow();
}
#endif

//
// Return the system time.
//
void appSystemTime( INT& Year, INT& Month, INT& DayOfWeek, INT& Day, INT& Hour, INT& Min, INT& Sec, INT& MSec )
{
	time_t			CurTime;
	struct tm		*St;		// System time.
	struct timeval	Tv;			// Use timeval to get milliseconds.

	gettimeofday( &Tv, NULL );
	CurTime = time( NULL );
	St = localtime( &CurTime );

	Year		= St->tm_year + 1900;
	Month		= St->tm_mon + 1;
	DayOfWeek	= St->tm_wday;
	Day			= St->tm_mday;
	Hour		= St->tm_hour;
	Min			= St->tm_min;
	Sec			= St->tm_sec;
	MSec		= (INT) (Tv.tv_usec / 1000);
}

void appSleep( FLOAT Seconds )
{
	const INT usec = appRound(Seconds * 1000000.0f);
	if (usec > 0)
	{
		usleep(usec);
	}
	else
	{
		sched_yield();
	}
}

/*-----------------------------------------------------------------------------
	Link functions.
-----------------------------------------------------------------------------*/

static bool FindBinaryInPath(FString &cmd)
{
    INT binpos = cmd.InStr(TEXT(" "));
    FString bin((binpos == -1) ? cmd : cmd.Left(binpos));

    if (bin.Len() == 0)
        return(false);

    if (bin.InStr(TEXT("/")) != -1)  // path specified; don't examine $PATH.
        return(access(TCHAR_TO_ANSI(*bin), X_OK) != -1);

    const char *_envr = getenv("PATH");
    if (_envr == NULL)
    {
        debugf(TEXT("No $PATH environment var set"));
        return(false);
    }

    FString envr(_envr);

    while (true)
    {
        INT pos = envr.InStr(TEXT(":"));
        FString path((pos == -1) ? envr : envr.Left(pos));
        path += TEXT("/");

        FString binpath = path;
        binpath += bin;

        char realized[MAXPATHLEN];
        if (realpath(TCHAR_TO_ANSI(*binpath), realized) != NULL)
        {
            struct stat statbuf;
            if (stat(realized, &statbuf) != -1)
            {
                /* dirs are usually "executable", but aren't programs. */
                if (!S_ISDIR(statbuf.st_mode))
                {
                    if (access(realized, X_OK) != -1)
                    {
                        cmd = FString(realized) + cmd.Mid(binpos);
                        return(true);
                    }
                }
            }
        }

        if (pos == -1)
            break;
        envr = envr.Mid(pos + 1);
    }

    return(false);
}

//
// Launch a uniform resource locator (i.e. http://www.epicgames.com/unreal).
// This is expected to return immediately as the URL is launched by another
// task.
//
void appLaunchURL( const TCHAR* URL, const TCHAR* Parms, FString* Error )
{
#if 0
    const char *_envr = getenv("BROWSER");
    if (!_envr)
    {
        debugf(TEXT("Tried to view URL but BROWSER environment var isn't set!"));
        debugf(TEXT(" URL to view was [%s]."), URL);
        debugf(TEXT(" Trying some standard browsers..."), URL);
        //_envr = "opera:netscape -raise -remote \"openURL(%s,new-window)\":mozilla:konqueror:galeon:xterm -e links:xterm -e lynx:";
        _envr = "opera:mozilla:galeon:konqueror:netscape:xterm -e links:xterm -e lynx:";
    }

    FString envr(appFromAnsi(_envr));

    bool foundBrowser = false;

    while (true)
    {
        INT pos = envr.InStr(TEXT(":"));
        FString cmd((pos == -1) ? envr : envr.Left(pos));
        INT had_percent_s = false;
        INT percentpos = 0;
        while (percentpos < cmd.Len() - 1)
        {
            percentpos++;
            if (cmd[percentpos - 1] != '%')
                continue;

            // need to deal with a '%' sequence...

            if (cmd[percentpos] == '%')
            {
                cmd = cmd.Left(percentpos) + cmd.Mid(percentpos + 1);
            }
            else if (cmd[percentpos] == 's')
            {
                cmd = cmd.Left(percentpos - 1) + URL + cmd.Mid(percentpos + 1);
                percentpos += appStrlen(URL);
                had_percent_s = true;
            }
            else
            {
                debugf(TEXT("Unrecognized percent sequence (%%%c) in BROWSER string."), (TCHAR) cmd[percentpos]);
                percentpos++;
            }
        }

        if (!had_percent_s)
        {
            cmd += TEXT(" ");
            cmd += URL;
        }

        if (FindBinaryInPath(cmd))
        {
            foundBrowser = true;
            debugf(TEXT("Spawning browser: %s"), *cmd);
            GUnixSpawnOnExit = new TCHAR[cmd.Len() + 1];
            appStrcpy(GUnixSpawnOnExit, *cmd);
            break;
        }

        if (pos == -1)
            break;
        envr = envr.Mid(pos + 1);
    }

    if (!foundBrowser)
    {
        debugf(TEXT("Couldn't find any browser to launch!"));
        debugf(TEXT("Please set the environment variable BROWSER next time."));

        fprintf(stderr, "Couldn't find any browser to launch!\n");
        fprintf(stderr, "Please set the environment variable BROWSER next time.\n");
    }
#endif

    appRequestExit(0);
}

/*-----------------------------------------------------------------------------
	File finding.
-----------------------------------------------------------------------------*/

//
// Clean out the file cache.
//
static INT GetFileAgeDays( const TCHAR* Filename )
{
	struct stat Buf;
	INT Result = 0;

	Result = stat(TCHAR_TO_ANSI(Filename),&Buf);
	if( Result==0 )
	{
		time_t CurrentTime, FileTime;
		FileTime = Buf.st_mtime;
		time( &CurrentTime );
		DOUBLE DiffSeconds = difftime( CurrentTime, FileTime );
		return (INT)(DiffSeconds / 60.0 / 60.0 / 24.0);
	}
	return 0;
}

void appCleanFileCache()
{
	// Delete all temporary files.
	FString Temp = FString::Printf( TEXT("%s%s*.tmp"), *GSys->CachePath, PATH_SEPARATOR );
    TArray<FString> Found;
    GFileManager->FindFiles( Found, *Temp, 1, 0 );
	for( INT i=0; i<Found.Num(); i++ )
	{
		Temp = FString::Printf( TEXT("%s%s%s"), *GSys->CachePath, PATH_SEPARATOR, *Found(i) );
		debugf( TEXT("Deleting temporary file: %s"), *Temp );
		GFileManager->Delete( *Temp );
	}

	// Delete cache files that are no longer wanted.
    Found.Empty();
    GFileManager->FindFiles( Found, *(GSys->CachePath * TEXT("*") + GSys->CacheExt), 1, 0 );
	if( GSys->PurgeCacheDays )
	{
		for( INT i=0; i<Found.Num(); i++ )
		{
			FString Temp = FString::Printf( TEXT("%s%s%s"), *GSys->CachePath, PATH_SEPARATOR, *Found(i) );
			INT DiffDays = GetFileAgeDays( *Temp );
			if( DiffDays > GSys->PurgeCacheDays )
			{
				debugf( TEXT("Purging outdated file from cache: %s (%i days old)"), *Temp, DiffDays );
				GFileManager->Delete( *Temp );
			}
		}
	}
}

/*-----------------------------------------------------------------------------
	Guids.
-----------------------------------------------------------------------------*/

//
// Create a new globally unique identifier.
//
FGuid appCreateGuid()
{
	FGuid Result;
	appGetGUID( (void*)&Result );
	return Result;
}

/*-----------------------------------------------------------------------------
	Clipboard
-----------------------------------------------------------------------------*/
static FString ClipboardText;
void appClipboardCopy( const TCHAR* Str )
{
	ClipboardText = FString( Str );
}

FString appClipboardPaste()
{
	return ClipboardText;
}
/*-----------------------------------------------------------------------------
	Command line.
-----------------------------------------------------------------------------*/

// Get startup directory.
const TCHAR* appBaseDir()
{
	static TCHAR BaseDir[PATH_MAX]=TEXT("");
    ANSICHAR AnsiBaseDir[PATH_MAX];

	if( !BaseDir[0] )
	{
		// If the executable isn't launched from its own directory, then the
		// dynamic linker won't be able to find the shared libraries.
		getcwd( AnsiBaseDir, sizeof(AnsiBaseDir) );
        appStrcpy( BaseDir, ANSI_TO_TCHAR(AnsiBaseDir));
		appStrcat( BaseDir, TEXT("/") );
	}
	return BaseDir;
}

// Get computer name.
const TCHAR* appComputerName()
{
	static TCHAR Result[256]=TEXT("");
    static ANSICHAR AnsiResult[256];
	if( !Result[0] )
    {
		gethostname( AnsiResult, sizeof(AnsiResult) );
        appStrcpy( Result, ANSI_TO_TCHAR(AnsiResult));
    }
	return Result;
}

// Get user name.
const TCHAR* appUserName()
{
	static TCHAR Result[256]=TEXT("");
	if( !Result[0] )
    {
        ANSICHAR *str = getlogin();
		appStrncpy( Result, ANSI_TO_TCHAR(str), sizeof(Result) );
    }
	return Result;
}

/*-----------------------------------------------------------------------------
	App init/exit.
-----------------------------------------------------------------------------*/

//
// Platform specific initialization.
//

/**
 * Does per platform initialization of timing information and returns the current time. This function is
 * called before the execution of main as GStartTime is statically initialized by it. The function also
 * internally sets GSecondsPerCycle, which is safe to do as static initialization order enforces complex
 * initialization after the initial 0 initialization of the value.
 *
 * @return	current time
 */
DOUBLE appInitTiming(void)
{
#if defined(__MACH__)
	// Time base is in nano seconds.
	mach_timebase_info_data_t Info;
	verify( mach_timebase_info( &Info ) == 0 );
	GSecondsPerCycle = 1e-9 * (DOUBLE) Info.numer / (DOUBLE) Info.denom;
#else
	// we use gettimeofday() instead of rdtsc, so it's 1000000 "cycles" per second on this faked CPU.
	GSecondsPerCycle = 1.0f / 1000000.0f;
#endif
	return appSeconds();
}

void appPlatformPreInit()
{
    mainPID = getpid();
}

#define DESIRED_FILE_HANDLES 1024
void appPlatformInit()
{
	// System initialization.
	GSys = new USystem;
	GSys->AddToRoot();
	for( INT i=0; i<GSys->Suppress.Num(); i++ )
		GSys->Suppress(i).SetFlags( RF_Suppress );

	// Randomize.
	srand( (unsigned)time( NULL ) );

	// Exit code handling
	ExitCodeMap = new TMap<pid_t,int>;

    // Identity.
    debugf( NAME_Init, TEXT("Computer: %s"), appComputerName() );
    debugf( NAME_Init, TEXT("User: %s"), appUserName() );

    // Timer resolution.
	debugf(NAME_Init, TEXT("High frequency timer resolution =%f MHz"), 0.000001 / GSecondsPerCycle);

	struct sigaction SigAction;

	// Only try once.
	INT DefaultFlag			= SA_RESETHAND;

	SigAction.sa_handler	= HandleSignal;
	SigAction.sa_flags		= DefaultFlag;
	sigemptyset( &SigAction.sa_mask );
	sigaction( SIGSEGV, &SigAction, 0 );

	SigAction.sa_handler	= HandleSignal;
	SigAction.sa_flags		= DefaultFlag;
	sigemptyset( &SigAction.sa_mask );
	sigaction( SIGILL, &SigAction, 0 );

	// Try multiple times.
	DefaultFlag				= 0;

	SigAction.sa_handler	= HandleChild;
	SigAction.sa_flags		= 0;
	sigemptyset( &SigAction.sa_mask );
	sigaction(SIGCHLD, &SigAction, 0);

	SigAction.sa_handler	= HandleSignal;
	SigAction.sa_flags		= DefaultFlag;
	sigemptyset( &SigAction.sa_mask );
	sigaction( SIGHUP, &SigAction, 0 );

	SigAction.sa_handler	= HandleSignal;
	SigAction.sa_flags		= DefaultFlag;
	sigemptyset( &SigAction.sa_mask );
	sigaction( SIGQUIT, &SigAction, 0 );

	SigAction.sa_handler	= HandleSignal;
	SigAction.sa_flags		= DefaultFlag;
	sigemptyset( &SigAction.sa_mask );
	sigaction( SIGTRAP, &SigAction, 0 );

	SigAction.sa_handler	= HandleSignal;
	SigAction.sa_flags		= DefaultFlag;
	sigemptyset( &SigAction.sa_mask );
	sigaction( SIGIOT, &SigAction, 0 );

	SigAction.sa_handler	= HandleSignal;
	SigAction.sa_flags		= DefaultFlag;
	sigemptyset( &SigAction.sa_mask );
	sigaction( SIGBUS, &SigAction, 0 );

	SigAction.sa_handler	= HandleSignal;
	SigAction.sa_flags		= DefaultFlag;
	sigemptyset( &SigAction.sa_mask );
	sigaction( SIGFPE, &SigAction, 0 );

	SigAction.sa_handler	= HandleSignal;
	SigAction.sa_flags		= DefaultFlag;
	sigemptyset( &SigAction.sa_mask );
	sigaction( SIGTERM, &SigAction, 0 );

	SigAction.sa_handler	= HandleInterrupt;
	SigAction.sa_flags		= DefaultFlag;
	sigemptyset( &SigAction.sa_mask );
	sigaction( SIGINT, &SigAction, 0 );

	signal( SIGPIPE, SIG_IGN );

    // attempt to change max file handles for this process...
    struct rlimit rl;
    if (getrlimit(RLIMIT_NOFILE, &rl) != -1)
        debugf(NAME_Init, TEXT("File handle limit is soft=(%d), hard=(%d)."), (int) rl.rlim_cur, (int) rl.rlim_max);
    else
    {
        rl.rlim_cur = DESIRED_FILE_HANDLES; // wing it.
        rl.rlim_max = RLIM_INFINITY;
    }

    if ( ((rl.rlim_cur != RLIM_INFINITY) && (rl.rlim_cur < DESIRED_FILE_HANDLES)) ||
         ((rl.rlim_max != RLIM_INFINITY) && (rl.rlim_max < DESIRED_FILE_HANDLES)) )
    {
        bool changed = false;

        if ((rl.rlim_cur != RLIM_INFINITY) && (rl.rlim_cur < DESIRED_FILE_HANDLES))
            rl.rlim_cur = DESIRED_FILE_HANDLES;
        if ((rl.rlim_max != RLIM_INFINITY) && (rl.rlim_max < DESIRED_FILE_HANDLES))
            rl.rlim_max = RLIM_INFINITY;

        changed = (setrlimit(RLIMIT_NOFILE, &rl) != -1);
        if (!changed)
        {
            debugf(NAME_Init, TEXT("failed to set open file limit to %d, %d."), (int) rl.rlim_cur, (int) rl.rlim_max);
            rl.rlim_max = DESIRED_FILE_HANDLES;
            changed = (setrlimit(RLIMIT_NOFILE, &rl) != -1);
            if (!changed)
                debugf(NAME_Init, TEXT("failed to set open file limit to %d, %d."), (int) rl.rlim_cur, (int) rl.rlim_max);
        }

        if (changed)
        {
            getrlimit(RLIMIT_NOFILE, &rl);
            debugf(NAME_Init, TEXT("Changed file handle limit to soft=(%d), hard=(%d)."), (int) rl.rlim_cur, (int) rl.rlim_max);
        }
    }
}

void appPlatformPreExit()
{
}

void appPlatformExit()
{
#if 0
    if (GUnixSpawnOnExit != NULL)
    {
        FString fstrcmd(GUnixSpawnOnExit);
        TArray<FString> args;
        fstrcmd.ParseIntoArray(TEXT(" "), &args);
        char *cmd = new char[args(0).Len() + 1];
        appToAnsi(*(args(0)), cmd);
        char **ansiargs = new char *[args.Num() + 1];
        for (int i = 0; i < args.Num(); i++)
        {
            ansiargs[i] = new char[args(i).Len() + 1];
            appToAnsi(*args(i), ansiargs[i]);

            // This is a total hack, but then again, so is GUnixSpawnOnExit.
            char *str = ansiargs[i];
            if (*str == '\"')
                memmove(str, str + 1, strlen(str));
            if (str[strlen(str) - 1] == '\"')
                str[strlen(str) - 1] = '\0';
        }
        ansiargs[args.Num()] = NULL;
        execv(cmd, ansiargs);  // hail mary.

        // if you hit this...there were problems.
        fprintf(stderr, "execv() failed. Command line:\n   ");
        for (int x = 0; ansiargs[x]; x++)
            fprintf(stderr, "%s ", ansiargs[x]);
        fprintf(stderr, "\n\n");
    }
#endif
}

/*-----------------------------------------------------------------------------
	Pathnames.
-----------------------------------------------------------------------------*/

// See if file specified by (Path) exists with a different case. This
//  assumes that the directories specified exist with the given case, and
//  only looks at the file part of the path. The first file found via
//  readdir() is the one picked, which may not be the first in ASCII order.
//  If a file is found, (Path) is overwritten to reflect the new case, and
//  (true) is returned. If no file is found, (Path) is untouched, and (false)
//  is returned.
static bool find_alternate_filecase(char *Path)
{
    bool retval = false;
    char *ptr = strrchr(Path, '/');
    char *filename = (ptr != NULL) ? ptr + 1 : Path;
    char *basedir = (ptr != NULL) ? Path : ((char *) ".");

    if (ptr != NULL)
        *ptr = '\0';  // separate dir and filename.

    // fast rejection: only check this if there's no wildcard in filename.
    if (strchr(filename, '*') == NULL)
    {
        DIR *dir = opendir(basedir);
        if (dir != NULL)
        {
            struct dirent *ent;
            while (((ent = readdir(dir)) != NULL) && (!retval))
            {
                if (strcasecmp(ent->d_name, filename) == 0)  // a match?
                {
                    strcpy(filename, ent->d_name);  // overwrite with new case.
                    retval = true;
                }
            }
            closedir(dir);
        }
    }

    if (ptr != NULL)
        *ptr = '/';  // recombine dir and filename into one path.

    return(retval);
}


// Convert pathname to Unix format.
ANSICHAR* appUnixPath( const ANSICHAR* Path )
{
	static ANSICHAR UnixPath[1024];
	ANSICHAR* Cur = UnixPath;
	strncpy( UnixPath, Path, 1024 );
    UnixPath[1023] = '\0';

    // convert dir separators.
    while( (Cur = strchr( Cur, '\\' )) )
		*Cur = '/';

    // fast rejection: only check this if the current path doesn't exist...
    if (access(UnixPath, F_OK) != 0)
    {
        Cur = UnixPath;
        if (*Cur == '/')
            Cur++;

        bool keep_looking = true;
        while (keep_looking)
        {
            Cur = strchr( Cur, '/' );
            if (Cur != NULL)
                *Cur = '\0';  // null-terminate so we have this chunk of path.

            // do insensitive check only if the current path doesn't exist...
            if (access(UnixPath, F_OK) != 0)
                keep_looking = find_alternate_filecase(UnixPath);

            if (Cur == NULL)
            {
                keep_looking = false;
            }
            else
            {
                *Cur = '/';   // reset string for next check.
                Cur++;
            }
        }
    }

	return UnixPath;
}

/*-----------------------------------------------------------------------------
	Networking.
-----------------------------------------------------------------------------*/

unsigned long appGetLocalIP( void )
{
    static bool lookedUp = false;
	static unsigned long LocalIP = 0;

	if( !lookedUp )
	{
        lookedUp = true;
	    char Hostname[256];
        Hostname[0] = '\0';
		if (gethostname( Hostname, sizeof(Hostname) ) == 0)
        {
			//check(gethostbyname_mutex != NULL);
			//gethostbyname_mutex->Lock();
	        struct hostent *Hostinfo = gethostbyname( Hostname );
            if (Hostinfo != NULL)
    		    LocalIP = *(unsigned long*)Hostinfo->h_addr_list[0];
			//gethostbyname_mutex->Unlock();
        }
	}

	return LocalIP;
}

/*-----------------------------------------------------------------------------
	String functions.
-----------------------------------------------------------------------------*/

INT stricmp( const ANSICHAR* s, const ANSICHAR* t )
{
	INT	i;
	for( i = 0; tolower(s[i]) == tolower(t[i]); i++ )
		if( s[i] == '\0' )
			return 0;
	return s[i] - t[i];
}

INT strnicmp( const ANSICHAR* s, const ANSICHAR* t, INT n )
{
	INT	i;
	if( n <= 0 )
		return 0;
	for( i = 0; tolower(s[i]) == tolower(t[i]); i++ )
		if( (s[i] == '\0') || (i == n - 1) )
			return 0;
	return s[i] - t[i];
}

ANSICHAR* strupr( ANSICHAR* s )
{
	INT	i;
	for( i = 0; s[i] != '\0'; i++ )
		s[i] = toupper(s[i]);
	return s;
}

#endif

/*-----------------------------------------------------------------------------
	The End.
-----------------------------------------------------------------------------*/

