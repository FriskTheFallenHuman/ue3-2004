/*=============================================================================
	FFileManagerUnix.cpp: Unreal Unix based file manager.
	Copyright 1997-2005 Epic Games, Inc. All Rights Reserved.
=============================================================================*/

#include "CorePrivate.h"

#if PLATFORM_UNIX

#include "FFileManagerUnix.h"

#pragma pack (push,8)
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <limits.h>
#include <unistd.h>
#include <glob.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#pragma pack(pop)

#define MAX_PRECACHE 8192

/*-----------------------------------------------------------------------------
	FArchiveFileReaderUnix implementation
-----------------------------------------------------------------------------*/

FArchiveFileReaderUnix::FArchiveFileReaderUnix(INT InFile, BYTE* InMap, FOutputDevice* InError, INT InSize)
	: File(InFile)
	, Map(InMap)
	, Error(InError)
	, Size(InSize)
	, Pos(0)
{
	ArIsLoading = ArIsPersistent = 1;
}

FArchiveFileReaderUnix::~FArchiveFileReaderUnix()
{
	if (File != -1)
	{
		Close();
	}
}

void FArchiveFileReaderUnix::Precache(INT HintCount)
{
	BYTE* StartAddr = Map + Pos;
	INT Length = Min(Min(HintCount, Size - Pos), MAX_PRECACHE);
	if (mlock(StartAddr, Length) == -1)
	{
		ArIsError = 1;
		Error->Logf(TEXT("Failed to precache (%i bytes): %s"), Length, ANSI_TO_TCHAR(strerror(errno)));
		return;
	}
	munlock(StartAddr, Length);
}

void FArchiveFileReaderUnix::Seek(INT InPos)
{
	check(InPos >= 0);
	check(InPos <= Size);
	Pos = InPos;
}

INT FArchiveFileReaderUnix::Tell()
{
	return Pos;
}

INT FArchiveFileReaderUnix::TotalSize()
{
	return Size;
}

UBOOL FArchiveFileReaderUnix::Close()
{
	if (File != -1)
	{
		munmap(Map, Size);
		close(File);
	}
	File = -1;
	return !ArIsError;
}

void FArchiveFileReaderUnix::Serialize(void* V, INT Length)
{
	if (Length > Size - Pos)
	{
		ArIsError = 1;
		Error->Logf(TEXT("ReadFile beyond EOF %i+%i/%i"), Pos, Length, Size);
		return;
	}
	appMemcpy(V, Map + Pos, Length);
	Pos += Length;
}



/*-----------------------------------------------------------------------------
	FArchiveFileWriterUnix implementation
-----------------------------------------------------------------------------*/

FArchiveFileWriterUnix::FArchiveFileWriterUnix(FILE* InFile, FOutputDevice* InError)
	: File(InFile)
	, Error(InError)
	, Pos(0)
	, BufferCount(0)
{
	ArIsSaving = ArIsPersistent = 1;
}

FArchiveFileWriterUnix::~FArchiveFileWriterUnix()
{
	if (File)
	{
		Close();
	}
	File = NULL;
}

void FArchiveFileWriterUnix::Seek(INT InPos)
{
	Flush();
	if (fseek(File, InPos, SEEK_SET))
	{
		ArIsError = 1;
		Error->Logf(*LocalizeError("SeekFailed", TEXT("Core")));
	}
	Pos = InPos;
}

INT FArchiveFileWriterUnix::Tell()
{
	return Pos;
}

UBOOL FArchiveFileWriterUnix::Close()
{
	Flush();
	if (File && fclose(File))
	{
		ArIsError = 1;
		Error->Logf(*LocalizeError("WriteFailed", TEXT("Core")));
	}
	File = NULL;
	return !ArIsError;
}

void FArchiveFileWriterUnix::Serialize(void* V, INT Length)
{
	Pos += Length;
	INT Copy;
	while (Length > (Copy = ARRAY_COUNT(Buffer) - BufferCount))
	{
		appMemcpy(Buffer + BufferCount, V, Copy);
		BufferCount += Copy;
		Length -= Copy;
		V = (BYTE*)V + Copy;
		Flush();
	}
	if (Length)
	{
		appMemcpy(Buffer + BufferCount, V, Length);
		BufferCount += Length;
	}
}

void FArchiveFileWriterUnix::Flush()
{
	if (BufferCount && fwrite(Buffer, BufferCount, 1, File) != 1)
	{
		ArIsError = 1;
		Error->Logf(*LocalizeError("WriteFailed", TEXT("Core")));
	}
	BufferCount = 0;
}


/*-----------------------------------------------------------------------------
	FFileManagerUnix implementation
-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
	Public interface
-----------------------------------------------------------------------------*/

UBOOL FFileManagerUnix::SetDefaultDirectory()
{
	return chdir(TCHAR_TO_ANSI(appBaseDir())) == 0;
}

FString FFileManagerUnix::GetCurrentDirectory()
{
	return appBaseDir();
}
	
FArchive* FFileManagerUnix::CreateFileReader(const TCHAR* Filename, DWORD Flags, FOutputDevice* Error)
{
	TCHAR FixedFilename[PATH_MAX];
	PathSeparatorFixup(FixedFilename, Filename);

	INT File = -1;
	File = open(TCHAR_TO_ANSI(FixedFilename), O_RDONLY);
	if (File == -1)
	{
		if (Flags & FILEREAD_NoFail)
			appErrorf(TEXT("Failed to read file: %s"), FixedFilename);
		return NULL;
	}

	INT Size = lseek(File, 0, SEEK_END);
	lseek(File, 0, SEEK_SET);

	BYTE* Map = (BYTE*)mmap(NULL, Size, PROT_READ, MAP_PRIVATE, File, 0);
	if (Map == MAP_FAILED)
	{
		close(File);
		if (Flags & FILEREAD_NoFail)
			appErrorf(TEXT("Failed to read file: %s"), FixedFilename);
		return NULL;
	}

	return new FArchiveFileReaderUnix(File, Map, Error, Size);
}

FArchive* FFileManagerUnix::CreateFileWriter(const TCHAR* Filename, DWORD Flags, FOutputDevice* Error)
{
	TCHAR FixedFilename[PATH_MAX];
	PathSeparatorFixup(FixedFilename, Filename);

	if (Flags & FILEWRITE_EvenIfReadOnly)
	{
		chmod(TCHAR_TO_ANSI(FixedFilename), S_IRUSR | S_IWUSR);
	}
	if ((Flags & FILEWRITE_NoReplaceExisting) && FileSize(FixedFilename) >= 0)
	{
		return NULL;
	}

	const ANSICHAR* Mode = (Flags & FILEWRITE_Append) ? "ab" : "wb";
	FILE* File = fopen(TCHAR_TO_ANSI(FixedFilename), Mode);
	if (!File)
	{
		if (Flags & FILEWRITE_NoFail)
			appErrorf(TEXT("Failed to write: %s"), FixedFilename);
		return NULL;
	}
	if (Flags & FILEWRITE_Unbuffered)
	{
		setvbuf(File, 0, _IONBF, 0);
	}

	return new FArchiveFileWriterUnix(File, Error);
}

INT FFileManagerUnix::FileSize(const TCHAR* Filename)
{
	TCHAR PlatformFilename[PATH_MAX];
	PathSeparatorFixup(PlatformFilename, Filename);

	struct stat StatBuf;
	if (::stat(TCHAR_TO_ANSI(PlatformFilename), &StatBuf) == -1)
	{
		return -1;
	}
	if (S_ISREG(StatBuf.st_mode) == 0)  // not a regular file?
	{
		return -1;
	}
	if (StatBuf.st_size > 0x7FFFFFFF)  // bigger than largest positive value that fits in an INT?
	{
		return -1;
	}

	return (INT)StatBuf.st_size;
}

DWORD FFileManagerUnix::Copy(const TCHAR* DestFile, const TCHAR* SrcFile, UBOOL ReplaceExisting, UBOOL EvenIfReadOnly, UBOOL Attributes, DWORD Compress, FCopyProgress* Progress)
{
	// move from read
	TCHAR SrcPlatformFilename[PATH_MAX];;
	PathSeparatorFixup(SrcPlatformFilename, SrcFile);

	// to write dir
	TCHAR DestPlatformFilename[PATH_MAX];;
	PathSeparatorFixup(DestPlatformFilename, DestFile);

	if (EvenIfReadOnly)
	{
		chmod(TCHAR_TO_ANSI(SrcPlatformFilename), S_IRUSR | S_IWUSR);
	}

	DWORD Result;
	if (Progress || Compress != FILECOPY_Normal)
	{
		Result = FFileManagerGeneric::Copy(DestPlatformFilename, SrcPlatformFilename, ReplaceExisting, EvenIfReadOnly, Attributes, Compress, Progress);
	}

	if (Result == COPY_OK && !Attributes)
	{
		chmod(TCHAR_TO_ANSI(DestPlatformFilename), 0);
	}

	return Result;
}

UBOOL FFileManagerUnix::Delete(const TCHAR* Filename, UBOOL RequireExists, UBOOL EvenReadOnly)
{
	TCHAR FixedFilename[PATH_MAX];
	PathSeparatorFixup(FixedFilename, Filename);

	if (EvenReadOnly)
	{
		chmod(TCHAR_TO_ANSI(FixedFilename), S_IRUSR | S_IWUSR);
	}
	return unlink(TCHAR_TO_ANSI(FixedFilename)) == 0 || (errno == ENOENT && !RequireExists);
}

UBOOL FFileManagerUnix::IsReadOnly(const TCHAR* Filename)
{
	// we can only delete from write directory
	TCHAR PlatformFilename[PATH_MAX];
	PathSeparatorFixup(PlatformFilename, Filename);

	if (::access(TCHAR_TO_ANSI(PlatformFilename), F_OK) == -1)
	{
		return FALSE;  // The Windows codepath returns 0 here too...
	}

	if (::access(TCHAR_TO_ANSI(PlatformFilename), W_OK) == -1)
	{
		if (errno != EACCES)
		{
			debugf(NAME_Error, TEXT("Error reading attributes for '%s'"), PlatformFilename);
			return FALSE;  // The Windows codepath returns 0 here too...
		}
		return TRUE; // it's read only.
	}
	return FALSE; // it exists and we can write to it.
}

UBOOL FFileManagerUnix::Move(const TCHAR* Dest, const TCHAR* Src, UBOOL Replace, UBOOL EvenIfReadOnly, UBOOL Attributes)
{
	// move from read
	TCHAR SrcPlatformFilename[PATH_MAX];;
	PathSeparatorFixup(SrcPlatformFilename, Src);

	// to write dir
	TCHAR DestPlatformFilename[PATH_MAX];;
	PathSeparatorFixup(DestPlatformFilename, Dest);

	int Result = ::rename(TCHAR_TO_ANSI(SrcPlatformFilename), TCHAR_TO_ANSI(DestPlatformFilename));
	if (Result == -1)
	{
		// !!! FIXME: do a copy and delete the original if errno==EXDEV
		debugf(NAME_Error, TEXT("Error moving file '%s' to '%s' (errno: %d)"), Src, Dest, errno);
	}
	return Result != 0;
}

UBOOL FFileManagerUnix::MakeDirectory(const TCHAR* Path, UBOOL Tree)
{
	TCHAR FixedPath[PATH_MAX];
	PathSeparatorFixup(FixedPath, Path);

	if (Tree)
	{
		return FFileManagerGeneric::MakeDirectory(FixedPath, Tree);
	}

	return mkdir(TCHAR_TO_ANSI(FixedPath), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) == 0 || errno == EEXIST;
}

UBOOL FFileManagerUnix::DeleteDirectory(const TCHAR* Path, UBOOL RequireExists, UBOOL Tree)
{
	TCHAR FixedPath[PATH_MAX];
	PathSeparatorFixup(FixedPath, Path);

	if (Tree)
	{
		return FFileManagerGeneric::DeleteDirectory(FixedPath, RequireExists, Tree);
	}

	return rmdir(TCHAR_TO_ANSI(FixedPath)) == 0 || (errno == ENOENT && !RequireExists);
}

void FFileManagerUnix::FindFiles(TArray<FString>& Result, const TCHAR* Filename, UBOOL Files, UBOOL Directories)
{
	TCHAR FixedPattern[PATH_MAX];
	glob_t GlobBuf;

	PathSeparatorFixup(FixedPattern, Filename);

	INT GlobFlags = GLOB_NOSORT | GLOB_MARK;
	glob(TCHAR_TO_ANSI(FixedPattern), GlobFlags, NULL, &GlobBuf);

	for (size_t i = 0; i < GlobBuf.gl_pathc; i++)
	{
		const TCHAR* Match = ANSI_TO_TCHAR(GlobBuf.gl_pathv[i]);
		// Strip the filename from the match
		for (const TCHAR* Ptr = Match; *Ptr != TEXT('\0'); Ptr++)
			if (*Ptr == TEXT('/'))
				Match = Ptr + 1;
		// Directories are GLOB_MARK'd with a trailing slash,
		// so this prevents directories from being added to the result.
		if (appStrlen(Match) != 0)
			new(Result)FString(Match);
	}

	globfree(&GlobBuf);
}

DOUBLE FFileManagerUnix::GetFileAgeSeconds(const TCHAR* Filename)
{
	struct stat FileInfo;
	if (0 == stat(TCHAR_TO_ANSI(Filename), &FileInfo))
	{
		time_t	CurrentTime, FileTime;
		FileTime = FileInfo.st_mtime;
		time(&CurrentTime);
		return difftime(CurrentTime, FileTime);
	}
	return -1.0;
}

#endif

/*-----------------------------------------------------------------------------
	The End.
-----------------------------------------------------------------------------*/

