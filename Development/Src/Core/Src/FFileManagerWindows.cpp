/*=============================================================================
	FFileManagerWindows.cpp: Unreal Windows OS based file manager.
	Copyright 1997-2005 Epic Games, Inc. All Rights Reserved.
=============================================================================*/

#include "CorePrivate.h"

#if _WIN32

#include "FFileManagerWindows.h"

#pragma pack (push,8)
#include <sys/stat.h>
#include <sys/utime.h>
#include <time.h>
#include <shlobj.h>
#pragma pack(pop)

/*-----------------------------------------------------------------------------
	FArchiveFileReaderWindows implementation
-----------------------------------------------------------------------------*/

FArchiveFileReaderWindows::FArchiveFileReaderWindows(HANDLE InHandle, FOutputDevice* InError, INT InSize)
	: Handle(InHandle)
	, Error(InError)
	, Size(InSize)
	, Pos(0)
	, BufferBase(0)
	, BufferCount(0)
{
	ArIsLoading = ArIsPersistent = 1;
}

FArchiveFileReaderWindows::~FArchiveFileReaderWindows()
{
	if (Handle)
	{
		Close();
	}
}

void FArchiveFileReaderWindows::Precache(INT HintCount)
{
	checkSlow(Pos == BufferBase + BufferCount);
	BufferBase = Pos;
	BufferCount = Min(Min(HintCount, (INT)(ARRAY_COUNT(Buffer) - (Pos&(ARRAY_COUNT(Buffer) - 1)))), Size - Pos);
	INT Count = 0;
	ReadFile(Handle, Buffer, BufferCount, (DWORD*)&Count, NULL);
	if (Count != BufferCount)
	{
		ArIsError = 1;
		Error->Logf(TEXT("ReadFile failed: Count=%i BufferCount=%i Error=%s"), Count, BufferCount, appGetSystemErrorMessage());
	}
}

void FArchiveFileReaderWindows::Seek(INT InPos)
{
	check(InPos >= 0);
	check(InPos <= Size);
	if( SetFilePointer( Handle, InPos, 0, FILE_BEGIN )==INVALID_SET_FILE_POINTER )
	{
		ArIsError = 1;
		Error->Logf(TEXT("SetFilePointer Failed %i/%i: %i %s"), InPos, Size, Pos, appGetSystemErrorMessage());
	}
	Pos = InPos;
	BufferBase = Pos;
	BufferCount = 0;
}

INT FArchiveFileReaderWindows::Tell()
{
	return Pos;
}

INT FArchiveFileReaderWindows::TotalSize()
{
	return Size;
}

UBOOL FArchiveFileReaderWindows::Close()
{
	if (Handle)
	{
		CloseHandle(Handle);
	}
	Handle = NULL;
	return !ArIsError;
}

void FArchiveFileReaderWindows::Serialize(void* V, INT Length)
{
	while (Length>0)
	{
		INT Copy = Min(Length, BufferBase + BufferCount - Pos);
		if (Copy == 0)
		{
			if (Length >= ARRAY_COUNT(Buffer))
			{
				INT Count = 0;
				ReadFile(Handle, V, Length, (DWORD*)&Count, NULL);
				if (Count != Length)
				{
					ArIsError = 1;
					Error->Logf(TEXT("ReadFile failed: Count=%i Length=%i Error=%s"), Count, Length, appGetSystemErrorMessage());
				}
				Pos += Length;
				BufferBase += Length;
				return;
			}
			Precache(MAXINT);
			Copy = Min(Length, BufferBase + BufferCount - Pos);
			if (Copy <= 0)
			{
				ArIsError = 1;
				Error->Logf(TEXT("ReadFile beyond EOF %i+%i/%i"), Pos, Length, Size);
			}
			if (ArIsError)
			{
				return;
			}
		}
		appMemcpy(V, Buffer + Pos - BufferBase, Copy);
		Pos += Copy;
		Length -= Copy;
		V = (BYTE*)V + Copy;
	}
}



/*-----------------------------------------------------------------------------
	FArchiveFileWriterWindows implementation
-----------------------------------------------------------------------------*/

FArchiveFileWriterWindows::FArchiveFileWriterWindows( HANDLE InHandle, FOutputDevice* InError, INT InPos )
:   Handle      ( InHandle )
,   Error       ( InError )
,   Pos         ( InPos )
,   BufferCount ( 0 )
{
	ArIsSaving = ArIsPersistent = 1;
}

FArchiveFileWriterWindows::~FArchiveFileWriterWindows()
{
	if( Handle )
	{
		Close();
	}
	Handle = NULL;
}

void FArchiveFileWriterWindows::Seek( INT InPos )
{
	Flush();
	if( SetFilePointer( Handle, InPos, 0, FILE_BEGIN )==0xFFFFFFFF )
	{
		ArIsError = 1;
		Error->Logf( *LocalizeError("SeekFailed",TEXT("Core")) );
	}
	Pos = InPos;
}

INT FArchiveFileWriterWindows::Tell()
{
	return Pos;
}

UBOOL FArchiveFileWriterWindows::Close()
{
	Flush();
	if( Handle && !CloseHandle(Handle) )
	{
		ArIsError = 1;
		Error->Logf( *LocalizeError("WriteFailed",TEXT("Core")) );
	}
	Handle = NULL;
	return !ArIsError;
}

void FArchiveFileWriterWindows::Serialize( void* V, INT Length )
{
	Pos += Length;
	INT Copy;
	while( Length > (Copy=ARRAY_COUNT(Buffer)-BufferCount) )
	{
		appMemcpy( Buffer+BufferCount, V, Copy );
		BufferCount += Copy;
		Length      -= Copy;
		V            = (BYTE*)V + Copy;
		Flush();
	}
	if( Length )
	{
		appMemcpy( Buffer+BufferCount, V, Length );
		BufferCount += Length;
	}
}

void FArchiveFileWriterWindows::Flush()
{
	if( BufferCount )
	{
		INT Result=0;
		if( !WriteFile( Handle, Buffer, BufferCount, (DWORD*)&Result, NULL ) )
		{
			ArIsError = 1;
			Error->Logf( *LocalizeError("WriteFailed",TEXT("Core")) );
		}
	}
	BufferCount = 0;
}


/*-----------------------------------------------------------------------------
	FFileManagerWindows implementation
-----------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
	Public interface
-----------------------------------------------------------------------------*/

UBOOL FFileManagerWindows::SetDefaultDirectory()
{
	return SetCurrentDirectoryW(appBaseDir())!=0;
}

FString FFileManagerWindows::GetCurrentDirectory()
{
	return appBaseDir();
}
	
FArchive* FFileManagerWindows::CreateFileReader( const TCHAR* Filename, DWORD Flags, FOutputDevice* Error )
{
	DWORD  Access    = GENERIC_READ;
	DWORD  WinFlags  = FILE_SHARE_READ;
	DWORD  Create    = OPEN_EXISTING;
	HANDLE Handle    = CreateFileW( Filename, Access, WinFlags, NULL, Create, FILE_ATTRIBUTE_NORMAL, NULL );
	if( Handle==INVALID_HANDLE_VALUE )
	{
		if( Flags & FILEREAD_NoFail )
		{
			appErrorf( TEXT("Failed to read file: %s"), Filename );
		}
		return NULL;
	}
	return new FArchiveFileReaderWindows(Handle,Error,GetFileSize(Handle,NULL));
}

FArchive* FFileManagerWindows::CreateFileWriter( const TCHAR* Filename, DWORD Flags, FOutputDevice* Error )
{
	MakeDirectory(*FFilename(Filename).GetPath(), TRUE);

	if( (GFileManager->FileSize (Filename) >= 0) && (Flags & FILEWRITE_EvenIfReadOnly) )
	{
		SetFileAttributesW(Filename, 0);
	}
	DWORD  Access    = GENERIC_WRITE;
	DWORD  WinFlags  = (Flags & FILEWRITE_AllowRead) ? FILE_SHARE_READ : 0;
	DWORD  Create    = (Flags & FILEWRITE_Append) ? OPEN_ALWAYS : (Flags & FILEWRITE_NoReplaceExisting) ? CREATE_NEW : CREATE_ALWAYS;
	HANDLE Handle    = CreateFileW( Filename, Access, WinFlags, NULL, Create, FILE_ATTRIBUTE_NORMAL, NULL );
	INT    Pos       = 0;
	if( Handle==INVALID_HANDLE_VALUE )
	{
		if( Flags & FILEWRITE_NoFail )
		{
			appErrorf( TEXT("Failed to create file: %s"), Filename );
		}
		return NULL;
	}
	if( Flags & FILEWRITE_Append )
	{
		Pos = SetFilePointer( Handle, 0, 0, FILE_END );
	}
	return new FArchiveFileWriterWindows(Handle,Error,Pos);
}

INT FFileManagerWindows::FileSize( const TCHAR* Filename )
{
	HANDLE Handle = CreateFileW( Filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );
	if( Handle==INVALID_HANDLE_VALUE )
	{
		return -1;
	}
	DWORD Result = GetFileSize( Handle, NULL );
	CloseHandle( Handle );
	return Result;
}

DWORD FFileManagerWindows::Copy( const TCHAR* DestFile, const TCHAR* SrcFile, UBOOL ReplaceExisting, UBOOL EvenIfReadOnly, UBOOL Attributes, DWORD Compress, FCopyProgress* Progress )
{
	if( EvenIfReadOnly )
	{
		SetFileAttributesW(DestFile, 0);
	}
	DWORD Result;
	if( Progress || Compress != FILECOPY_Normal )
	{
		Result = FFileManagerGeneric::Copy( DestFile, SrcFile, ReplaceExisting, EvenIfReadOnly, Attributes, Compress, Progress );
	}
	else
	{
		if( CopyFileW(SrcFile, DestFile, !ReplaceExisting) != 0 )
		{
			Result = COPY_OK;
		}
		else
		{
			Result = COPY_MiscFail;
		}
	}
	if( Result==COPY_OK && !Attributes )
	{
		SetFileAttributesW(DestFile, 0);
	}
	return Result;
}

UBOOL FFileManagerWindows::Delete( const TCHAR* Filename, UBOOL RequireExists, UBOOL EvenReadOnly )
{
	if( EvenReadOnly )
	{
		SetFileAttributesW(Filename,FILE_ATTRIBUTE_NORMAL);
	}
	INT Result = (DeleteFile(Filename))!=0 || (!RequireExists && GetLastError()==ERROR_FILE_NOT_FOUND);
	if( !Result )
	{
		DWORD error = GetLastError();
		debugf( NAME_Error, TEXT("Error deleting file '%s' (GetLastError: %d)"), Filename, error );
	}
	return Result!=0;
}

UBOOL FFileManagerWindows::IsReadOnly( const TCHAR* Filename )
{
	DWORD rc;
	if( FileSize( Filename ) < 0 )
	{
		return( 0 );
	}
	rc = GetFileAttributesW(Filename);
	if (rc != 0xFFFFFFFF)
	{
		return ((rc & FILE_ATTRIBUTE_READONLY) != 0);
	}
	else
	{
		debugf( NAME_Error, TEXT("Error reading attributes for '%s'"), Filename );
		return (0);
	}
}

UBOOL FFileManagerWindows::Move( const TCHAR* Dest, const TCHAR* Src, UBOOL Replace, UBOOL EvenIfReadOnly, UBOOL Attributes )
{
	//warning: MoveFileEx is broken on Windows 95 (Microsoft bug).
	Delete( Dest, 0, EvenIfReadOnly );
	INT Result = MoveFileW(Src,Dest);
	if( !Result )
	{
		DWORD error = GetLastError();
		debugf( NAME_Error, TEXT("Error moving file '%s' to '%s' (GetLastError: %d)"), Src, Dest, error );
	}
	return Result!=0;
}

UBOOL FFileManagerWindows::MakeDirectory( const TCHAR* Path, UBOOL Tree )
{
	if( Tree )
	{
		return FFileManagerGeneric::MakeDirectory( Path, Tree );
	}
	return ( CreateDirectoryW(Path,NULL) )!=0 || GetLastError()==ERROR_ALREADY_EXISTS;
}

UBOOL FFileManagerWindows::DeleteDirectory( const TCHAR* Path, UBOOL RequireExists, UBOOL Tree )
{
	if( Tree )
		return FFileManagerGeneric::DeleteDirectory( Path, RequireExists, Tree );
	return ( RemoveDirectoryW(Path) )!=0 || (!RequireExists && GetLastError()==ERROR_FILE_NOT_FOUND);
}

void FFileManagerWindows::FindFiles( TArray<FString>& Result, const TCHAR* Filename, UBOOL Files, UBOOL Directories )
{
	HANDLE Handle=NULL;
	WIN32_FIND_DATAW Data;
	Handle=FindFirstFileW(Filename,&Data);
	if( Handle!=INVALID_HANDLE_VALUE )
	{
		do
		{
			if
			(   appStricmp(Data.cFileName,TEXT("."))
			&&  appStricmp(Data.cFileName,TEXT(".."))
			&&  ((Data.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY)?Directories:Files) )
			{
				new(Result)FString(Data.cFileName);
			}
		}
		while( FindNextFileW(Handle,&Data) );
	}
	if( Handle!=INVALID_HANDLE_VALUE )
	{
		FindClose( Handle );
	}
}

DOUBLE FFileManagerWindows::GetFileAgeSeconds( const TCHAR* Filename )
{
	struct _stat FileInfo;
	if( 0 == _wstat( Filename, &FileInfo ) )
	{
		time_t CurrentTime, FileTime;	
		FileTime = FileInfo.st_mtime;
		time( &CurrentTime );
		return difftime( CurrentTime, FileTime );
	}
	return -1.0;
}

#endif

/*-----------------------------------------------------------------------------
	The End.
-----------------------------------------------------------------------------*/

