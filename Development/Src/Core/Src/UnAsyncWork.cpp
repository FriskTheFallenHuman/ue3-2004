/*=============================================================================
	UnAsyncWork.cpp: Implementations of queued work classes
	Copyright 1997-2005 Epic Games, Inc. All Rights Reserved.
=============================================================================*/

#include "CorePrivate.h"

/*-----------------------------------------------------------------------------
	FAsyncWorkBase implementation.
-----------------------------------------------------------------------------*/

/**
 * Caches the thread safe counter and uses self deletion to free itself.
 *
 * @param InWorkCompletionCounter	Counter to decrement on completion of task
 */
FAsyncWorkBase::FAsyncWorkBase( FThreadSafeCounter* InWorkCompletionCounter ) 
{
	// Cache the counter used to signal completion by decrementing.
	WorkCompletionCounter = InWorkCompletionCounter;
#if !XBOX
	// We're using a thread safe counter to determine whether work is completed.
	if( InWorkCompletionCounter )
	{
		DoneEvent = NULL;
	}
	// Create a manual reset event only if we're not using the counter.
	else
	{
		DoneEvent = GSynchronizeFactory->CreateSynchEvent(TRUE);
	}
#else
	bIsDone = FALSE;
#endif
}

/**
 * Cleans up the event if on non-Xbox platforms
 */ 
FAsyncWorkBase::~FAsyncWorkBase(void)
{
#if !XBOX
	if( DoneEvent )
	{
		GSynchronizeFactory->Destroy(DoneEvent);
	}
#endif
}

/**
 * Called after work has finished. Marks object as being done.
 */ 
void FAsyncWorkBase::Dispose()
{
	if( WorkCompletionCounter )
	{
		// Signal work as being completed by decrementing counter.
		WorkCompletionCounter->Decrement();
		// The queued work code won't access "this" after calling dispose so it's safe to delete here.
		delete this;
	}
	else
	{
#if XBOX
		appInterlockedExchange(&bIsDone,TRUE);
#else
		// Now use the event to indicate it's done
		DoneEvent->Trigger();
#endif
	}
}

/**
 * Returns whether the work is done.
 *
 * @return TRUE if work has been completed, FALSE otherwise
 */
UBOOL FAsyncWorkBase::IsDone(void)
{
	// We cannot call this function for auto- destroy objects that use a thread safe counter
	// to signal completion.
	check( WorkCompletionCounter == NULL );
#if XBOX
	// Use spin lock on consoles.
	return bIsDone;
#else
	// Use signal to avoid deadlock scenarios on the PC where it is not clear whether each
	// worker thread has its own CPU core or hyper thread.
	check(DoneEvent);
	// Returns true if the event was signaled
	return DoneEvent->Wait(0);
#endif
}

/*-----------------------------------------------------------------------------
	FAsyncCopy.
-----------------------------------------------------------------------------*/

/**
 * Initializes the data.
 */
FAsyncCopy::FAsyncCopy(void* InDest,void* InSrc,DWORD InSize,FThreadSafeCounter* Counter) 
: FAsyncWorkBase( Counter )
, Src(InSrc)
, Dest(InDest)
, Size(InSize)
{}

/**
 * Performs the async copy
 */
void FAsyncCopy::DoWork(void)
{
	// Do the copy
	appMemcpy(Dest,Src,Size);
}

/*-----------------------------------------------------------------------------
	FAsyncCopyAligned.
-----------------------------------------------------------------------------*/

/**
 * Performs the async copy
 */
void FAsyncCopyAligned::DoWork(void)
{
	// Do the copy
#if XBOX
	XMemCpy128(Dest,Src,Size);
#else
	appMemcpy(Dest,Src,Size);
#endif
}
