/*=============================================================================
	UnFile.cpp: ANSI C core.
	Copyright 1997-2005 Epic Games, Inc. All Rights Reserved.
=============================================================================*/

#include "CorePrivate.h"

#include <time.h>
#include <stdio.h>
#include <stdarg.h>

/*-----------------------------------------------------------------------------
	Time.
-----------------------------------------------------------------------------*/

//
// String timestamp.
//
const TCHAR* appTimestamp()
{
	static TCHAR Result[1024];
	*Result = 0;
#if _MSC_VER
	ANSICHAR Temp[1024]="";
	_strdate( Temp );
	appStrcpy( Result, ANSI_TO_TCHAR(Temp) );
	appStrcat( Result, TEXT(" ") );
	_strtime( Temp );
	appStrcat( Result, ANSI_TO_TCHAR(Temp) );
#else
    time_t        CurTime;
    struct tm*    SysTime;

    CurTime = time( NULL );
    SysTime = localtime( &CurTime );
    appSprintf( Result, ANSI_TO_TCHAR(asctime( SysTime )) );
    // Strip newline.
    Result[appStrlen( Result )-1] = TEXT('\0');
#endif
	return Result;
}

/*-----------------------------------------------------------------------------
	Memory functions.
-----------------------------------------------------------------------------*/

INT appMemcmp( const void* Buf1, const void* Buf2, INT Count )
{
	return memcmp( Buf1, Buf2, Count );
}

UBOOL appMemIsZero( const void* V, int Count )
{
	BYTE* B = (BYTE*)V;
	while( Count-- > 0 )
		if( *B++ != 0 )
			return 0;
	return 1;
}

void* appMemmove( void* Dest, const void* Src, INT Count )
{
	return memmove( Dest, Src, Count );
}

void appMemset( void* Dest, INT C, INT Count )
{
	memset( Dest, C, Count );
}

#ifndef DEFINED_appMemzero
void appMemzero( void* Dest, INT Count )
{
	memset( Dest, 0, Count );
}
#endif

#ifndef DEFINED_appMemcpy
void appMemcpy( void* Dest, const void* Src, INT Count )
{
	memcpy( Dest, Src, Count );
}
#endif

/*-----------------------------------------------------------------------------
	String functions.
-----------------------------------------------------------------------------*/

//
// Copy a string with length checking.
//warning: Behavior differs from strncpy; last character is zeroed.
//
TCHAR* appStrncpy( TCHAR* Dest, const TCHAR* Src, INT MaxLen )
{
	wcsncpy( Dest, Src, MaxLen );
	check(MaxLen>0);
	Dest[MaxLen-1]=0;
	return Dest;
}

//
// Concatenate a string with length checking
//
TCHAR* appStrncat( TCHAR* Dest, const TCHAR* Src, INT MaxLen )
{
	INT Len = appStrlen(Dest);
	TCHAR* NewDest = Dest + Len;
	if( (MaxLen-=Len) > 0 )
	{
		appStrncpy( NewDest, Src, MaxLen );
		NewDest[MaxLen-1] = 0;
	}
	return Dest;
}

//
// Standard string functions.
//
VARARG_BODY( INT, appSprintf, const TCHAR*, VARARG_EXTRA(TCHAR* Dest) )
{
	INT	Result = -1;
	//@warning: make sure code using appSprintf allocates enough memory if the below 1024 is ever changed.
	GET_VARARGS_RESULT(Dest,1024/*!!*/,Fmt,Fmt,Result);
	return Result;
}

VARARG_BODY( INT, appSnprintf, const TCHAR*, VARARG_EXTRA(INT MaxLength) VARARG_EXTRA(TCHAR* Dest) )
{
	INT Result = -1;
	GET_VARARGS_RESULT(Dest, MaxLength, Fmt, Fmt, Result);
	return Result;
}

TCHAR* appStrcpy( TCHAR* Dest, const TCHAR* Src )
{
	return wcscpy( Dest, Src );
}

INT appStrlen( const TCHAR* String )
{
	return (INT) wcslen( String );
}

TCHAR* appStrstr( const TCHAR* String, const TCHAR* Find )
{
	return (TCHAR *) wcsstr( String, Find );
}

TCHAR* appStrchr( const TCHAR* String, int c )
{
	return (TCHAR *) wcschr( String, c );
}

TCHAR* appStrcat( TCHAR* Dest, const TCHAR* Src )
{
	return wcscat( Dest, Src );
}

INT appStrcmp( const TCHAR* String1, const TCHAR* String2 )
{
	return wcscmp( String1, String2 );
}

INT appStricmp( const TCHAR* String1, const TCHAR* String2 )
{
	return _wcsicmp( String1, String2 );
}

INT appStrncmp( const TCHAR* A, const TCHAR* B, INT Count )
{
	return wcsncmp( A, B, Count );
}

TCHAR* appStrupr( TCHAR* String )
{
	return _wcsupr( String );
}

INT appAtoi( const TCHAR* Str )
{
	return _wtoi( Str );
}

TCHAR* appItoa( const INT Num )
{
	static TCHAR Buffer[20];
	appMemzero( Buffer, 20*sizeof(TCHAR) );
	return _itow( Num, Buffer, 10 );
}

FLOAT appAtof( const TCHAR* Str )
{
	return _wtof( Str );
}

INT appStrtoi( const TCHAR* Start, TCHAR** End, INT Base )
{
	return wcstoul( Start, End, Base );
}

INT appStrnicmp( const TCHAR* A, const TCHAR* B, INT Count )
{
	return _wcsnicmp( A, B, Count );
}

/*-----------------------------------------------------------------------------
	Sorting.
-----------------------------------------------------------------------------*/

void appQsort( void* Base, INT Num, INT Width, int(CDECL *Compare)(const void* A, const void* B ) )
{
	qsort( Base, Num, Width, Compare );
}

/*-----------------------------------------------------------------------------
	The End.
-----------------------------------------------------------------------------*/

