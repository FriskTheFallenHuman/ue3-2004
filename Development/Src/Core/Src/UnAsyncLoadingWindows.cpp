/**
 * UnAsyncLoadingWindows.cpp -- Contains all Windows platform specific definitions
 * of interfaces and concrete classes for async io loading support in the Unreal
 * engine.
 *
 * Copyright 2004-2005 Epic Games, Inc. All Rights Reserved.
 */

#include "CorePrivate.h"

#if _MSC_VER

/**
 * Initializes critical section, event and other used variables. 
 *
 * This is called in the context of the thread object that aggregates 
 * this, not the thread that passes this runnable to a new thread.
 *
 * @return True if initialization was successful, false otherwise
 */
UBOOL FAsyncIOManagerWindows::Init()
{
	CriticalSection				= GSynchronizeFactory->CreateCriticalSection();
	OutstandingRequestsEvent	= GSynchronizeFactory->CreateSynchEvent();
	RequestIndex				= 1;
	IsRunning.Increment();
	return TRUE;
}

/**
 * Called in the context of the aggregating thread to perform cleanup.
 */
void FAsyncIOManagerWindows::Exit()
{
	for( TMap<FString,HANDLE>::TIterator It(NameToHandleMap); It; ++It )
	{
		CloseHandle( It.Value() );
	}
	NameToHandleMap.Empty();
	GSynchronizeFactory->Destroy( CriticalSection );
	GSynchronizeFactory->Destroy( OutstandingRequestsEvent );
}

/**
 * This is called if a thread is requested to terminate early
 */
void FAsyncIOManagerWindows::Stop()
{
	OutstandingRequestsEvent->Trigger();
	IsRunning.Decrement();
}

/**
 * This is where all the actual loading is done. This is only called
 * if the initialization was successful.
 *
 * @return always 0
 */
DWORD FAsyncIOManagerWindows::Run()
{
	// IsRunning gets decremented by Stop.
	while( IsRunning.GetValue() > 0 )
	{
		FAsyncIORequest IORequest = {0};
		{
			FScopeLock ScopeLock( CriticalSection );
			if( OutstandingRequests.Num() )
			{
				IORequest = OutstandingRequests(0);
				OutstandingRequests.Remove( 0 );
				BusyReading.Increment();	// We're busy reading. Updated inside scoped lock to ensure CancelAllRequests works correctly.
			}
			else
			{
				IORequest.FileHandle = NULL;
			}
		}

		if( IORequest.FileHandle )
		{
			//@warning: this code doesn't handle failure as it doesn't have a way to pass back the information.
			DWORD BytesRead;
			SetFilePointer( IORequest.FileHandle, IORequest.Offset, 0, FILE_BEGIN );
			ReadFile( IORequest.FileHandle, IORequest.Dest, IORequest.Size, &BytesRead, NULL );
			IORequest.Counter->Decrement(); // Request fulfilled.
			BusyReading.Decrement();		// We're done reading for now.
		}
		else
		{
			// Wait till the calling thread signals further work.
			OutstandingRequestsEvent->Wait();
		}
	}

	return 0;
}

/**
 * Requests data to be loaded async. Returns immediately.
 *
 * @param	Filename	Filename to load
 * @param	Offset		Offset into file
 * @param	Size		Size of load request
 * @param	Dest		Pointer to load data into
 * @param	Counter		Thread safe counter to decrement when loading has finished
 *
 * @return Returns an index to the request that can be used for canceling or 0 if the request failed.
 */
QWORD FAsyncIOManagerWindows::LoadData( FString Filename, UINT Offset, UINT Size, void* Dest, FThreadSafeCounter* Counter )
{
	HANDLE FileHandle = NameToHandleMap.FindRef( Filename );

	if( !FileHandle )
	{
#ifndef XBOX
		FileHandle = CreateFileW( *Filename, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );
#else
		FFilename CookedFilename = Filename;
		CookedFilename = CookedFilename.GetPath() + TEXT("\\") + CookedFilename.GetBaseFilename() + TEXT(".xxx");

		FileHandle =	CreateFileA( GetXenonFilename(*CookedFilename),	GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL );
#endif
	}

	if( FileHandle != INVALID_HANDLE_VALUE )
	{
		FScopeLock ScopeLock( CriticalSection );

		NameToHandleMap.Set( *Filename, FileHandle );

		FAsyncIORequest IORequest;

		IORequest.RequestIndex	= RequestIndex++;
		IORequest.FileHandle	= FileHandle;
		IORequest.Offset		= Offset;
		IORequest.Size			= Size;
		IORequest.Dest			= Dest;
		IORequest.Counter		= Counter;
	
		OutstandingRequests.AddItem( IORequest );

		// Trigger event telling IO thread to wake up to perform work.
		OutstandingRequestsEvent->Trigger();

		return IORequest.RequestIndex;
	}
	else
	{
		return 0;
	}
}

/**
 * Removes N outstanding requests from the queue in an atomic all or nothing operation.
 * NOTE: Requests are only canceled if ALL requests are still pending and neither one
 * is currently in flight.
 *
 * @param	RequestIndices	Indices of requests to cancel.
 * @return	TRUE if all requests were still outstanding, FALSE otherwise
 */
UBOOL FAsyncIOManagerWindows::CancelRequests( QWORD* RequestIndices, UINT NumIndices )
{
	FScopeLock ScopeLock( CriticalSection );
	
	UINT	NumFound			= 0;
	UINT*	OutstandingIndices	= (UINT*) appAlloca( sizeof(UINT) * NumIndices );

	for( UINT OutstandingIndex=0; OutstandingIndex<(UINT)OutstandingRequests.Num() && NumFound<NumIndices; OutstandingIndex++ )
	{
		for( UINT RequestIndex=0; RequestIndex<NumIndices; RequestIndex++ )
		{
			if( OutstandingRequests(OutstandingIndex).RequestIndex == RequestIndices[RequestIndex] )
			{
				OutstandingIndices[NumFound] = OutstandingIndex;
				NumFound++;
			}
		}
	}

	if( NumFound == NumIndices )
	{
		for( UINT RequestIndex=0; RequestIndex<NumFound; RequestIndex++ )
		{
			OutstandingRequests.Remove( OutstandingIndices[RequestIndex] - RequestIndex );
		}

		return TRUE;
	}
	else
	{
		return FALSE;
	}
}

/**
 * Blocks till all currently outstanding requests are canceled.
 */
void FAsyncIOManagerWindows::CancelAllRequests()
{
	// We need the scope lock here to ensure that BusyReading isn't being updated while we try to read from it.
	FScopeLock ScopeLock( CriticalSection );

	OutstandingRequests.Empty();

	while( BusyReading.GetValue() )
	{
		// We could probably use an event instead.
		Sleep(1);
	}
}

#endif
