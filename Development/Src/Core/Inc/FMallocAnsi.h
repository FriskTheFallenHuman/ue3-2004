/*=============================================================================
	FMallocAnsi.h: ANSI memory allocator.
	Copyright 1997-1999 Epic Games, Inc. All Rights Reserved.

	Revision history:
		* Created by Tim Sweeney
=============================================================================*/

//
// ANSI C memory allocator.
//
class FMallocAnsi : public FMalloc
{
public:
	// FMalloc interface.
	void* Malloc( SIZE_T Size )
	{
		check(Size>=0);
		void* Ptr = malloc( Size );
		check(Ptr);
		return Ptr;
	}
	void* Realloc( void* Ptr, SIZE_T NewSize )
	{
		check(NewSize>=0);
		void* Result;
		
		if ((PTRINT) Ptr < 0x10000)
		{
			Ptr = NULL;
		}
		
		if( Ptr && NewSize )
		{
			Result = realloc( Ptr, NewSize );
		}
		else if ( NewSize )
		{
			Result = malloc( NewSize );
		}
		else
		{
			if ( Ptr )
				free( Ptr );
			Result = NULL;
		}
		return Result;
	}
	void Free( void* Ptr )
	{
		if ((PTRINT) Ptr < 0x10000)
		{
			Ptr = NULL;
		}

		free( Ptr );
	}
	void DumpAllocs()
	{
		debugf( NAME_Exit, TEXT("Allocation checking disabled") );
	}
	void HeapCheck()
	{
#if (defined _MSC_VER)
		INT Result = _heapchk();
		check(Result!=_HEAPBADBEGIN);
		check(Result!=_HEAPBADNODE);
		check(Result!=_HEAPBADPTR);
		check(Result!=_HEAPEMPTY);
		check(Result==_HEAPOK);
#endif
	}
	void Init( UBOOL Reset )
	{
#if _MSC_VER
		// Enable low fragmentation heap - http://msdn2.microsoft.com/en-US/library/aa366750.aspx
		intptr_t	CrtHeapHandle = _get_heap_handle();
		ULONG		EnableLFH = 2;
		HeapSetInformation( (PVOID)CrtHeapHandle, HeapCompatibilityInformation, &EnableLFH, sizeof(EnableLFH) );
#endif
	}
	void Exit()
	{
	}
};

/*-----------------------------------------------------------------------------
	The End.
-----------------------------------------------------------------------------*/

