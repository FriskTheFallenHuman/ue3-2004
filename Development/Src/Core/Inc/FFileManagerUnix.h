/*=============================================================================
	FFileManagerUnix.h: Unreal Unix based file manager.
	Copyright 1997-1999 Epic Games, Inc. All Rights Reserved.

	Revision history:
		* Created by Tim Sweeney
=============================================================================*/

#include "FFileManagerGeneric.h"

/*-----------------------------------------------------------------------------
	FArchiveFileReaderUnix
-----------------------------------------------------------------------------*/

// File manager.
class FArchiveFileReaderUnix : public FArchive
{
public:
    FArchiveFileReaderUnix( INT InFile, BYTE* InMap, FOutputDevice* InError, INT InSize );
    ~FArchiveFileReaderUnix();

    virtual void Precache( INT HintCount );
    virtual void Seek( INT InPos );
    virtual INT Tell();
    virtual INT TotalSize();
    virtual UBOOL Close();
    virtual void Serialize( void* V, INT Length );

protected:
    INT				File;
    FOutputDevice*	Error;
    INT				Size;
    INT				Pos;
    BYTE*			Map;
};

/*-----------------------------------------------------------------------------
	FArchiveFileWriterUnix
-----------------------------------------------------------------------------*/

class FArchiveFileWriterUnix : public FArchive
{
public:
    FArchiveFileWriterUnix( FILE* InFile, FOutputDevice* InError );
    ~FArchiveFileWriterUnix();

    virtual void Seek( INT InPos );
    virtual INT Tell();
    virtual UBOOL Close();
    virtual void Serialize( void* V, INT Length );
    virtual void Flush();

protected:
    FILE*			File;
    FOutputDevice*	Error;
    INT				Pos;
    INT				BufferCount;
    BYTE			Buffer[4096];
};


/*-----------------------------------------------------------------------------
	FFileManagerUnix
-----------------------------------------------------------------------------*/

class FFileManagerUnix : public FFileManagerGeneric
{
public:
	UBOOL SetDefaultDirectory();
	FString GetCurrentDirectory();

	FArchive* CreateFileReader( const TCHAR* OrigFilename, DWORD Flags, FOutputDevice* Error );
    FArchive* CreateFileWriter( const TCHAR* OrigFilename, DWORD Flags, FOutputDevice* Error );
	INT FileSize( const TCHAR* Filename );
	DWORD Copy( const TCHAR* DestFile, const TCHAR* SrcFile, UBOOL ReplaceExisting, UBOOL EvenIfReadOnly, UBOOL Attributes, DWORD Compress, FCopyProgress* Progress );
	UBOOL Delete( const TCHAR* OrigFilename, UBOOL RequireExists=0, UBOOL EvenReadOnly=0 );
	UBOOL IsReadOnly( const TCHAR* Filename );
	UBOOL Move(const TCHAR* Dest, const TCHAR* Src, UBOOL Replace = 1, UBOOL EvenIfReadOnly = 0, UBOOL Attributes = 0);
	UBOOL MakeDirectory( const TCHAR* OrigPath, UBOOL Tree=0 );
	UBOOL DeleteDirectory( const TCHAR* OrigPath, UBOOL RequireExists=0, UBOOL Tree=0 );
	void FindFiles( TArray<FString>& Result, const TCHAR* OrigPattern, UBOOL Files, UBOOL Directories );
	DOUBLE GetFileAgeSeconds( const TCHAR* Filename );

private:
    void PathSeparatorFixup( TCHAR* Dest, const TCHAR* Src )
    {
        appStrcpy( Dest, Src );
        for( TCHAR *Cur = Dest; *Cur != TEXT('\0'); Cur++ )
        if( *Cur == TEXT('\\') )
            *Cur = TEXT('/');
    }
};

/*-----------------------------------------------------------------------------
	The End.
-----------------------------------------------------------------------------*/

