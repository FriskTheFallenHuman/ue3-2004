/*=============================================================================
	UnAsyncWork.h: Definition of queued work classes
	Copyright 1997-2005 Epic Games, Inc. All Rights Reserved.
=============================================================================*/

#ifndef _UNASYNCWORK_H
#define _UNASYNCWORK_H


/**
 * Base class for simple implementatin of async queued work.
 */
class FAsyncWorkBase : public FQueuedWork
{
private:
#if XBOX
	/**
	 * Use a spin lock because we know that the thread is not competing with
	 * the main thread for cycles
	 */
	volatile UBOOL bIsDone;
#else
	/**
	 * Event used to indicate the copy is done
	 */
	FEvent* DoneEvent;
#endif

	/**
	 * Thread safe counter that is optionally used to signal completion of the
	 * async work in the case of auto- destroy.
	 */
	FThreadSafeCounter* WorkCompletionCounter;

public:
	/**
	 * Caches the thread safe counter and uses self deletion to free itself if it's non- NULL.
	 * A NULL pointer indicates that IsDone is going to be called instead and the caller is 
	 * responsible for deletion.
	 *
	 * @param InWorkCompletionCounter	Counter to decrement on completion of task if non- NULL
	 */
	FAsyncWorkBase( FThreadSafeCounter* InWorkCompletionCounter );

	/**
	 * Cleans up the event on non-Xbox platforms.
	 */
	virtual ~FAsyncWorkBase(void);

	/**
	 * Returns whether the work is done.
	 *
	 * @return TRUE if work has been completed, FALSE otherwise
	 */
	UBOOL IsDone(void);

	/**
	 * Called if entry is removed from queue before it has started which might happen at exit.
	 */
	virtual void Abandon(void)
	{}

	/**
	 * Called after work has finished. Marks object as being done.	 
	 */ 
	virtual void Dispose(void);
};

/**
 * Asynchronous memory copy, used for copying large amounts of data in the
 * background
 */
class FAsyncCopy : public FAsyncWorkBase
{
protected:
	/** The source memory for the copy						*/
	void* Src;
	/** The destination memory being written to				*/
	void* Dest;
	/** The amount of data to copy in bytes					*/
	DWORD Size;

public:
	/**
	 * Initializes the data and creates the event on non Xbox platforms
	 */
	FAsyncCopy(void* Dest,void* Src,DWORD Size,FThreadSafeCounter* Counter = NULL);

	/**
	 * Performs the async copy
	 */
	virtual void DoWork(void);
};

/**
 * Asynchronous memory copy, used for copying large amounts of data in the
 * background. This version has platform specific alignment and caching 
 * restrictions so use with care!
 */
class FAsyncCopyAligned : public FAsyncCopy
{
public:
	/**
	 * Passes the data to FAsyncCopy constructor.
	 */
	FAsyncCopyAligned(void* Dest,void* Src,DWORD Size,FThreadSafeCounter* Counter = NULL)
	: FAsyncCopy( Dest, Src, Size, Counter )
	{}

	/**
	 * Performs the async copy
	 */
	virtual void DoWork(void);
};

#if _MSC_VER && !CONSOLE

/**
 * Asynchronous DXT compression, used for compressing textures in the background
 */
class FAsyncDXTCompress : public FAsyncWorkBase
{
public:
	/**
	 * Initializes the data and creates the event.
	 *
	 * @param	SourceData		Source texture data to DXT compress, in BGRA 8bit per channel unsigned format.
	 * @param	InPixelFormat	Texture format
	 * @param	InSizeX			Number of texels along the X-axis
	 * @param	InSizeY			Number of texels along the Y-axis
	 * @param	SRGB			Whether the texture is in SRGB space
	 * @param	bIsNormalMap	Whether the texture is a normal map
	 * @param	bSupportDXT1a	Whether to use DXT1a or DXT1 format (if PixelFormat is DXT1)
	 * @param	NotifyEvent		Optional user-specified event to trigger when the work is done. May be NULL.
	 * @param	Counter			Optional Counter to decrement on completion of task. May be NULL.
	 */
	FAsyncDXTCompress(
		void* SourceData, enum EPixelFormat InPixelFormat, INT InSizeX, INT InSizeY, UBOOL SRGB, UBOOL bIsNormalMap, UBOOL bSupportDXT1a,
		FEvent* NotifyEvent = NULL,
		FThreadSafeCounter* Counter = NULL);

	/** Destructor. Frees the memory used for the DXT-compressed resulting data.  */
	virtual ~FAsyncDXTCompress();
	
	/**
	 * Performs the async decompression
	 */
	virtual void DoWork();

	/**
	 * This method is called after DoWork() has finished, to allow it to perform any custom cleanup.
	 * If the user has specified a notify event, it will be triggered here.
	 */ 
	virtual void Dispose();

	/**
	 * Returns the texture format.
	 * @return	Texture format
	 */
	enum EPixelFormat	GetPixelFormat() const;

	/**
	 * Returns the size of the image.
	 * @return	Number of texels along the X-axis
	 */
	INT					GetSizeX() const;

	/**
	 * Returns the size of the image.
	 * @return	Number of texels along the Y-axis
	 */
	INT					GetSizeY() const;

	/**
	 * Returns the compressed data, once the work is done. This buffer will be deleted when the work is deleted.
	 * @return	Start address of the compressed data
	 */
	const void*			GetResultData() const;

	/**
	 * Returns the size of compressed data in number of bytes, once the work is done.
	 * @return	Size of compressed data, in number of bytes
	 */
	INT					GetResultSize() const;

protected:
	/** Optional user-specified event to trigger when the work is done. */
	FEvent* NotifyEvent;

	/** Compression result */
	struct FNVCompression* Compression;

	/** Texture format */
	enum EPixelFormat	PixelFormat;

	/** Number of texels along the X-axis */
	INT					SizeX;

	/** Number of texels along the Y-axis */
	INT					SizeY;
};

/**
 * Starts an asynchronous queued work to DXT-compress an image.
 *
 * @param	SourceData		Source image data to DXT compress, in BGRA 8bit per channel unsigned format.
 * @param	InPixelFormat	Image format
 * @param	InSizeX			Number of texels along the X-axis
 * @param	InSizeY			Number of texels along the Y-axis
 * @param	SRGB			Whether the texture is in SRGB space
 * @param	bIsNormalMap	Whether the texture is a normal map
 * @param	bSupportDXT1a	Whether to use DXT1a or DXT1 format (if PixelFormat is DXT1)
 * @param	NotifyEvent		Optional user-specified event to trigger when the work is done. May be NULL.
 * @return					The newly created async work
 */
FAsyncDXTCompress* DXTCompressAsync( void* SourceData, enum EPixelFormat PixelFormat, INT SizeX, INT SizeY, UBOOL SRGB, UBOOL bIsNormalMap, UBOOL bSupportDXT1a, FEvent* NotifyEvent=NULL );

#endif	// _MSC_VER && !CONSOLE

#endif
