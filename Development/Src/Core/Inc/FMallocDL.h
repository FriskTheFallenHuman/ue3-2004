/*=============================================================================
	FMallocDL.h: Doug Lea memory allocator
	Copyright 2021-2022 Flaming Entertainment, LLC

Revision history:
	* Created by Pat 'raynorpat' Raynor.
=============================================================================*/

extern "C" {
	#define USE_DL_PREFIX 1
	#define USE_LOCKS 1
	#define USE_SPIN_LOCKS 0
	//#define USE_RECURSIVE_LOCKS 1

	#include "../../../External/dlmalloc/malloc-2.8.6.h"

	#if _MSC_VER
		#pragma warning(push)
		#pragma warning(disable : 4668)	// macro redefinition
	#endif

	#include "../../../External/dlmalloc/malloc-2.8.6.c"

	#if _MSC_VER
		#pragma warning(pop)
	#endif
}

//
// Doug Lea POSIX/ANSI memory allocator.
//
class FMallocDL : public FMalloc
{
public:

	virtual void* Malloc( SIZE_T Count )
	{
		void* Ptr = dlmalloc( Count );
		check(Ptr);
		return Ptr;
	}

	virtual void* Realloc( void* Original, SIZE_T Count )
	{
		void* Result;
		if( Original && Count )
		{
			Result = dlrealloc( Original, Count );
		}
		else if( Count )
		{
			Result = dlmalloc( Count );
		}
		else
		{
			if( Original )
				dlfree( Original );
			Result = NULL;
		}
		return Result;
	}
	
	void Free( void* Ptr )
	{
		dlfree( Ptr );
	}

	void DumpAllocs()
	{
		struct mallinfo minfo = dlmallinfo();
		debugf(TEXT("Free chunks %d"), minfo.ordblks);
		debugf(TEXT("Num fastbin blocks %d"), minfo.smblks);
		debugf(TEXT("Fastbin free space %d"), minfo.fsmblks);
		debugf(TEXT("Fastbin max allocated space %d (%6.1fKB)"), minfo.usmblks, minfo.usmblks/1024.f);
		debugf(TEXT("Total allocated space %d (%6.1fKB)"), minfo.uordblks, minfo.uordblks/1024.f);
		debugf(TEXT("Total free space %d (%6.1fKB)"), minfo.fordblks, minfo.fordblks/1024.f);
	}

	void HeapCheck()
	{
	}

	void Init( UBOOL Reset )
	{
	}

	void Exit()
	{
	}
};

/*-----------------------------------------------------------------------------
	The End.
-----------------------------------------------------------------------------*/

