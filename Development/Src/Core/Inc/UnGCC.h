/*=============================================================================
	UnGCC.h: Unreal definitions for Gnu G++. Unfinished. Unsupported.
	Copyright 1997-1999 Epic Games, Inc. All Rights Reserved.
=============================================================================*/

/*----------------------------------------------------------------------------
	Platform compiler definitions.
----------------------------------------------------------------------------*/
#ifndef _INCL_UNGNUG_H_
#define _INCL_UNGNUG_H_

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <float.h>

// Unicode
#ifndef __APPLE__
    #include <uchar.h>
#endif

#undef ASM
#undef ASM3DNOW
#undef ASMKNI
#undef ASMLINUX

#if defined(__APPLE__)

	#define __UNIX__  1
    #define MACOSX 1

	#if defined(__LITTLE_ENDIAN__)
		#define __INTEL_BYTE_ORDER__ 1
	#endif

#elif defined(__linux__) || defined(__unix__)

    #define __UNIX__  1
	#define __LINUX__ 1

    #include <endian.h>
    #if __BYTE_ORDER == __LITTLE_ENDIAN
        #define __INTEL_BYTE_ORDER__ 1
    #endif

	#if defined(_LINUX64) || defined(_LP64)
		#define __x86_64__ 1
	#endif
	
#else

	#error Unsupported platform.

#endif

// Compiler name.
#if defined(__clang__)
    #define COMPILER "Compiled with Clang ("__VERSION__")"
#else
    #define COMPILER "Compiled with GNU g++ ("__VERSION__")"
#endif

// Platform CPU type
#if defined(__x86_64__) || defined(__ppc64__) || defined(__powerpc64__) || defined(__aarch64__)
	#define PLATFORM_64BITS 1
#else
	#define PLATFORM_32BITS 1
#endif

#define SUPPORTS_PRAGMA_PACK 1

/*----------------------------------------------------------------------------
	Platform specifics types and defines.
----------------------------------------------------------------------------*/

// Undo any Windows defines.
#undef BYTE
#undef WORD
#undef DWORD
#undef INT
#undef FLOAT
#undef MAXBYTE
#undef MAXWORD
#undef MAXDWORD
#undef MAXINT
#undef VOID
#undef CDECL

// Make sure HANDLE is defined.
#define HANDLE DWORD
#define HINSTANCE DWORD

// Sizes.
enum {DEFAULT_ALIGNMENT = 8 }; // Default boundary to align memory allocations on.
enum {CACHE_LINE_SIZE   = 32}; // Cache line size.

#if __GNUG__
#define GCC_PACK(n)  __attribute__((packed,aligned(n)))
#define GCC_ALIGN(n) __attribute__((aligned(n)))
#define GCC_MOVE_ALIGN(n) 
#else
#define GCC_PACK(n) 
#define GCC_ALIGN(n) 
#define GCC_MOVE_ALIGN(n) 
#endif

// LLVM needs aligned access, and gcc seems maybe even a tiny bit faster with it
#define REQUIRES_ALIGNED_ACCESS 1

// Optimization macros
#define DISABLE_OPTIMIZATION  
#define ENABLE_OPTIMIZATION  

// Function type macros.
#define VARARGS
#define CDECL
#define STDCALL
#define FORCEINLINE inline
#define ZEROARRAY 0 /* Zero-length arrays in structs */
#define __cdecl
#define NOEXCEPT

#define VSNPRINTF wvsnprintf

// Variable arguments.
#define GET_VARARGS(msg,len,lastarg,fmt)	\
{	\
	va_list ArgPtr;	\
	va_start( ArgPtr, lastarg );	\
	VSNPRINTF( msg, len, fmt, ArgPtr );	\
	va_end( ArgPtr );	\
}

#define GET_VARARGS_RESULT(msg,len,lastarg,fmt,result)	\
{	\
	va_list ArgPtr;	\
	va_start( ArgPtr, lastarg );	\
	result = VSNPRINTF( msg, len, fmt, ArgPtr );	\
	va_end( ArgPtr );	\
}

#include <stdint.h>

// Unsigned base types.
typedef uint8_t             BYTE;		// 8-bit  unsigned.
typedef uint16_t            _WORD;		// 16-bit unsigned.
typedef uint32_t            UINT;		// 32-bit unsigned.
typedef uint32_t            DWORD;		// 32-bit unsigned.
typedef uint64_t            QWORD;		// 64-bit unsigned.

// Signed base types.
typedef	int8_t              SBYTE;		// 8-bit  signed.
typedef int16_t				SHORT;		// 16-bit signed.
typedef int16_t             SWORD;		// 16-bit signed.
typedef int32_t             INT;		// 32-bit signed.
typedef int64_t             SQWORD;		// 64-bit signed.

// Character types.
typedef char			    ANSICHAR;	// An ANSI character.
typedef unsigned char		ANSICHARU;	// An ANSI character.

// Clang / GCC has 4 byte wchar_t, so we will use char16_t
typedef char16_t            UNICHAR;	// A unicode character.
typedef char16_t            UNICHARU;	// A unicode character.

// Other base types.
typedef int32_t				UBOOL;		// Boolean 0 (false) or 1 (true).
typedef float				FLOAT;		// 32-bit IEEE floating point.
typedef double				DOUBLE;		// 64-bit IEEE double.
typedef long				LONG;		//
typedef size_t              SIZE_T;     // Corresponds to C SIZE_T.
typedef uintptr_t			PTRINT;		// Integer large enough to hold a pointer.

// We also need long defined for string conversions
typedef long 				_LONG;
typedef unsigned long		_ULONG;
typedef long double			LDOUBLE;

// Bitfield type.
typedef unsigned int		BITFIELD;	// For bitfields.

// Make sure characters are signed.
#ifdef __CHAR_UNSIGNED__
	#error "Bad compiler option: Characters must be signed"
#endif

// Strings.
#define LINE_TERMINATOR TEXT("\n")
#define PATH_SEPARATOR TEXT("/")

// DLL file extension.
#if defined(__APPLE__)
	#define DLLEXT TEXT(".dylib")
#else
	#define DLLEXT TEXT(".so")
#endif

// NULL.
#undef NULL
#define NULL 0

// Package implementation.
#define IMPLEMENT_PACKAGE_PLATFORM(pkgname) \
	BYTE GLoaded##pkgname;

// Platform support options.
#define PLATFORM_NEEDS_ARRAY_NEW 1

// To maintain 2-byte UNICHAR, we must implement our own functions
UNICHAR* wcscpy( UNICHAR* Dest, const UNICHAR* Src);
UNICHAR* wcsncpy( UNICHAR* Dest, const UNICHAR* Src, INT MaxLen );
UNICHAR* wcscat( UNICHAR* String, const UNICHAR *Add );
INT wcslen( const UNICHAR* String );
INT wcscmp( const UNICHAR* Str1, const UNICHAR *Str2 );
INT wcsncmp( const UNICHAR* Str1, const UNICHAR *Str2, INT max );
UNICHAR* wcschr( const UNICHAR* String, const UNICHAR Find );
UNICHAR* wcsstr( const UNICHAR* String, const UNICHAR* Find );
INT _wcsicmp( const UNICHAR* String1, const UNICHAR* String2 );
UNICHAR* _wcsupr( UNICHAR* String );
DOUBLE wcstod( const UNICHAR* Start, UNICHAR** End );
_LONG wcstol( const UNICHAR* Start, UNICHAR** End, INT Base );
_ULONG wcstoul( const UNICHAR* Start, UNICHAR** End, INT Base );
INT _wtoi( const UNICHAR* Str );
INT _wcsnicmp( const UNICHAR* Str1, const UNICHAR *Str2, INT max );
INT wprintf( const UNICHAR* fmt, ... );
INT vswscanf( const UNICHAR *str, const UNICHAR *fmt, va_list args );
INT swscanf( const UNICHAR *str, const UNICHAR* fmt, ... );
INT wvsnprintf( UNICHAR *str, INT size, const UNICHAR *fmt, va_list args );
INT iswspace( UNICHAR ch  );
UNICHAR *_itow( const INT Num, UNICHAR *Buffer, const INT BufSize );
QWORD _wcstoui64( const UNICHAR* Start, UNICHAR** End, INT Base );
UNICHAR* _ui64tow( QWORD Num, UNICHAR *Buffer, INT Base );
UNICHAR* _i64tow( SQWORD Num, UNICHAR *Buffer, INT Base );
FLOAT _wtof( const UNICHAR* Str );

ANSICHAR* unixToANSI( ANSICHAR* ACh, const UNICHAR* InUCh );
INT unixGetSizeANSI( const UNICHAR* InUCh );
UNICHAR* unixToUNICODE( UNICHAR* UCh, const ANSICHAR* InACh );
INT unixGetSizeUNICODE( const ANSICHAR* InACh );
UNICHAR* unixANSIToUNICODE(char* str);
void unixDetectUNICODE( void );

#define UNICHAR_TO_ANSI(str) unixToANSI((ANSICHAR*)appAlloca(unixGetSizeANSI(str)),str)
#define ANSI_TO_UNICHAR(str) unixToUNICODE((TCHAR*)appAlloca(unixGetSizeUNICODE(str)),str)

#define TCHAR_TO_ANSI UNICHAR_TO_ANSI
#define ANSI_TO_TCHAR ANSI_TO_UNICHAR

// Memory
#define appAlloca(size) ((size==0) ? 0 : alloca((size+7)&~7))

// System identification.
extern "C"
{
	extern HINSTANCE      hInstance;
}

/*----------------------------------------------------------------------------
	Math functions.
----------------------------------------------------------------------------*/

const FLOAT	SRandTemp = 1.f;
extern INT GSRandSeed;

inline INT appTrunc(FLOAT F)
{
	return (INT)F;
//	return (INT)truncf(F);
}
inline FLOAT appTruncFloat(FLOAT F)
{
	return (FLOAT)appTrunc(F);
}

inline FLOAT appExp(FLOAT Value) { return expf(Value); }
inline FLOAT appLoge(FLOAT Value) { return logf(Value); }
inline FLOAT appFmod(FLOAT Y, FLOAT X) { return fmodf(Y, X); }
inline FLOAT appSin(FLOAT Value) { return sinf(Value); }
inline FLOAT appAsin(FLOAT Value) { return asinf((Value < -1.f) ? -1.f : ((Value < 1.f) ? Value : 1.f)); }
inline FLOAT appCos(FLOAT Value) { return cosf(Value); }
inline FLOAT appAcos(FLOAT Value) { return acosf((Value < -1.f) ? -1.f : ((Value < 1.f) ? Value : 1.f)); }
inline FLOAT appTan(FLOAT Value) { return tanf(Value); }
inline FLOAT appAtan(FLOAT Value) { return atanf(Value); }
inline FLOAT appAtan2(FLOAT Y, FLOAT X) { return atan2f(Y, X); }
inline FLOAT appSqrt(FLOAT Value);
inline FLOAT appPow(FLOAT A, FLOAT B) { return powf(A, B); }
inline UBOOL appIsNan(FLOAT A) { return isnan(A) != 0; }
inline INT appFloor(FLOAT F);
inline INT appCeil(FLOAT Value) { return appTrunc(ceilf(Value)); }
inline INT appRand() { return rand(); }
inline void appRandInit(INT Seed) { srand(Seed); }
inline FLOAT appFrand() { return rand() / (FLOAT)RAND_MAX; }
inline void appSRandInit(INT Seed) { GSRandSeed = Seed; }

inline FLOAT appFractional(FLOAT Value) { return Value - appTruncFloat(Value); }

inline FLOAT appSRand()
{
	GSRandSeed = (GSRandSeed * 196314165) + 907633515;
	//@todo fix type aliasing
	FLOAT Result;
	*(INT*)&Result = (*(INT*)&SRandTemp & 0xff800000) | (GSRandSeed & 0x007fffff);
	return appFractional(Result);
}

//
//  MSM: Round (to nearest) a floating point number to an integer.
//
inline INT appRound(FLOAT F)
{
	return appTrunc(roundf(F));
}

inline INT appFloor(FLOAT F)
{
	return appTrunc(floorf(F));
}

inline FLOAT appInvSqrt(FLOAT F)
{
	return 1.0f / sqrtf(F);
}

//
// MSM: Fast float square root using SSE.
// Accurate to within 1 LSB.
//
inline FLOAT appSqrt(FLOAT F)
{
	return sqrtf(F);
}

#if defined(__MACH__)
	#include <mach/mach_time.h>
#else
	#include <time.h>
#endif

extern DOUBLE GSecondsPerCycle;

//
// CPU cycles, related to GSecondsPerCycle.
//
#if ASMLINUX
#define DEFINED_appCycles 1
inline DWORD appCycles()
{
	DWORD r;
	asm("rdtsc" : "=a" (r) : "d" (r));
	return r;
}
#endif

DOUBLE appSecondsSlow();

//
// Seconds, arbitrarily based.
//
#define DEFINED_appSeconds 1
inline DOUBLE appSeconds()
{
#if defined(CLOCK_MONOTONIC)
    struct timespec t;
    clock_gettime(CLOCK_MONOTONIC, &t);
    return 1e-9 * (FLOAT)t.tv_sec + (FLOAT)t.tv_nsec;
#elif defined(__MACH__)
    static FLOAT factor = 0;
    if( factor == 0 )
    {
        mach_timebase_info_data_t info;
        if( mach_timebase_info( &info ) == 0 )
            factor = 1e-9 * (FLOAT)info.numer / (FLOAT)info.denom;
        else
            factor = -1;
    }
    if( factor != -1 )
        return factor * (FLOAT)mach_absolute_time();
    else
        return appSecondsSlow();
#else
    return appSecondsSlow();
#endif
}

//
// Memory copy.
//
#define DEFINED_appMemcpy 1
inline void appMemcpy( void* Dest, const void* Src, INT Count )
{
	//!!vogel: TODO
	memcpy( Dest, Src, Count );
}

//
// Memory zero.
//
#define DEFINED_appMemzero 1
inline void appMemzero( void* Dest, INT Count )
{
	//!!vogel: TODO
	memset( Dest, 0, Count );
}

#endif  // include-once blocker.

/*----------------------------------------------------------------------------
	The End.
----------------------------------------------------------------------------*/

