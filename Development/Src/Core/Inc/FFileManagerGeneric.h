/*=============================================================================
	FFileManagerGeneric.h: Unreal generic file manager support code.
	Copyright 1997-2005 Epic Games, Inc. All Rights Reserved.

	This base class simplifies FFileManager implementations by providing
	simple, unoptimized implementations of functions whose implementations
	can be derived from other functions.
=============================================================================*/

/*-----------------------------------------------------------------------------
	File Manager.
-----------------------------------------------------------------------------*/

class FFileManagerGeneric : public FFileManager
{
public:
	virtual INT FileSize( const TCHAR* Filename );
	virtual DWORD Copy( const TCHAR* InDestFile, const TCHAR* InSrcFile, UBOOL ReplaceExisting, UBOOL EvenIfReadOnly, UBOOL Attributes, DWORD Compress, FCopyProgress* Progress );
	virtual UBOOL MakeDirectory(const TCHAR* Path, UBOOL Tree = 0);
	virtual UBOOL DeleteDirectory( const TCHAR* Path, UBOOL RequireExists=0, UBOOL Tree=0 );
	virtual UBOOL Move( const TCHAR* Dest, const TCHAR* Src, UBOOL ReplaceExisting=1, UBOOL EvenIfReadOnly=0, UBOOL Attributes=0 );
	
protected:
	virtual UBOOL IsDrive( const TCHAR* Path );
};

/*-----------------------------------------------------------------------------
	The End.
-----------------------------------------------------------------------------*/

