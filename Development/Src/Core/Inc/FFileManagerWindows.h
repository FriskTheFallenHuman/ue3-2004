/*=============================================================================
	FFileManagerWindows.h: Unreal Windows OS based file manager.
	Copyright 1997-2005 Epic Games, Inc. All Rights Reserved.
=============================================================================*/

#include "FFileManagerGeneric.h"

/*-----------------------------------------------------------------------------
	FArchiveFileReaderWindows
-----------------------------------------------------------------------------*/

// File manager.
class FArchiveFileReaderWindows : public FArchive
{
public:
	FArchiveFileReaderWindows( HANDLE InHandle, FOutputDevice* InError, INT InSize );
	~FArchiveFileReaderWindows();

	virtual void Precache( INT HintCount );
	virtual void Seek( INT InPos );
	virtual INT Tell();
	virtual INT TotalSize();
	virtual UBOOL Close();
	virtual void Serialize( void* V, INT Length );

protected:
	HANDLE          Handle;
	FOutputDevice*  Error;
	INT             Size;
	INT             Pos;
	INT             BufferBase;
	INT             BufferCount;
	BYTE            Buffer[1024];
};


/*-----------------------------------------------------------------------------
	FArchiveFileWriterWindows
-----------------------------------------------------------------------------*/

class FArchiveFileWriterWindows : public FArchive
{
public:
	FArchiveFileWriterWindows( HANDLE InHandle, FOutputDevice* InError, INT InPos );
	~FArchiveFileWriterWindows();

	virtual void Seek( INT InPos );
	virtual INT Tell();
	virtual UBOOL Close();
	virtual void Serialize( void* V, INT Length );
	virtual void Flush();

protected:
	HANDLE          Handle;
	FOutputDevice*  Error;
	INT             Pos;
	INT             BufferCount;
	BYTE            Buffer[4096];
};


/*-----------------------------------------------------------------------------
	FFileManagerWindows
-----------------------------------------------------------------------------*/

class FFileManagerWindows : public FFileManagerGeneric
{
public:
	UBOOL SetDefaultDirectory();
	FString GetCurrentDirectory();

	FArchive* CreateFileReader( const TCHAR* Filename, DWORD Flags, FOutputDevice* Error );
	FArchive* CreateFileWriter( const TCHAR* Filename, DWORD Flags, FOutputDevice* Error );
	INT FileSize( const TCHAR* Filename );
	DWORD Copy( const TCHAR* DestFile, const TCHAR* SrcFile, UBOOL ReplaceExisting, UBOOL EvenIfReadOnly, UBOOL Attributes, DWORD Compress, FCopyProgress* Progress );
	UBOOL Delete( const TCHAR* Filename, UBOOL RequireExists=0, UBOOL EvenReadOnly=0 );
	UBOOL IsReadOnly( const TCHAR* Filename );
	UBOOL Move( const TCHAR* Dest, const TCHAR* Src, UBOOL Replace=1, UBOOL EvenIfReadOnly=0, UBOOL Attributes=0 );
	UBOOL MakeDirectory( const TCHAR* Path, UBOOL Tree=0 );
	UBOOL DeleteDirectory( const TCHAR* Path, UBOOL RequireExists=0, UBOOL Tree=0 );
	void FindFiles( TArray<FString>& Result, const TCHAR* Filename, UBOOL Files, UBOOL Directories );
	DOUBLE GetFileAgeSeconds( const TCHAR* Filename );
};

/*-----------------------------------------------------------------------------
	The End.
-----------------------------------------------------------------------------*/

