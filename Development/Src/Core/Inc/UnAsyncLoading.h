/**
 * UnAsyncLoading.h -- Contains all base level interfaces for Async
 * loading support in the Unreal engine.
 *
 * Copyright 2004-2005 Epic Games, Inc. All Rights Reserved.
 */

#ifndef _UNASYNCLOADING_H
#define _UNASYNCLOADING_H

/**
 * Virtual base class of async. IO manager, explicitely thread aware.	
 */
struct FAsyncIOManager : public FRunnable
{
	/**
	 * Requests data to be loaded async. Returns immediately.
	 *
	 * @param	Filename	Filename to load
	 * @param	Offset		Offset into file
	 * @param	Size		Size of load request
	 * @param	Dest		Pointer to load data into
	 * @param	Counter		Thread safe counter to decrement when loading has finished
	 *
	 * @return Returns an index to the request that can be used for canceling or 0 if the request failed.
	 */
	virtual QWORD LoadData( FString Filename, UINT Offset, UINT Size, void* Dest, FThreadSafeCounter* Counter ) = 0;
	
	/**
	 * Removes N outstanding requests from the queue in an atomic all or nothing operation.
	 * NOTE: Requests are only canceled if ALL requests are still pending and neither one
	 * is currently in flight.
	 *
	 * @param	RequestIndices	Indices of requests to cancel.
	 * @return	TRUE if all requests were still outstanding, FALSE otherwise
	 */
	virtual UBOOL CancelRequests( QWORD* RequestIndices, UINT NumIndices ) = 0;
	
	/**
	 * Blocks till all currently outstanding requests are canceled.
	 */
	virtual void CancelAllRequests() = 0;
};

#if _WINDOWS

/**
 * Windows implementation of an async IO manager.	
 */
struct FAsyncIOManagerWindows : public FAsyncIOManager
{
	// FAsyncIOManager interface.

	/**
	 * Requests data to be loaded async. Returns immediately.
	 *
	 * @param	Filename	Filename to load
	 * @param	Offset		Offset into file
	 * @param	Size		Size of load request
	 * @param	Dest		Pointer to load data into
	 * @param	Counter		Thread safe counter to decrement when loading has finished
	 *
	 * @return Returns an index to the request that can be used for canceling or 0 if the request failed.
	 */
	virtual QWORD LoadData( FString Filename, UINT Offset, UINT Size, void* Dest, FThreadSafeCounter* Counter );

	/**
	 * Removes N outstanding requests from the queue in an atomic all or nothing operation.
	 * NOTE: Requests are only canceled if ALL requests are still pending and neither one
	 * is currently in flight.
	 *
	 * @param	RequestIndices	Indices of requests to cancel.
	 * @return	TRUE if all requests were still outstanding, FALSE otherwise
	 */
	virtual UBOOL CancelRequests( QWORD* RequestIndices, UINT NumIndices );
	
	/**
	 * Blocks till all currently outstanding requests are canceled.
	 */
	virtual void CancelAllRequests();

	// FRunnable interface.

	/**
	 * Initializes critical section, event and other used variables. 
	 *
	 * This is called in the context of the thread object that aggregates 
	 * this, not the thread that passes this runnable to a new thread.
	 *
	 * @return True if initialization was successful, false otherwise
	 */
	virtual UBOOL Init();

	/**
	 * Called in the context of the aggregating thread to perform cleanup.
	 */
	virtual void Exit();

	/**
	 * This is where all the actual loading is done. This is only called
	 * if the initialization was successful.
	 *
	 * @return always 0
	 */
	virtual DWORD Run();
	
	/**
	 * This is called if a thread is requested to terminate early
	 */
	virtual void Stop();
	
protected:
	struct FAsyncIORequest
	{
		/** Index of request */
		QWORD				RequestIndex;
		/** Handle to file */
		HANDLE				FileHandle;
		/** Offset into file */
		UINT				Offset;
		/** Size in bytes of data to read */
		UINT				Size;
		/** Pointer to memory region used to read data into */
		void*				Dest;
		/** Thread safe counter that is decremented once work is done */
		FThreadSafeCounter* Counter;
	};

	/** Critical section used to syncronize access to outstanding requests map */
	FCriticalSection*		CriticalSection;
	/** TMap of file names to file handles */
	TMap<FString,HANDLE>	NameToHandleMap;
	/** Array of outstanding requests, processed in FIFO order */
	TArray<FAsyncIORequest>	OutstandingRequests;
	/** Event that is signaled if there are outstanding requests */
	FEvent*					OutstandingRequestsEvent;
	/** Thread safe counter that is 1 if the thread is currently reading from disk, 0 otherwise */
	FThreadSafeCounter		BusyReading;
	/** Thread safe counter that is 1 if the thread is available to process requests, 0 otherwise */
	FThreadSafeCounter		IsRunning;
	/** Current request index. We don't really worry about wrapping around with a QWORD */
	QWORD					RequestIndex;
};

#elif PLATFORM_UNIX

/**
 * Unix implementation of an async IO manager.
 */
struct FAsyncIOManagerUnix : public FAsyncIOManager
{
	// FAsyncIOManager interface.

	/**
	 * Requests data to be loaded async. Returns immediately.
	 *
	 * @param	Filename	Filename to load
	 * @param	Offset		Offset into file
	 * @param	Size		Size of load request
	 * @param	Dest		Pointer to load data into
	 * @param	Counter		Thread safe counter to decrement when loading has finished
	 *
	 * @return Returns an index to the request that can be used for canceling or 0 if the request failed.
	 */
	virtual QWORD LoadData(FString Filename, UINT Offset, UINT Size, void* Dest, FThreadSafeCounter* Counter);

	/**
	 * Removes N outstanding requests from the queue in an atomic all or nothing operation.
	 * NOTE: Requests are only canceled if ALL requests are still pending and neither one
	 * is currently in flight.
	 *
	 * @param	RequestIndices	Indices of requests to cancel.
	 * @return	TRUE if all requests were still outstanding, FALSE otherwise
	 */
	virtual UBOOL CancelRequests(QWORD* RequestIndices, UINT NumIndices);

	/**
	 * Blocks till all currently outstanding requests are canceled.
	 */
	virtual void CancelAllRequests();

	// FRunnable interface.

	/**
	 * Initializes critical section, event and other used variables.
	 *
	 * This is called in the context of the thread object that aggregates
	 * this, not the thread that passes this runnable to a new thread.
	 *
	 * @return True if initialization was successful, false otherwise
	 */
	virtual UBOOL Init();

	/**
	 * Called in the context of the aggregating thread to perform cleanup.
	 */
	virtual void Exit();

	/**
	 * This is where all the actual loading is done. This is only called
	 * if the initialization was successful.
	 *
	 * @return always 0
	 */
	virtual DWORD Run();

	/**
	 * This is called if a thread is requested to terminate early
	 */
	virtual void Stop();

protected:
	struct FAsyncIORequest
	{
		/** Index of request */
		QWORD				RequestIndex;
		/** Handle to file */
		HANDLE				FileHandle;
		/** Offset into file */
		UINT				Offset;
		/** Size in bytes of data to read */
		UINT				Size;
		/** Pointer to memory region used to read data into */
		void*				Dest;
		/** Thread safe counter that is decremented once work is done */
		FThreadSafeCounter* Counter;
	};

	/** Critical section used to syncronize access to outstanding requests map */
	FCriticalSection*		CriticalSection;
	/** TMap of file names to file handles */
	TMap<FString, HANDLE>	NameToHandleMap;
	/** Array of outstanding requests, processed in FIFO order */
	TArray<FAsyncIORequest>	OutstandingRequests;
	/** Event that is signaled if there are outstanding requests */
	FEvent*					OutstandingRequestsEvent;
	/** Thread safe counter that is 1 if the thread is currently reading from disk, 0 otherwise */
	FThreadSafeCounter		BusyReading;
	/** Thread safe counter that is 1 if the thread is available to process requests, 0 otherwise */
	FThreadSafeCounter		IsRunning;
	/** Current request index. We don't really worry about wrapping around with a QWORD */
	QWORD					RequestIndex;
};

#endif

#endif
