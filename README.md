# Unreal Engine 3: 2004 Build

Unreal Engine 3 2004 Build With CMake Build System

## Status

- CMake Building support.
  
  - Only Win32 support, no x64/Linux/Mac building ATM

- Debug building ATM (I Had not tested release building)
* All Projects except PCLaunc-UT/War/Demo game compiles fine.

## RoadMap:

- [ ] Get the engine to fully compile in Visual Studio 2022.

- [x] Remove UTGame since it cointains the entire of the Unreal Tournament 4 Assets.

- [ ] Updated All of the External Dependencies to newer ones
  
  - [ ] Replace Novodex (Formely know has PhysX nowdays) with either PhysX itself or Jolt.
  - [x] Update Libogg.
  - [x] Update Libpng.
  - [x] Update and make Zlib a separate dependency.
  - [x] Update DirectX9 Headers and Libs.

- [ ] Remove XBOX support.

- [ ] Support for non monolith builds.
